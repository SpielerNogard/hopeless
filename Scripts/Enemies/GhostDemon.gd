extends "BaseDemon.gd"

var attack_remaining_cd = 0

var preffered_vector = Vector2.ZERO
var preffered_target_position = Vector2.ZERO


var target_distance_activation = 200	#sobald in dieser Range, ignorieren von Abstoß Regeln
var target_distance_deactivation = 200

var distance_to_preffered_position = 0
var preffered_distance_to_target = 150
var number_of_solid_objects = 0

var speed_multiplier
var attack_cd = 100


func set_collision_allowed(new_collision_allowed):
	collision_allowed = new_collision_allowed

func stop_attacking():
	is_attacking = false
	sprite.modulate.r = 0.67
	sprite.modulate.g = 0.67
	sprite.modulate.b = 0.67
	current_moving_direction = Vector2.ZERO
func _ready():
	
	death_sound = get_parent().get_node("GhostDemonDeathSound")
	took_dmg_sound = get_parent().get_node("GhostDemonDmgSound")
	
	var scaling = (1 + (Settings.start_scaling * 0.01)) * pow((1 + (Settings.scaling_per_round * 0.01)), Global.runde - 1)
	type = "Ghost"
	game.add_demon(self,type)
	if Global.current_town_visiting == Global.demon_castle.name:
		scaling *=  pow((1 + (Settings.scaling_per_round * 0.01)), 5)
	var size_scale = 1 + (Global.runde - 1) * 0.05
	if size_scale >1.3:
		size_scale = 1.3
	self.scale = Vector2(size_scale, size_scale)
	speed_multiplier = Global.ghost_demon_speed_multiplier * scaling
	current_health = Global.ghost_demon_max_health * scaling
	preffered_target_position = character.global_position * scaling
	XP_value = round(Global.ghost_demon_xp_amount * scaling)
	max_speed = Global.ghost_demon_max_speed * scaling
	
	if max_speed > 1500:
		max_speed = 1500
	attack_dmg = Global.ghost_demon_attack_damage * scaling
	attack_cd = Global.ghost_demon_attack_cd / scaling
	attack_speed = float(1) / (attack_cd)
	floatiness_factor = 0.075
	minimum_distance_to_other_demons = 10


	health_bar.max_value = current_health
	health_bar.value = current_health
	health_bar.visible = false
	own_position = self.global_position


func check_inivisibility():
	if number_of_solid_objects >0:
		sprite.modulate.a = 0.1
	elif !invisibility_frames_on:
		sprite.modulate.a = 0.7

func other_routines(delta):
	check_inivisibility()
	distance_to_preffered_position = own_position.distance_to(preffered_target_position)
	attack_remaining_cd -= delta

func can_attack():
	return (distance_to_preffered_position < target_distance_activation and !is_attacking and attack_remaining_cd <= 0)

func attack():
	is_attacking = true
	sprite.modulate.r = 0.93
	sprite.modulate.g = 0.27
	sprite.modulate.b = 0.27
	if is_aggro:
		preffered_target_position = targeted_soldier.global_position
	else:
		preffered_target_position = character.global_position
		
	preffered_vector = preffered_target_position - own_position
	preffered_vector = preffered_vector.normalized()
	current_moving_direction = preffered_vector * current_moving_direction.length()

func attack_routines(delta):
	current_moving_direction = current_moving_direction + (preffered_vector * max_speed * floatiness_factor * speed_multiplier)
	if current_moving_direction.length() > max_speed * speed_multiplier:
		current_moving_direction = current_moving_direction.normalized() * max_speed * speed_multiplier
	self.position += current_moving_direction * delta
	own_position += current_moving_direction * delta
	
	if distance_to_preffered_position >target_distance_deactivation:
		is_attacking = false
		sprite.modulate.r = 0.67
		sprite.modulate.g = 0.67
		sprite.modulate.b = 0.67
		attack_remaining_cd = attack_cd

func cached_move(delta):
	current_moving_direction += new_movement_vector * floatiness_factor
	own_position += current_moving_direction * delta
	self.position += current_moving_direction * delta

func move(delta):
	var too_close = false
	if is_aggro:
		preffered_target_position = targeted_soldier.global_position
	else:
		preffered_target_position = character.global_position

	if distance_to_preffered_position > preffered_distance_to_target:
		new_movement_vector = (preffered_target_position - self.global_position).normalized()
	else:
		new_movement_vector =  (self.global_position - preffered_target_position).normalized()
		too_close = true

	walking_animation(new_movement_vector)
	if collision_allowed and !too_close:
		avoiding_demons()
	new_movement_vector += avoid_demons_vector
	current_moving_direction = current_moving_direction + (new_movement_vector.normalized() * max_speed * floatiness_factor)
	if current_moving_direction.length() >max_speed:
		current_moving_direction = current_moving_direction.normalized() * max_speed
	self.position += current_moving_direction * delta
	own_position += current_moving_direction * delta

func _on_HurtBox_body_entered(body):
	if body.name =="Player" and is_attacking:
		body.take_dmg(attack_dmg)
	
	if body.name !="Player":
		number_of_solid_objects += 1
	pass # Replace with function body.

func _on_HurtBox_body_exited(body):
	if body.name !="Player":
		number_of_solid_objects -= 1
	pass # Replace with function body.




func _on_AnimationPlayer_animation_finished(anim_name):
	process_animation_finished(anim_name)
