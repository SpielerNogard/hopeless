extends AnimatedSprite

var killed_demons = 0
var summoned_demons = 0
var summon_time_per_cycle = 0
var summon_time_left = 0
var demons_to_summon_per_cycle = 5

var all_black_demons = []
var current_black_demon_nr = 0
var current_ghost_demon_nr = 0
var all_ghost_demons = []

var black_demon = preload("res://Scenes/Enemies/Demon.tscn")
var ghost_demon = preload("res://Scenes/Enemies/GhostDemon.tscn")

var time_after_start = 0


onready var game = get_tree().current_scene

func victory():
	var UI = get_parent().get_node("CanvasLayer/GUI")
	UI.on_victory(Global.black_demons_to_summon + Global.ghost_demons_to_summon)
# Called when the node enters the scene tree for the first time.

# This is a super stupid solution.... but it works (2h Troubleshooting)
func spawn_all_instances():
	var black_demons_to_summon = Global.black_demons_to_summon
	var ghost_demons_to_summon = Global.ghost_demons_to_summon
	
	for a in range(0,black_demons_to_summon):
		var my_blackdemon = black_demon.instance()
		all_black_demons.append(my_blackdemon)
	
	for a in range(0, ghost_demons_to_summon):
		var my_ghost_demon = ghost_demon.instance()
		all_ghost_demons.append(my_ghost_demon)

func _ready():
	spawn_all_instances()
	Global.summoning_portal_killed_Demons= 0
	Global.summoning_portal_summoned_demons = 0
	summon_time_per_cycle = Global.summoning_portal_demon_summon_time
	summon_time_left = summon_time_per_cycle
	
func one_dead_demon(demon):
	killed_demons += 1
	#Global.summoning_portal_killed_Demons+=1
	#Global.summoning_portal_all_killed_Demons+=1

	if Global.summoning_portal_killed_Demons == Global.summoning_portal_demons_to_kill:
		print("Victory")
		victory()

func super_stupid_function():
	for i in range(0,100):
		pass

func summonDemon():
	super_stupid_function()
	var demons_left_to_summon = demons_to_summon_per_cycle
	var distance_angle = 0
	var distance_from_portal = 100
	
	while demons_left_to_summon >0:
		if !game.can_demon_be_summoned():
			return

		var demon_type = game.give_demon_to_summon()
		
		if demon_type == "black_demon":
			if current_black_demon_nr < all_black_demons.size():
				add_child(all_black_demons[current_black_demon_nr])
				current_black_demon_nr +=1
			
			#add_child(demon)
		elif demon_type == "ghost_demon":
			if current_ghost_demon_nr < all_ghost_demons.size():
				add_child(all_ghost_demons[current_ghost_demon_nr])
				current_ghost_demon_nr +=1
			
		demons_left_to_summon -= 1
		
		#distance_angle += (PI *2) / demons_to_summon_per_cycle
		#var demon = game.give_demon_to_summon().instance()
		#var position_change = Vector2(sin(distance_angle) * distance_from_portal, cos(distance_angle) * distance_from_portal)
		#demon.position += position_change
		#add_child(demon)
		#demons_left_to_summon -= 1
	
	return
	#only spawn a new demon after summoning time
	Global.summoning_portal_current_time += 1
	if Global.summoning_portal_current_time >= Global.summoning_portal_demon_summon_time:
		#only spawn a specific amount on the field
		if Global.summoning_portal_max_demons_on_field > Global.summoning_portal_current_demons_on_field:
			#only spawn the ammount of demons you need to finish the Quest
			if Global.summoning_portal_demons_to_kill > Global.summoning_portal_summoned_demons:
				var demon = Global.give_demon_to_summon().instance()
				add_child(demon)
				Global.summoning_portal_summoned_demons +=1
				get_parent().add_demon(demon)
				print(Global.summoning_portal_summoned_demons)
		Global.summoning_portal_current_time = 0
		
					
func _physics_process(delta):
	time_after_start +=delta
	summon_time_left -=delta
	
	if summon_time_left <=0:
		summon_time_left = summon_time_per_cycle
		summonDemon()
	
func _on_Demon_death():
	one_dead_demon(self)
	
