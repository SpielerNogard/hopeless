extends "BaseDemon.gd"



onready var grid_parent_node = get_tree().current_scene.get_node("movementgrids")

onready var grid_to_player = grid_parent_node.get_node("GridToPlayer")

onready var soldier_aggro_grid = grid_parent_node.get_node("GridToSoldiers")



var current_tile_x = 0
var current_tile_y = 0
var new_tile_x = 0
var new_tile_y = 0
var distance_to_player_as_cell = 100
var distance_to_soldier_as_cell = 100
var targets_in_attack_range = 0		# Soldaten sagen dem Dämonen, ob er in Angriffsreichweite ist
var attack_distance_to_player = 500
var follow_path_vector = Vector2.ZERO

func _ready():
	type = "Black"
	game.add_demon(self,"Black")
	
	death_sound = get_parent().get_node("BlackDemonDeathSound")
	took_dmg_sound = get_parent().get_node("BlackDemonDmgSound")
	var scaling = (1 + (Settings.start_scaling * 0.01)) * pow((1 + (Settings.scaling_per_round * 0.01)), Global.runde - 1)
	
	if Global.current_town_visiting == Global.demon_castle.name:
		scaling *=  pow((1 + (Settings.scaling_per_round * 0.01)), 5)
	
	var size_scale = 1 + (Global.runde - 1) * 0.05
	if size_scale >1.5:
		size_scale = 1.5
	self.scale = Vector2(size_scale, size_scale)

	attack_speed = Global.black_demon_attack_speed * scaling
	
	if attack_speed >5:
		attack_speed = 5

	current_health = Global.black_demon_max_health * scaling
	XP_value = round(Global.black_demon_xp_amount * scaling)
	max_speed = Global.black_demon_max_speed * scaling
	
	if max_speed > 1500:
		max_speed = 1500
	
	attack_dmg = Global.black_demon_attack_damage * scaling
	attack_distance_to_player = Global.black_demon_attack_distance_to_player
	
	own_position = self.global_position
	floatiness_factor = 0.075
	health_bar.max_value = current_health
	health_bar.value = current_health
	health_bar.visible = false

func set_collision_allowed(new_collision_allowed : bool):
	self.collision_allowed = new_collision_allowed

func get_vector_from_relevant_grid(relevant_grid):
	if relevant_grid.get_distance_of_cell(current_tile_x,current_tile_y) == 0:
		var new_vector =  relevant_grid.get_vector_direction(current_tile_x,current_tile_y, own_position) - own_position
		return new_vector.normalized()
	else:
		return relevant_grid.get_vector_direction(current_tile_x,current_tile_y, own_position)

func attack_routines(delta):
	var type = "Attack_"

	var new_look_direction = look_direction
	follow_path()
	var angle = (follow_path_vector.angle()) * (180/3.14)
	if angle <= 60 and angle >= -60:
		new_look_direction = "E"
	elif angle <= 120 and angle >= 60:
		new_look_direction = "S"
	elif angle >= -120 and angle <= -60:
		new_look_direction = "N"
	elif angle <= -120 or angle >= 120:
		new_look_direction = "W"


	if new_look_direction != look_direction:
		look_direction = new_look_direction
		var test = animator.current_animation_position 
		animator.current_animation = str(type + new_look_direction)
		animator.seek(test, true)	# bringt die neue Animation auf den Zeitpunkt der alten, das True wird benötigt, damit es geupdated wird
		
func follow_path():
	if is_aggro:
		if distance_to_soldier_as_cell <=8:
			follow_path_vector = get_vector_from_relevant_grid(soldier_aggro_grid)
		else:
			follow_path_vector = get_vector_from_relevant_grid(grid_to_player)
			
			
			if is_instance_valid(targeted_soldier) == false:
				is_aggro = false
			elif own_position.distance_to(targeted_soldier.global_position) >300:		# 300 wird derzeitig für die Aggro Range genutzt
				pass
				is_aggro = false
				if targeted_soldier.has_method("remove_aggro_demon"):
					targeted_soldier.remove_aggro_demon(self)
	else:
		follow_path_vector = get_vector_from_relevant_grid(grid_to_player)

func move(delta):

	if grid_to_player.is_there_a_wall(current_tile_x,current_tile_y):
		new_movement_vector = own_position - Vector2(current_tile_x * 48 + 24, current_tile_y *48 + 24)
		new_movement_vector =new_movement_vector.normalized() * max_speed
		move_and_collide(current_moving_direction * delta)
		own_position = self.global_position
		current_moving_direction = Vector2.ZERO
		take_dmg((current_health/10) + 10)
		print("JA Moin")
		return

	follow_path()
	walking_animation(follow_path_vector)
	avoiding_demons()
	new_movement_vector = avoid_demons_vector + follow_path_vector
	current_moving_direction += new_movement_vector.normalized() * max_speed * floatiness_factor
	if current_moving_direction.length()>max_speed:
		current_moving_direction = current_moving_direction.normalized() * max_speed

	is_move_allowed(delta)

	self.position += current_moving_direction * delta
	own_position +=current_moving_direction * delta

func is_move_allowed(delta):
	var new_position = own_position + current_moving_direction * delta
	new_tile_x = floor(new_position.x / 48)
	new_tile_y = floor(new_position.y / 48)
	current_moving_direction = grid_to_player.slide_at_wall(current_tile_x, 
	current_tile_y, 
	new_tile_x, 
	new_tile_y, 
	current_moving_direction)

func in_attack_range(change):
	targets_in_attack_range +=change

func attack():
	animator.play("Attack_" + look_direction)
	animator.playback_speed = attack_speed
	
	is_attacking = true
	#current_moving_direction = Vector2.ZERO
	#animator.play("Attack_" + look_direction)

func other_routines(delta):
	current_tile_x = floor(own_position.x / 48)
	current_tile_y = floor(own_position.y / 48)

func cached_move(delta):
	current_moving_direction += new_movement_vector * floatiness_factor
	is_move_allowed(delta)
	own_position += current_moving_direction * delta
	self.position += current_moving_direction * delta
	
func can_attack():
	if attack_allowed:
		distance_to_player_as_cell= grid_to_player.get_distance_of_cell(current_tile_x,current_tile_y)
		distance_to_soldier_as_cell= soldier_aggro_grid.get_distance_of_cell(current_tile_x,current_tile_y)
		var distance_to_player = own_position.distance_to(character.global_position)
		
		if distance_to_player_as_cell <= 1 or distance_to_soldier_as_cell <=1:
			return true
		
		if targets_in_attack_range >0 and distance_to_soldier_as_cell<=2:
			return true
		elif distance_to_player < attack_distance_to_player and distance_to_player_as_cell<=2:
			return true
	return false

func player_take_dmg(body):
	if body.is_in_group("Player"):
		if !is_aggro:
			body.take_dmg(attack_dmg)

	elif body.is_in_group("Soldier"):
		if body.has_method("take_dmg"):
			body.take_dmg(attack_dmg)

func _on_AttackAreaEast_body_entered(body):
	player_take_dmg(body)

func _on_AttackAreaSouth_body_entered(body):
	player_take_dmg(body)

func _on_AttackAreaWest_body_entered(body):
	player_take_dmg(body)

func _on_AttackAreaNorth_body_entered(body):
	player_take_dmg(body)

func _on_AnimationPlayer_animation_finished(anim_name):
	process_animation_finished(anim_name)


func _on_AttackNorth_area_entered(area):

	player_take_dmg(area.get_parent())


func _on_AttackWest_area_entered(area):

	player_take_dmg(area.get_parent())


func _on_AttackSouth_area_entered(area):

	player_take_dmg(area.get_parent())


func _on_AttackEast_area_entered(area):

	player_take_dmg(area.get_parent())
