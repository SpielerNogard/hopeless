extends Node2D

export (NodePath) var demon_1_node
export (NodePath) var demon_2_node
export (NodePath) var demon_3_node
export (NodePath) var demon_4_node

onready var demon_1 = get_node(demon_1_node)
onready var demon_2 = get_node(demon_2_node)
onready var demon_3 = get_node(demon_3_node)
onready var demon_4 = get_node(demon_4_node)

onready var my_timer = $Timer

var demon_to_kill = 1

func kill_demons():
	if demon_to_kill == 1:
		demon_1.die()
		demon_to_kill = 2
		my_timer.start(0.3)
	

func _on_Timer_timeout():
	if demon_to_kill == 2:
		demon_2.die()
		demon_to_kill += 1
	elif demon_to_kill == 3:
		demon_3.die()
		demon_to_kill += 1
	elif demon_to_kill == 4:
		demon_4.die()
		demon_to_kill += 1
	
	my_timer.start(0.3)


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Death":
		get_parent().get_parent().done_killing()
		queue_free()
