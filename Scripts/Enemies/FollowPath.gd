extends KinematicBody2D

var path = PoolVector2Array()


var max_health = 50
var current_health = 20
var bodys_of_interest = []

var max_speed = 200
var current_moving_direction = Vector2 (0,0)

var attack_distance_to_player = 2 # in Tiles angegeben

onready var animator = $AnimatedSprite

onready var movement_grid = get_tree().current_scene.get_node("MovementGrid")
onready var demon_grid = get_tree().current_scene.get_node("DemonAggroGrid")
var look_direction = "S"

var look_in_a_direction_for_some_frames = 6
var current_time_of_looking_in_a_direction = 0

var collision_allowed = true
var own_position = Vector2(0,0)
var follow_Path_vector = Vector2.ZERO
var avoid_demons_vector = Vector2.ZERO

var current_tile_x = 0
var current_tile_y = 0
var new_tile_x = 0
var new_tile_y = 0
var floatiness_factor = 0.05	#braucht ca.40 frames für top speed/ 80 frames für Richtungswechsel
var new_movement_vector = Vector2.ZERO
var follow_path = true
var slow_down = false
var offset = Vector2(0,0)
var slow_down_speed = 0.99
var current_speed = max_speed
func _ready():
	own_position = self.global_position + offset

func get_vector_from_relevant_grid(relevant_grid):
	var cell_distance = relevant_grid.get_distance_of_cell(current_tile_x,current_tile_y)
	if cell_distance == 0:
		var player_position = relevant_grid.get_vector_direction(current_tile_x,current_tile_y, own_position)
		var new_vector =  player_position - own_position
		return new_vector.normalized()
	else:
		return relevant_grid.get_vector_direction(current_tile_x,current_tile_y, own_position)

func movement_direction(delta):
	current_speed = current_moving_direction.length()
	if follow_path:
		own_position = self.global_position + offset
		current_tile_x = floor(own_position.x / 48)
		current_tile_y = floor(own_position.y / 48)
		if demon_grid.get_distance_of_cell(current_tile_x,current_tile_y)<=5:
			follow_Path_vector = get_vector_from_relevant_grid(demon_grid)
		else:
			follow_Path_vector = get_vector_from_relevant_grid(movement_grid)
		walking_animation()
		if abs(follow_Path_vector.x) > 2 or abs(follow_Path_vector.y) >2:
			follow_Path_vector =follow_Path_vector.normalized() * max_speed
			self.position += follow_Path_vector * delta
			current_moving_direction = Vector2.ZERO
			return
		follow_Path_vector = follow_Path_vector * max_speed
	else:
		follow_Path_vector = Vector2.ZERO
		current_moving_direction = current_moving_direction * slow_down_speed
	if collision_allowed:
		avoiding_bodys()
	new_movement_vector = follow_Path_vector + avoid_demons_vector * (current_speed / max_speed)
	if new_movement_vector.length() > max_speed:
		new_movement_vector = new_movement_vector.normalized() * max_speed
	

	current_moving_direction = current_moving_direction + (new_movement_vector * floatiness_factor)
	if current_moving_direction.length()>max_speed:
		current_moving_direction = current_moving_direction.normalized() * max_speed
	is_move_allowed(delta)
	
	move_and_collide(current_moving_direction * delta)
	
func is_move_allowed(delta):
	var new_position = own_position +  current_moving_direction * delta
	new_tile_x = floor(new_position.x / 48)
	new_tile_y = floor(new_position.y / 48)

	
	current_moving_direction = movement_grid.slide_at_wall(current_tile_x, current_tile_y, new_tile_x, new_tile_y, current_moving_direction)

func walking_animation():
	var type = "Walking_"
	var vector_length_one = new_movement_vector.normalized()
	var direction_x = vector_length_one.x
	var direction_y = vector_length_one.y
	var new_look_direction = look_direction
	
	if direction_x >  0.75:
		new_look_direction = "E"
	elif direction_x < -0.75:
		new_look_direction = "W"
	elif direction_y < -0.75:
		new_look_direction = "N"
	elif direction_y > 0.75:
		new_look_direction = "S"
	if new_look_direction != look_direction:
		look_direction = new_look_direction
		animator.play(type + look_direction)

func avoiding_bodys():
	var push_other_demons = Vector2 (0,0)
	for body in bodys_of_interest:
		var body_position = body.global_position + offset
		var distance = own_position.distance_to(body_position)
		var push_demon = own_position - body_position
		push_demon = push_demon.normalized()
		push_demon = push_demon * push_look_up(distance)
		push_other_demons += push_demon

			
	avoid_demons_vector = push_other_demons

func push_look_up(distance):
	var push_power = 1
	if distance >60:
		return -0.1
	else:
		return 0.5
	return push_power



func _physics_process(delta):
	own_position = self.global_position + offset
