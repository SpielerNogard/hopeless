extends KinematicBody2D

# Status Variabeln
var is_dead = false
var is_attacking = false
var collision_allowed = true
var is_aggro = false
var attack_allowed = true
var blinded = false
var blinded_movement_vector = Vector2.ZERO

var blind_duration_left = 0.0

# Variabeln zur Berechnung
var fully_updated_health_bar = true
var current_health_bar_update_point = 0
var avoid_demons_vector = Vector2.ZERO
var own_position
var new_movement_vector = Vector2.ZERO
var look_direction = "S"
var current_moving_direction = Vector2 (0,0)
var targeted_soldier		# in Theorie nur für GhostDemon relevant, war aber auch in Demon drinnen
var invisibility_frames_on = false
var type = "Black"

# Laden von Child Elemente
onready var health_bar = $HealthBar
onready var sprite = $AnimatedSprite
onready var animator : AnimationPlayer = $AnimationPlayer
onready var awarenss: Area2D = $HurtBox
onready var test = $HurtBox/CollisionShape2D
onready var took_dmg_sound
onready var death_sound

# Laden von Elemente aus der Szene
onready var character = get_tree().current_scene.get_node("Player")
onready var summoning_portal = get_parent()
onready var game = get_tree().current_scene

# Variabeln, die für jeden Dämonen bestimmt werden müssen
var minimum_distance_to_other_demons = 30
var current_health
var current_visible_health = 0		# Muss bei rdy = current_heath gesetzt werden
var XP_value
var floatiness_factor = 0.075
var max_speed = 200
var attack_dmg = 10
var time_between_attacks = 1.0
var attack_speed = 0.5

var preffered_distance_to_player = 10

func make_aggro(soldier) ->bool:
	if is_aggro or is_dead:
		return false
	targeted_soldier = soldier
	is_aggro = true
	game.add_demon_aggro(self)
	return true


func undo_aggro():
	if is_aggro:
		game.remove_demon_aggro(self)
		is_aggro = false

func attack_routines(delta):
	pass


func make_blinded(time):
	if is_dead:
		return
	blinded = true
	blind_duration_left = time 
	blinded_movement_vector = Vector2(randf(),randf())
	walking_animation(blinded_movement_vector)
	animator.play("Walking_"+look_direction)

func blinded_movement(delta):
	var movement_vector = blinded_movement_vector
	if collision_allowed:
		avoiding_demons()

	movement_vector += avoid_demons_vector
	
	current_moving_direction += movement_vector.normalized() * max_speed * floatiness_factor
	if current_moving_direction.length()>max_speed:
		current_moving_direction = current_moving_direction.normalized() * max_speed
	is_move_allowed(delta)
	self.position += current_moving_direction * delta
	own_position += current_moving_direction * delta

func is_move_allowed(delta):
	pass

func _physics_process(delta):
	if game.fight_over:
		return
	blind_duration_left -= delta
	if blind_duration_left <=0:
		blinded = false
	
	if is_aggro:
		if is_instance_valid(targeted_soldier)==false:
			undo_aggro()
	
	if !fully_updated_health_bar:
		update_health_bar(delta * 5)	# Braucht 0.2 Sekunden für das Update
	other_routines(delta)
	if is_dead:
		return
	
	if blinded:
		blinded_movement(delta)
		return
	
	if is_attacking:
		attack_routines(delta)
		return
	if can_attack():
		attack()
		return

	if collision_allowed:
		move(delta)
	else:
		cached_move(delta)

func other_routines(delta):
	pass
func update_health_bar(delta):
	current_health_bar_update_point += delta
	
	if current_health_bar_update_point >1:
		current_health_bar_update_point = 1
		fully_updated_health_bar = true
	var new_value = current_visible_health * (1 - current_health_bar_update_point) + current_health * current_health_bar_update_point
	
	if new_value == 0:
		health_bar.queue_free()
		fully_updated_health_bar = true
	else:
		health_bar.value = new_value

func set_collision_allowed(new_collision_allowed : bool):
	collision_allowed = new_collision_allowed

func can_attack():

	pass

func attack():
	pass

func move(delta):
	pass

func cached_move(delta):
	pass

func stop_attacking():
	pass
func take_dmg(dmg):
	if is_dead or game.fight_over:
		return
	stop_attacking()
	health_bar.visible = true
	current_health -= dmg
	current_health_bar_update_point = 0
	fully_updated_health_bar = false
	current_visible_health = health_bar.value
	

	
	if current_health <= 0:
		if Settings.enable_soundeffekte and !death_sound.playing:
			death_sound.volume_db = Settings.soundeffekte_volume
			death_sound.play()
		death()
	else:
		sprite.modulate.a = 0.05
		invisibility_frames_on = true
		yield(get_tree().create_timer(0.1), "timeout")
		invisibility_frames_on = false
		sprite.modulate.a = 1
		if Settings.enable_soundeffekte and !took_dmg_sound.playing:
			took_dmg_sound.volume_db = Settings.soundeffekte_volume
			took_dmg_sound.play()



func walking_animation(relevant_vector):
	var type = "Walking_"
	var vector_length_one = relevant_vector.normalized()
	var direction_x = vector_length_one.x
	var direction_y = vector_length_one.y
	var new_look_direction = look_direction
	
	if direction_x >  0.75:
		new_look_direction = "E"
	elif direction_x < -0.75:
		new_look_direction = "W"
	elif direction_y < -0.75:
		new_look_direction = "N"
	elif direction_y > 0.75:
		new_look_direction = "S"
	if new_look_direction != look_direction:
		look_direction = new_look_direction
		animator.play(type + look_direction)


func get_aggro():
	return is_aggro

func death():
	is_dead = true
	game.remove_demon(self,type)
	
	if is_aggro:
		game.remove_demon_aggro(self)
	
	if is_aggro and is_instance_valid(targeted_soldier)==true:
		targeted_soldier.remove_aggro_demon(self)

	
	character._get_XP(XP_value)

	animator.play("Death")
	

	summoning_portal.one_dead_demon(self)

func avoiding_demons():
	#var current_speed = current_moving_direction.length()
	avoid_demons_vector = Vector2.ZERO
	var test = awarenss.get_overlapping_areas()
	var max_iterations = 10
	var current_iteration = 0
	
	for area in test:
		current_iteration +=1
		if current_iteration > max_iterations:
			break

		if area.is_in_group("Demon"):
			calc_per_area(area,minimum_distance_to_other_demons)
		else:
			#calc_per_area(area,10)
			#print("YES")
			pass

func calc_per_area(area, min_distance):
	var area_position = area.global_position
	var distance = own_position.distance_to(area_position)
	if distance < min_distance:
		avoid_demons_vector += (own_position - area_position).normalized() * 10
	else:
		avoid_demons_vector += (own_position - area_position).normalized() * (1/(0.1*(distance - (min_distance - 1))))

func process_animation_finished(anim_name):
	if anim_name == "Death":
		#self.global_position = Vector2(10000000000, 10000000000)
		#set_physics_process(false)
		game.add_me_to_kill(self)
	if "Attack" in anim_name:
		animator.play("Walking_"+look_direction)
		is_attacking = false
		attack_allowed = false
		animator.playback_speed = 1
		yield(get_tree().create_timer((float(1) / (attack_speed))), "timeout")
		attack_allowed = true
