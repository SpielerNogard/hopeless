extends Node2D


# Declare member variables here. Examples:
onready var soldat = preload("res://Scenes/Players/Soldier.tscn")
onready var mage = preload("res://Scenes/Players/Mage.tscn")
onready var game = get_tree().current_scene

var summoning_time = 1
var current_time = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func summon_mate():
	if Global.current_soldier_on_field < Global.max_soldier_on_field:
		if Global.einfacher_fussoldat_fur_Schlacht > 0:
			var soldat_spawn = soldat.instance()
			add_child(soldat_spawn)
			Global.einfacher_fussoldat_fur_Schlacht-=1
			Global.current_soldier_on_field+=1
			
			
		if Global.Mages_fur_Schlacht > 0:
			var mages_spawn = mage.instance()
			add_child(mages_spawn)
			Global.Mages_fur_Schlacht-=1
			Global.current_soldier_on_field+=1
		
	
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	current_time += 1
	if summoning_time <= current_time:
		summon_mate()
		current_time = 0
