extends "play_skill.gd"



# var b = "text"


var added_armor = 0
var removed_speed = 0

onready var player_material = get_parent().get_parent().material

func _ready():
	cooldown = 8
	active_time = 10
	channel_time = 0.5
	mana_cost = 50
	picture = preload("res://Assets/GUI/Skill_Icon/Combat_16_T.png")

func get_full_cooldown():
	return cooldown

func activate_skill(orientation):
	player_material.set_shader_param("transition_to_stone", 1)
	active_time_left = active_time
	var previous_armor = Global.player_armor
	var new_armor = previous_armor * 5
	added_armor = new_armor - previous_armor 
	Global.player_armor_multiplier = 30

func disable_skill():
	Global.player_armor_multiplier = 1
	player_material.set_shader_param("transition_to_stone", 0)

