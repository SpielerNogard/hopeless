extends "play_skill.gd"




var little_thunderstorms = preload("res://Scenes/Players/LightningEffect.tscn")
onready var collision = get_node("AttackArea/CollisionShape2D")

var dmg = 10
var max_targets = 40
var targets_left = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	cooldown = 1
	active_time = 0.1
	channel_time = 3
	mana_cost = 100
	Global.set_armor_and_damage()
	dmg = Global.player_attack_damage * 5
	picture = preload("res://Assets/GUI/Skill_Icon/if762.png")
	sound_reducing = 10

func activate_skill(orientation):
	collision.disabled = false
	targets_left = max_targets

func get_full_cooldown():
	return cooldown

	
func disable_skill():
	collision.disabled = true

func other_skill_relevant_stuff(delta):
	dmg = Global.player_attack_damage * 2
func _on_AttackArea_area_entered(area):
	if area.is_in_group("Demon") and targets_left >0:
#		area.get_parent().take_dmg(dmg)
		
		var erstellteCharacter = little_thunderstorms.instance()
		area.get_parent().add_child(erstellteCharacter)
		erstellteCharacter.set_dmg(dmg)
		targets_left -= 1
