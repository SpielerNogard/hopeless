extends "play_skill.gd"


var speed_up = 2
var given_movementspeed = 0

func _ready():
	cooldown = 1.5
	active_time = 0.5
	mana_cost = 0

func activate_skill(orientation):

	var previous_ms = Global.player_speed
	var next_ms = previous_ms * speed_up
	given_movementspeed = next_ms - previous_ms


	Global.player_speed_up_speed = speed_up
	
func disable_skill():
	Global.player_speed_up_speed = 1
