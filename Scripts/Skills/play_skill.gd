extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var current_cooldown = 0.0
var cooldown = 1 #in Sekunden

var channel_time = 1
var active_time = 1
var active_time_left = 0
var mana_cost = 200
onready var player = get_parent().get_parent()
onready var sound_player = $skillSound

var sound_reducing = 0

var picture = preload("res://Assets/GUI/Skill_Icon/32_6.png")
var running = false


func get_mana_cost():
	return mana_cost

func get_full_cooldown():
	return cooldown

func is_of_cooldown():
	if current_cooldown <= 0:
		return true
	return false 

func get_channel_time():
	return channel_time

func play_skill(orientation):
	if current_cooldown <=0:
		player.pay_mana_cost(mana_cost)
		current_cooldown = cooldown
		running = true
		active_time_left = active_time
		activate_skill(orientation)
		play_sound()


func play_sound():
	if Settings.enable_soundeffekte:
		sound_player.volume_db = Settings.soundeffekte_volume - sound_reducing
		sound_player.play() 

func activate_skill(orientation):
	pass
	

func _process(delta):
	current_cooldown -= delta
	active_time_left -= delta
	
	if active_time_left <=0 and running:
		running = false
		disable_skill()
	if active_time_left >0:
		other_skill_relevant_stuff(delta)

func disable_skill():
	pass

func other_skill_relevant_stuff(delta):
	pass
