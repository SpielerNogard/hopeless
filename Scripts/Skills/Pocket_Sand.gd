extends "play_skill.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"




var collision_enabled = false

onready var collision = get_node("pocket_sand/AttackArea/CollisionShape2D")
onready var particles = get_node("pocket_sand")
# Called when the node enters the scene tree for the first time.
func _ready():
	cooldown = 1
	active_time = 0.1
	channel_time = 0.5
	mana_cost = 30
	picture = preload("res://Assets/GUI/Skill_Icon/32_6.png")
	

func get_full_cooldown():
	return cooldown


func disable_skill():
	collision.disabled = true

func activate_skill(orientation):
	particles.restart()
	collision.disabled = false
	if orientation == "N":
		self.rotation_degrees = -90
	elif orientation == "E":
		self.rotation_degrees = 0
	elif orientation == "S":
		self.rotation_degrees = 90
	elif orientation == "W":
		self.rotation_degrees = -180
	particles.emitting = true
	

func _on_AttackArea_area_entered(area):
	if area.is_in_group("Demon"):
		area.get_parent().take_dmg(0)
		area.get_parent().make_blinded(5)
