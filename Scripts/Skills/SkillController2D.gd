extends Node2D

onready var skill_1_ui_picture = get_node("../../CanvasLayer/GUI/horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skills/Skill1/NinePatchRect/TextureRect")
onready var skill_2_ui_picture = get_node("../../CanvasLayer/GUI/horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skills/Skill2/NinePatchRect/TextureRect")
onready var skill_3_ui_picture = get_node("../../CanvasLayer/GUI/horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skills/Skill3/NinePatchRect/TextureRect")
onready var skill_4_ui_picture = get_node("../../CanvasLayer/GUI/horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skills/Skill4/NinePatchRect/TextureRect")

onready var skill_1_cooldown = get_node("../../CanvasLayer/GUI/horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skills/Skill1/NinePatchRect/Cooldown")
onready var skill_2_cooldown = get_node("../../CanvasLayer/GUI/horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skills/Skill2/NinePatchRect/Cooldown")
onready var skill_3_cooldown = get_node("../../CanvasLayer/GUI/horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skills/Skill3/NinePatchRect/Cooldown")
onready var skill_4_cooldown = get_node("../../CanvasLayer/GUI/horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skills/Skill4/NinePatchRect/Cooldown")

var default = "pocket_sand"
var skill_1 = default
var skill_2 = "thunder_storm"
var skill_3 = default
var skill_4 = default

func play_skill(number):
	var skill = ""
	if number == 1:
		skill = get_node(skill_1)
	if number == 2:
		skill = get_node(skill_2)
	if number == 3:
		skill = get_node(skill_3)
	if number == 4:
		skill = get_node(skill_4)
	skill.play_skill(get_parent().current_look_direction)	
	
func set_skill(number,name):
	if number == 1:
		skill_1 = name
	if number == 2:
		skill_2 = name
	if number == 3:
		skill_3 = name
	if number == 4:
		skill_4 = name
# Called when the node enters the scene tree for the first time.

func get_skill_cooldown(number):
	var skill = ""
	if number == 1:
		skill = get_node(skill_1)
	if number == 2:
		skill = get_node(skill_2)
	if number == 3:
		skill = get_node(skill_3)
	if number == 4:
		skill = get_node(skill_4)
	return(skill.current_cooldown)
	
func set_skill_picture_in_ui():
	skill_1_ui_picture.set_texture(get_node(skill_1).picture)
	skill_2_ui_picture.set_texture(get_node(skill_2).picture)
	skill_3_ui_picture.set_texture(get_node(skill_3).picture)
	skill_4_ui_picture.set_texture(get_node(skill_4).picture)
	
func set_cooldowns():
	skill_1_cooldown.set_text(str(get_node(skill_1).current_cooldown_in_secs)+" s")
	skill_2_cooldown.set_text(str(get_node(skill_1).current_cooldown_in_secs)+" s")
	skill_3_cooldown.set_text(str(get_node(skill_1).current_cooldown_in_secs)+" s")
	skill_4_cooldown.set_text(str(get_node(skill_1).current_cooldown_in_secs)+" s")
	
func get_skill_name(number):
	var skill = ""
	if number == 1:
		skill = skill_1
	if number == 2:
		skill = skill_2
	if number == 3:
		skill = skill_3
	if number == 4:
		skill = skill_4
	return(skill)
	
func get_skill_picture(number):
	var skill = ""
	if number == 1:
		skill = get_node(skill_1)
	if number == 2:
		skill = get_node(skill_2)
	if number == 3:
		skill = get_node(skill_3)
	if number == 4:
		skill = get_node(skill_4)
	return(skill.picture)
	
	
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#poket_sand.play_skill("North")
	pass
	set_skill_picture_in_ui()
	set_cooldowns()
	
	
