extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var animations_left = 3
var dmg = 1



# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func set_dmg(new_dmg):
	dmg = new_dmg


func _on_AnimatedSprite_animation_finished():
	get_parent().take_dmg(dmg/3)
	if animations_left <=0:
		queue_free()
	animations_left  -= 1
	
