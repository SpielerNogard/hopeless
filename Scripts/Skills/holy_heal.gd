extends "play_skill.gd"





var heal_per_second = 600

onready var particle = get_node("Particles2D")
onready var attack_area = get_node("Particles2D/AttackArea")
onready var collision = get_node("Particles2D/AttackArea/CollisionShape2D")

func _ready():
	cooldown = 10
	active_time = 4
	channel_time = 2
	mana_cost = 50
	picture = preload("res://Assets/GUI/Skill_Icon/Basic_10_T.png")


func activate_skill(orientation):
	particle.emitting = true
	collision.disabled = false

func disable_skill():
	collision.disabled = true

func other_skill_relevant_stuff(delta):
	var overlapping_bodies = attack_area.get_overlapping_bodies()
	var overlapping_areas = attack_area.get_overlapping_areas()
	
	for body in overlapping_bodies:
		if body.is_in_group("Player"):
			body.heal(heal_per_second * delta)
	
	for area in overlapping_areas:
		if area.is_in_group("Soldier"):
			area.get_parent().heal(heal_per_second * delta)
