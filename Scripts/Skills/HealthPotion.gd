extends "play_skill.gd"

var is_health_potion = true
var amount = 1

func _ready():
	cooldown = 2
	mana_cost = 0
	active_time = 0.7


func is_of_cooldown():
	if current_cooldown <=0:
		if Global.new_health_potion.equipped:
			if Global.new_health_potion.owned >0:
				return true
		else:
			if Global.new_mana_potion.owned > 0:
				return true
	return false

func play_skill(orientation):
	if current_cooldown <=0:
		current_cooldown = cooldown
		running = true
		play_sound()
		yield(get_tree().create_timer(0.3),"timeout")
		active_time_left = active_time
		activate_skill(orientation)

func activate_skill(orientation):
	amount = 0
	if Global.new_health_potion.equipped:
		use_health_potion()
		is_health_potion = true
	else:
		use_mana_potion()
		is_health_potion = false
	
func other_skill_relevant_stuff(delta):
	if !is_health_potion:
		player.give_mana(amount * delta)
	else:
		player.give_health(amount * delta)
	
func use_mana_potion():
	Global.new_mana_potion.owned -=1
	amount = (Global.new_mana_potion.strength * Global.player_max_mana * 0.01)  / (active_time)


func use_health_potion():
	Global.new_health_potion.owned -=1
	amount = (Global.new_health_potion.strength * Global.player_max_health * 0.01) /(active_time)

