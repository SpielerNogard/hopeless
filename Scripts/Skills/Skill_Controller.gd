extends Node2D

onready var gui = get_tree().current_scene.get_node("InformationLayer/GUI")
onready var player = get_parent()





var default = "pocket_sand"

var skill_1_name = default
var skill_2_name = "thunder_storm"
var skill_3_name = "holy_heal"
var skill_4_name = "stone_skin"

var current_cooldown_speed_up = 0
var speed_up_time = 2
onready var skill_1_node
onready var skill_2_node
onready var skill_3_node 
onready var skill_4_node

onready var potion = get_node("HealthPotion")
onready var basic_attacks = get_node("BasicAttack")
onready var speedUp = get_node("speedUp")


onready var error_sound = $ErrorSound


func get_channel_time(nr):
	var skill = ""
	
	if nr == 1:
		skill = skill_1_node
	if nr == 2:
		skill = skill_2_node
	if nr == 3:
		skill = skill_3_node
	if nr == 4:
		skill = skill_4_node
		
	if nr == 5:
		skill = basic_attacks
	
	if nr == 6:
		skill = speedUp
	if nr == 7:
		skill = potion
	return skill.get_channel_time()

func play_skill(number):
	var skill = ""
	
	if number == 1:
		skill = skill_1_node
	if number == 2:
		skill = skill_2_node
	if number == 3:
		skill = skill_3_node
	if number == 4:
		skill = skill_4_node
		
	if number == 5:
		skill = basic_attacks
	
	if number == 6:
		skill = speedUp

	elif number == 7:
		skill = potion

	if skill.is_of_cooldown():
		skill.play_skill(player.current_look_direction)

func is_skill_ready(nr,mana_left):
	var skill = ""
	
	if nr == 1:
		skill = skill_1_node
	elif nr == 2:
		skill = skill_2_node
	elif nr == 3:
		skill = skill_3_node
	elif nr == 4:
		skill = skill_4_node
	elif nr == 5:
		skill = basic_attacks
	elif nr == 6:
		skill = speedUp
	elif nr == 7:
		skill = potion
	

	if skill.is_of_cooldown() and mana_left >= skill.get_mana_cost():
		return true
	elif nr != 5:
		if Settings.enable_soundeffekte:
			error_sound.volume_db = Settings.soundeffekte_volume
			error_sound.play(0)
	
	return false
	

	
func set_skill(number,name):
	if number == 1:
		skill_1_name = name
		skill_1_node = get_node(name)
	if number == 2:
		skill_2_name = name
		skill_2_node = get_node(name)
	if number == 3:
		skill_3_name = name
		skill_3_node = get_node(name)
	if number == 4:
		skill_4_name = name
		skill_4_node = get_node(name)
	set_skill_picture_in_ui()


func get_skill_cooldown(number):
	var skill = ""
	if number == 1:
		skill = skill_1_node
	if number == 2:
		skill = skill_2_node
	if number == 3:
		skill = skill_3_node
	if number == 4:
		skill = skill_4_node
	return(skill.current_cooldown)

func get_all_skills():
	var all_my_skills = Global.player_skills

	var skill1 = all_my_skills[0]
	var skill2 = all_my_skills[1]
	var skill3 = all_my_skills[2]
	var skill4 = all_my_skills[3]
	
	set_skill(1,skill1[0])
	set_skill(2,skill2[0])
	set_skill(3,skill3[0])
	set_skill(4,skill4[0])
	
func set_skill_picture_in_ui():
	gui.set_skill_picture_in_ui(1, skill_1_node.picture)
	gui.set_skill_picture_in_ui(2, skill_2_node.picture)
	gui.set_skill_picture_in_ui(3, skill_3_node.picture)
	gui.set_skill_picture_in_ui(4, skill_4_node.picture)
	
	
	gui.set_potion_picture()
	#skill_1_ui_picture.set_texture(get_node(skill_1).picture)
	#skill_2_ui_picture.set_texture(get_node(skill_2).picture)
	#skill_3_ui_picture.set_texture(get_node(skill_3).picture)
	#skill_4_ui_picture.set_texture(get_node(skill_4).picture)
	
func set_cooldowns():
	var remaining_cooldown = skill_1_node.current_cooldown
	var value = remaining_cooldown/skill_1_node.get_full_cooldown() * 100
	gui.set_cooldown_skill(remaining_cooldown,value,1)
	gui.set_mana_cost(skill_1_node.get_mana_cost() , 1)
	
	remaining_cooldown = skill_2_node.current_cooldown
	value = remaining_cooldown/skill_2_node.get_full_cooldown() * 100
	gui.set_cooldown_skill(remaining_cooldown,value,2)
	gui.set_mana_cost(skill_2_node.get_mana_cost() , 2)
	
	remaining_cooldown = skill_3_node.current_cooldown
	value = remaining_cooldown/skill_3_node.get_full_cooldown() * 100
	gui.set_cooldown_skill(remaining_cooldown,value,3)
	gui.set_mana_cost(skill_3_node.get_mana_cost() , 3)
	
	remaining_cooldown = skill_4_node.current_cooldown
	value = remaining_cooldown/skill_4_node.get_full_cooldown() * 100
	gui.set_cooldown_skill(remaining_cooldown,value,4)
	gui.set_mana_cost(skill_4_node.get_mana_cost() , 4)
	
	remaining_cooldown = basic_attacks.current_cooldown
	value = remaining_cooldown/basic_attacks.get_full_cooldown() * 100
	gui.set_cooldown_skill(remaining_cooldown,value,5)
	gui.set_mana_cost(basic_attacks.get_mana_cost() , 5)
	
	remaining_cooldown = speedUp.current_cooldown
	value = remaining_cooldown/speedUp.get_full_cooldown() * 100
	gui.set_cooldown_skill(remaining_cooldown,value,6)
	gui.set_mana_cost(speedUp.get_mana_cost() , 6)
	
	remaining_cooldown = potion.current_cooldown
	value = remaining_cooldown/potion.get_full_cooldown() * 100
	gui.set_stats_potion(remaining_cooldown,value)
	gui.set_mana_cost(potion.get_mana_cost() , 7)


	
func get_skill_name(number):
	var skill = ""
	if number == 1:
		skill = skill_1_name
	if number == 2:
		skill = skill_2_name
	if number == 3:
		skill = skill_3_name
	if number == 4:
		skill = skill_4_name
	return(skill)
	
func get_skill_picture(number):
	var skill = ""
	if number == 1:
		skill = skill_1_node
	if number == 2:
		skill = skill_2_node
	if number == 3:
		skill = skill_3_node
	if number == 4:
		skill = skill_4_node
	return(skill.picture)
	
	
func speed_up():
	if current_cooldown_speed_up <=0:
		current_cooldown_speed_up = speed_up_time
	
func _ready():
	skill_1_node = get_node(skill_1_name)
	skill_2_node = get_node(skill_2_name)
	skill_3_node = get_node(skill_3_name)
	skill_4_node = get_node(skill_4_name)
	

	# skill_nodes sind sonst nill :/
	yield (get_tree().current_scene,"ready")
	get_all_skills()
	set_skill_picture_in_ui()
	
func handle_speed_up(delta):
	if current_cooldown_speed_up >= 0:
		current_cooldown_speed_up -= delta
		Global.player_speed = Global.player_speed_up_speed
	else:
		Global.player_speed = Global.player_start_speed

func _process(delta):
	#poket_sand.play_skill("North")
	#pass
	set_skill_picture_in_ui()

	
	
	set_cooldowns()
	#get_all_skills()
	
	
