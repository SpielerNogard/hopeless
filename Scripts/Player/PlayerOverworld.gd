extends KinematicBody2D

export (NodePath) var walkable_tiles_node


onready var animator = $AnimationPlayer
onready var walkable_tiles = get_node(walkable_tiles_node)
onready var bump_sound = $BumpSound


var x
var y
var current_tile_x
var current_tile_y
var next_tile_x
var next_tile_y
var next_tile_position
var town_to_enter = null
var speed
var base_speed

var animation_playing = false
var in_town = false
var in_cutscene = false
var shall_enter_town = false

var swapped_directions = false
var previous_direction = "S"


var moving_between_tiles = false

var movement = Vector2.ZERO

var size_tiles = 48




func _ready():
	x = self.global_position.x
	y = self.global_position.y
	base_speed = Global.player_base_movement_speed
	speed = base_speed
	
	current_tile_x = floor(x / size_tiles)
	current_tile_y = floor(y / size_tiles)
	
	self.global_position = Vector2(current_tile_x * size_tiles + size_tiles / 2,
									current_tile_y * size_tiles + + size_tiles / 2)
	

func set_in_cutscene(is_in_cutscene):
	in_cutscene = is_in_cutscene

func _physics_process(delta):
	if in_cutscene or in_town:
		return
	
	if Input.is_action_pressed("ui_overworld_speed"):
		speed = base_speed * 2
		animator.playback_speed = 2
	else:
		speed = base_speed
		animator.playback_speed = 1
	
	x = self.global_position.x
	y = self.global_position.y
	
	current_tile_x = floor(x / size_tiles)
	current_tile_y = floor(y / size_tiles)
	

	
	if moving_between_tiles:
		move_exact_tiles(delta)

	elif town_to_enter != null and shall_enter_town:
		in_town = true
		shall_enter_town = false
		town_to_enter.enter_town()
	else:
		decide_next_tile(delta)


func move_exact_tiles(delta, first_step = false):
	if walkable_tiles.get_cell(next_tile_x,next_tile_y) == -1:
		moving_between_tiles = false
		self.global_position = Vector2(current_tile_x * size_tiles + size_tiles / 2,
									current_tile_y * size_tiles + + size_tiles / 2)
		
		animator.stop()
		animator.seek(0,true)
		animation_playing = false
		if Settings.enable_soundeffekte and swapped_directions:
			swapped_directions = false
			bump_sound.volume_db = Settings.soundeffekte_volume
			bump_sound.play()
		return
	
	
	var distance = self.global_position.distance_to(next_tile_position)
	if distance < movement.length() * delta or distance > 100:
		self.global_position = next_tile_position
		moving_between_tiles = false
	else:
		self.position += movement * delta


func decide_next_tile(delta):
	movement = Vector2.ZERO
	var new_direction = previous_direction
	
	if Input.is_action_pressed("ui_up"):
		new_direction = "N"
		moving_between_tiles = true
		movement.y = -speed
		next_tile_x = current_tile_x
		next_tile_y = current_tile_y - 1
		play_animation("N")
		
	elif Input.is_action_pressed("ui_down"):
		new_direction = "S"
		moving_between_tiles = true		
		movement.y = speed
		next_tile_x = current_tile_x
		next_tile_y = current_tile_y + 1
		play_animation("S")
		
	elif Input.is_action_pressed("ui_left"):
		new_direction = "W"
		moving_between_tiles = true
		movement.x = -speed
		next_tile_x = current_tile_x - 1
		next_tile_y = current_tile_y
		play_animation("W")
		
	elif Input.is_action_pressed("ui_right"):
		new_direction = "E"
		moving_between_tiles = true
		movement.x = speed
		next_tile_x = current_tile_x + 1
		next_tile_y = current_tile_y
		play_animation("E")
	
	else:
		if animation_playing:
			animator.stop()
			animator.seek(0,true)
			animation_playing = false
	if new_direction != previous_direction:
		previous_direction = new_direction
		swapped_directions = true
	
	if moving_between_tiles:
		next_tile_position = Vector2(next_tile_x * size_tiles + size_tiles / 2, next_tile_y * size_tiles + size_tiles / 2)
		move_exact_tiles(delta)


func play_animation(direction):
	animator.play("Walking_" + direction )
	animation_playing = true

# Ersetzen durch eigentliches laden des Lvls

func set_town_enter(scene_path):
	town_to_enter = scene_path


func set_town_node(node):
	town_to_enter = node

func set_in_town(is_in_town):
	shall_enter_town = is_in_town
	in_town = false

func enter_town():
	Global.switch_to_lvl()
	self.set_physics_process(false)
