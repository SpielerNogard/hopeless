extends Camera2D

onready var player = get_parent().get_node("Path2D/PathFollow2D/KinematicBody2D")

func _process(delta):
	position.x = player.global_position.x
	position.y = player.global_position.y
