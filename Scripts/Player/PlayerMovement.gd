extends KinematicBody2D

export var casting_finished = false

var movement = Vector2 (0,0)
var animation_time = 1.2

var current_frame = 0



onready var animation_player = $AnimationPlayer
onready var lvl_up_particles = $LVL_UP
onready var death_space = $BigBlackSpace
onready var game = get_tree().current_scene
onready var gui = game.get_node("InformationLayer/GUI")
onready var skill_controler = $Skill_Controller

var current_look_direction = "S"
var speed
var attack
var attack_speed
var armor
var max_health
var current_health
var max_mana
var current_mana

var has_won = false

var is_attacking = false
var is_dead = false
var channeling = false
var spell_preparing = 1
var in_battle = false

# Called when the node enters the scene tree for the first time.
func _ready():
	yield (get_tree().current_scene,"ready")
	Global.set_armor_and_damage()
	
	
	speed = Global.player_speed
	max_health = Global.player_max_health
	current_health = max_health
	max_mana = Global.player_max_mana
	current_mana = max_mana
	armor = Global.player_armor
	attack = Global.player_attack_damage
	attack_speed = Global.player_attack_speed

func give_item(item,amount):
	if item == "XP":
		_get_XP(amount)

func current_look_direction_lookup():
	if Input.is_action_pressed("ui_up"):
		current_look_direction = "N"
	if Input.is_action_pressed("ui_down"):
		current_look_direction = "S"
	if Input.is_action_pressed("ui_left"):
		current_look_direction = "W"
	if Input.is_action_pressed("ui_right"):
		current_look_direction = "E"


func moving():
	movement = Vector2 (0,0)
	current_look_direction_lookup()

	if Input.is_action_pressed("ui_up"):
		movement.y = -speed
	elif Input.is_action_pressed("ui_down"):
		movement.y = speed
	if Input.is_action_pressed("ui_left"):
		movement.x = -speed
	elif Input.is_action_pressed("ui_right"):
		movement.x = speed
	
	#movement = movement.normalized()
	#movement = movement * speed
	return movement

func animation(type):
	if !is_dead:
		animation_player.play(type + current_look_direction)

func take_dmg(amount):
	var dmg = amount * ( float(100) / (100 + armor))
	current_health-= dmg

	if current_health <= 0:
		die()	
		current_health = 0

func die():
	if !is_dead:
		animation_player.play("Death")
		is_dead = true
		#get_tree().paused = true
		self.z_index = 10
		death_space.visible = true
		game.set_process(false)
		self.global_position = Vector2(10000,10000)
		
		#get_parent().get_node("CanvasLayer/GUI").visible = false
		
		var GUI = get_parent().get_node("InformationLayer/GUI")
		GUI.dead_player()
		$Skill_Controller.queue_free()
		

func _get_XP(amount):
	Global.player_current_xp+= amount
	if Global.player_current_xp >= Global.player_xp_next_lvl:
		_lvl_up()

func heal(heal_amount):
	current_health += heal_amount
	if current_health > max_health:
		current_health = max_health

func _lvl_up():
	lvl_up_particles.emitting = true
	Global.player_current_lvl += 1
	Global.player_current_xp-=Global.player_xp_next_lvl
	Global.player_xp_next_lvl = round(Global.player_xp_next_lvl * Global.player_xp_multiplier)
	
	Global.player_max_health += Global.player_health_per_lvl
	max_health = Global.player_max_health
	#current_health = max_health
	
	Global.player_max_mana += Global.player_mana_per_lvl
	max_mana = Global.player_max_mana
#	current_mana = max_mana

func process_action(nr):
	if skill_controler.is_skill_ready(nr,current_mana):
		spell_preparing = nr
		animation("Casting_")
		var channel_time = float(1) /(skill_controler.get_channel_time(nr) * 0.6)
		channel_time = channel_time * pow(0.8, (Global.player_current_lvl - 1))
		
		animation_player.playback_speed = channel_time
		
		channeling = true
		return true
	else:
		pass
		return false
func pay_mana_cost(cost):
	current_mana -= cost
	if current_mana <0:
		current_mana = 0

func process_skills(delta):
	
	if in_battle:
		if Input.is_action_just_pressed("ui_skill1"):
			return process_action(1)
		
		if Input.is_action_just_pressed("ui_skill2"):
			return process_action(2)
		
		if Input.is_action_just_pressed("ui_skill3"):
			return process_action(3)
		
		if Input.is_action_just_pressed("ui_skill4"):
			return process_action(4)
	
		if Input.is_action_just_pressed("ui_speed"):
			if skill_controler.is_skill_ready(6,current_mana):
				skill_controler.play_skill(6)
				return false

		if Input.is_action_just_pressed("ui_potion"):
			if skill_controler.is_skill_ready(7,current_mana):
				skill_controler.play_skill(7)
				return false


	if Input.is_action_pressed("ui_attack"):
		if skill_controler.is_skill_ready(5,current_mana):
			
			skill_controler.play_skill(5)
			animation("Attack_")
			animation_player.playback_speed = Global.player_attack_speed * 0.3 / 0.7
			is_attacking = true
			return true
		else:
			return false
	return false


func give_health(heal):
	current_health += heal
	if current_health > max_health:
		current_health = max_health
func give_mana(mana_restore):
	current_mana += mana_restore
	if current_mana >max_mana:
		current_mana = max_mana

func attacking():
	if !is_attacking:
		is_attacking = true
		animation_player.playback_speed = Global.player_attack_speed
		animation("Attack_")
		return	true
	return false

func get_stats():
	Global.set_armor_and_damage()
	attack = Global.player_attack_damage
	armor = Global.player_armor
	attack_speed = Global.player_attack_speed
	max_mana = Global.player_max_mana
	max_health = Global.player_max_health
	speed = Global.player_speed * Global.player_speed_up_speed

func _physics_process(delta):
	if is_dead or has_won:
		return
	get_stats()
	
	gui.update_ui(current_health, max_health,current_mana, max_mana)
	
	if casting_finished:
		casting_finished = false
		skill_controler.play_skill(spell_preparing)
		animation_player.playback_speed = 1
	
	if !channeling:
		if process_skills(delta):
			return
	
	if channeling:
		return
	
	if is_attacking:
		return
	
	movement = moving()
	animation("Walking_")
	movement = move_and_slide(movement)
	#if collision:
	#	if "Demon" in collision.collider.name :
		#	_take_dmg(1)
			#if collision.collider.has_method("take_dmg"):
			#	collision.collider.take_dmg(10)
	


func get_current_movement_vector():
	return movement


func _on_AttackAreaEast_area_entered(area):
	if area.is_in_group("Demon"):
		area.get_parent().take_dmg(attack)

func _on_Demon_sendXPValue(XP):
	_get_XP(XP)

func _on_AttackAreaNorth_area_entered(area):
	if area.is_in_group("Demon"):
		area.get_parent().take_dmg(attack)

func _on_AttackAreaSouth_area_entered(area):
	if area.is_in_group("Demon"):
		area.get_parent().take_dmg(attack)

func _on_AttackAreaWest_area_entered(area):
	if area.is_in_group("Demon"):
		area.get_parent().take_dmg(attack)


func _on_AnimationPlayer_animation_finished(anim_name):
	if "Attack" in anim_name:
		is_attacking = false
		animation_player.playback_speed = 1
	if anim_name == "Death":
		pass
	if "Casting" in anim_name:
		if !is_dead and !has_won:
			skill_controler.play_skill(spell_preparing)
			animation_player.playback_speed = 1
			channeling = false
			is_attacking = false


func _on_AttackAreaWest_body_entered(body):
	if body.is_in_group("NPC"):
		body.get_parent().talk()


func _on_AttackAreaSouth_body_entered(body):
	if body.is_in_group("NPC"):
		body.get_parent().talk()


func _on_AttackAreaNorth_body_entered(body):
	if body.is_in_group("NPC"):
		body.get_parent().talk()


func _on_AttackAreaEast_body_entered(body):
	if body.is_in_group("NPC"):
		body.get_parent().talk()
