extends KinematicBody2D

# Werte, die jeder Soldat haben muss
var health = 200000
var max_health = 20000
var attack = 2
var max_speed = 200
var floatiness_factor = 0.05	#braucht ca.40 frames für top speed/ 80 frames für Richtungswechsel
var base_speed = 200
var armor = 20

# Diese bestimmen, wie weit Soldaten von Dämonen entfernt sein wollen (in Tiles angegeben)
var preffered_distance_to_player_in_cells = 12
var preffered_distance_to_demons_in_cells = 1
var max_distance_to_demons_in_cells = 8

var minimum_distance_to_demons_in_cells = 10
var preffered_distance_to_demons_in_pixels = 1000


# Varibelen um derzeitigen Status des Soldaten zu erkennen
var is_dead = false
var is_attacking = false
var its_time_between_attacks = false
var in_attack_range = 0
var wants_to_walk_to_demons = false
var wants_to_go_to_player = false
var too_close_to_demons = false


onready var game = get_tree().current_scene
onready var animator = $AnimationPlayer
onready var collision_test = $Awareness
onready var health_bar = $HealthBar
onready var sprite = $AnimatedSprite



onready var grid_parent_node = get_tree().current_scene.get_node("movementgrids")
onready var demon_grid = grid_parent_node.get_node("GridToDemons")
onready var movement_grid = grid_parent_node.get_node("GridToPlayer")



var invisibility_frames_on = false

# Eine Menge weitere Variablen zur Movementberechnung
var current_tile_x = 0
var current_tile_y = 0
var new_tile_x = 0
var new_tile_y = 0

var follow_Path_vector = Vector2.ZERO
var avoid_demons_vector = Vector2.ZERO
var new_movement_vector = Vector2.ZERO
var current_moving_direction = Vector2.ZERO
var own_position = Vector2(0,0)
var look_direction = "S"


var distance_to_next_demon_in_cells = 100
var distance_to_player_in_cells = 1000
var distance_to_player_in_px = 10000

var current_visible_health = 0		# Muss bei rdy = current_heath gesetzt werden

var fully_updated_health_bar = true
var current_health_bar_update_point = 0

#var demons_of_interest = []
#var max_number_of_aggroed_demons = 4
#var current_aggroed_demons = 0
#var demons_in_aggro_range = []
#var soldiers_in_range = []
#var aggro_field_enabled = true
#onready var signal_field_to_soldiers = $SoldiersCheckDemonsInAggroRange/CollisionShape2D
#onready var aggro_field = $AggroRange/CollisionShape2D
#onready var attack_area_east = $AttackAreaEast/CollisionShape2D
#onready var attack_area_west = $AttackAreaWest/CollisionShape2D
#onready var attack_area_north = $AttackAreaNorth/CollisionShape2D
#onready var attack_area_south = $AttackAreaSouth/CollisionShape2D

func heal(heal_amount):
	health += heal_amount
	if health > max_health:
		health = max_health

func update_health_bar(delta):
	current_health_bar_update_point += delta
	
	if current_health_bar_update_point >1:
		current_health_bar_update_point = 1
		fully_updated_health_bar = true
	var new_value = current_visible_health * (1 - current_health_bar_update_point) + health * current_health_bar_update_point
	
	if new_value == 0:
		health_bar.queue_free()
		fully_updated_health_bar = true
	else:
		health_bar.value = new_value

func _process(delta):
	own_position = self.global_position 
	max_speed = base_speed
	floatiness_factor = 0.075
	if !fully_updated_health_bar:
		update_health_bar(delta * 5)	# Braucht 0.2 Sekunden für das Update

	other_routines()

	if is_dead:
		return

	if is_attacking:
		attack_behaviour()
		return
	
	if can_attack():
		attack()
		return
	
	current_tile_x = floor(own_position.x / 48)
	current_tile_y = floor(own_position.y / 48)
	distance_to_player_in_cells = movement_grid.get_distance_of_cell(current_tile_x, current_tile_y)
	distance_to_next_demon_in_cells = demon_grid.get_distance_of_cell(current_tile_x, current_tile_y)
	
	does_he_want_to_walk_to_demons()
	does_he_want_to_walk_to_the_player()
	is_he_too_close_to_demons()
		#movement_direction(delta)
	movement_direction(delta)

func is_he_too_close_to_demons():
	if distance_to_next_demon_in_cells <= minimum_distance_to_demons_in_cells:
		too_close_to_demons = true
	if distance_to_next_demon_in_cells > minimum_distance_to_demons_in_cells:
		too_close_to_demons = false
	
	
func does_he_want_to_walk_to_the_player():
	if distance_to_player_in_cells > preffered_distance_to_player_in_cells:
		wants_to_go_to_player = true
	elif distance_to_player_in_cells <= preffered_distance_to_player_in_cells - 1:
		wants_to_go_to_player = false

func does_he_want_to_walk_to_demons():
	if distance_to_next_demon_in_cells <= preffered_distance_to_demons_in_cells:
		wants_to_walk_to_demons = false
		#print(distance_to_next_demon_in_cells)
		#current_moving_direction = Vector2.ZERO
		pass

	elif distance_to_next_demon_in_cells >= preffered_distance_to_demons_in_cells:
		wants_to_walk_to_demons = true

func can_attack():
	if distance_to_next_demon_in_cells <=1 and !its_time_between_attacks:
		return true
	return false


func attack_behaviour():
	consider_canceling_attack()
	more_attack_behaviour()

func more_attack_behaviour():
	pass

func consider_canceling_attack():
	var relevant_bodies = collision_test.get_overlapping_bodies()
	var relevant_areas = collision_test.get_overlapping_areas()
	
	for body in relevant_bodies:
		var minimum_distance = 2000
		if body.is_in_group("Player"):
			minimum_distance = 40
		elif body.is_in_group("Soldier"):
			minimum_distance = 20
		var distance = own_position.distance_to(body.global_position)
		
		if distance <= minimum_distance:
			cancel_attack()
			return
	
	for body in relevant_areas:
		if body.is_in_group("Soldier"):
			var minimum_distance = 20
			var distance = own_position.distance_to(body.global_position)
			if distance <= minimum_distance:
				cancel_attack()
				return


func cancel_attack():
	is_attacking = false
	its_time_between_attacks = true
	yield(get_tree().create_timer(1.0), "timeout")
	its_time_between_attacks = false

func movement_direction(delta):
	if movement_grid.is_there_a_wall(current_tile_x,current_tile_y):
		new_movement_vector = own_position - Vector2(current_tile_x * 48 + 24, current_tile_y *48 + 24)
		new_movement_vector =new_movement_vector.normalized() * max_speed
		move_and_collide(current_moving_direction * delta)
		own_position = self.global_position
		current_moving_direction = Vector2.ZERO
		return
		
	follow_path(delta)
	
	avoiding_demons()
	new_movement_vector = avoid_demons_vector + follow_Path_vector
	current_moving_direction += new_movement_vector.normalized() * max_speed * floatiness_factor
	if current_moving_direction.length()>max_speed:
		current_moving_direction = current_moving_direction.normalized() * max_speed

	is_move_allowed(delta)
	walking_animation()
	move_and_collide(current_moving_direction * delta)
	own_position = self.global_position

func follow_path(delta):
	if wants_to_go_to_player:
		follow_Path_vector = get_vector_from_relevant_grid(movement_grid)
	else:
		if wants_to_walk_to_demons:
			follow_Path_vector = get_vector_from_relevant_grid(demon_grid)
		else:
			#print("Not Player, NOT Demon Attack")
			if too_close_to_demons:
				follow_Path_vector = get_vector_from_relevant_grid(demon_grid).rotated(PI)
			else:
				follow_Path_vector = get_vector_from_relevant_grid(movement_grid).rotated(PI)



func get_vector_from_relevant_grid(relevant_grid):
	var cell_distance = relevant_grid.get_distance_of_cell(current_tile_x,current_tile_y)
	if cell_distance == 0:
		var player_position = relevant_grid.get_vector_direction(current_tile_x,current_tile_y, own_position)
		var new_vector =  player_position - own_position
		return new_vector.normalized()
	else:
		return relevant_grid.get_vector_direction(current_tile_x,current_tile_y, own_position)
func walking_animation():
	var type = "Walking_"
	var new_look_direction = look_direction

	var angle = (current_moving_direction.angle()) *180/3.14

	if angle <= 60 and angle >= -60:
		new_look_direction = "E"

	elif angle <= 120 and angle >= 60:
		new_look_direction = "S"


	elif angle >= -120 and angle <= -60:
		new_look_direction = "N"

	elif angle <= -120 or angle >= 120:
		new_look_direction = "W"

	if new_look_direction != look_direction:
		look_direction = new_look_direction
		animator.play(type + look_direction)

func avoiding_demons():
	avoid_demons_vector = Vector2.ZERO
	var relevant_areas = collision_test.get_overlapping_areas()
	var relevant_bodies = collision_test.get_overlapping_bodies()
	for area in relevant_areas:
		if area.is_in_group("Demon"):
			calc_per_body(area, minimum_distance_to_demons_in_cells)
		else:
			calc_per_body(area, 30)
	for body in relevant_bodies:
		calc_per_body(body, 2000)



func calc_per_body(body, max_distance):
	var body_position = body.global_position
	var distance = own_position.distance_to(body_position)
	var push_power = 0.1

	if body.is_in_group("Player"):
		if distance < 60:
			#print("TOO CLOSE")
			avoid_demons_vector = ((own_position - body_position).normalized() * 10)
			max_speed = base_speed * 2.5
			floatiness_factor = 0.25
			return
		else:
			if is_player_in_moving_direction(body_position, distance):
				avoid_demons_vector += ((own_position - body_position).normalized() * 0.7).rotated(PI/2)

	if distance < max_distance:
		push_power = 10
	else:
		push_power = 1/(0.1*(distance - (max_distance -1)))
	avoid_demons_vector += (own_position - body_position).normalized() * push_power



func is_player_in_moving_direction(body_position, distance):
	var expected_position = own_position + current_moving_direction.normalized() * distance

	if expected_position.distance_to(body_position) < 60:
		return true
	return false
func is_move_allowed(delta):
	var new_position = own_position + current_moving_direction * delta
	new_tile_x = floor(new_position.x / 48)
	new_tile_y = floor(new_position.y / 48)
	current_moving_direction = movement_grid.slide_at_wall(current_tile_x, current_tile_y, new_tile_x, new_tile_y, current_moving_direction)

func attack():
	pass
func other_routines():
	pass
func take_dmg(dmg):
	if is_dead:
		return
	var real_dmg = ceil(dmg * ( float(100) / (100 + armor)))

	health_bar.visible = true
	
	health -= real_dmg
	current_health_bar_update_point = 0
	fully_updated_health_bar = false
	current_visible_health = health_bar.value

	if health <= 0:
		death()
	else:
		sprite.modulate.a = 0.05
		invisibility_frames_on = true
		yield(get_tree().create_timer(0.1), "timeout")
		invisibility_frames_on = false
		sprite.modulate.a = 1
func death():
	pass
