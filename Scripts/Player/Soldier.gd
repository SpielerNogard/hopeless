extends "BaseSoldier.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var label = $Label
var demons_of_interest = []
var max_number_of_aggroed_demons = 4
var current_aggroed_demons = 0
var aggroed_demons = []

var soldiers_in_range = []



onready var aggro_field = $AggroField/CollisionShape2D

# Called when the node enters the scene tree for the first time.
func _ready():
	health = Global.einfacher_fusssoldat_health
	max_health = health
	current_visible_health = health
	attack = Global.einfacher_fusssoldat_attack_damage
	max_speed = Global.einfacher_fusssoldat_speed
	base_speed = max_speed
	floatiness_factor = 0.1
	preffered_distance_to_player_in_cells = 8
	preffered_distance_to_demons_in_cells = 0
	max_distance_to_demons_in_cells = 8
	animator.play("Walking_S")
	game.add_soldier(self)
	minimum_distance_to_demons_in_cells = 0
	minimum_distance_to_demons_in_cells = 20
	armor = Global.einfacher_fusssoldat_armor
	health_bar.max_value = health
	health_bar.value = health
	health_bar.visible = false

func other_routines():
	label.text = str(current_aggroed_demons) + "/" + str(max_number_of_aggroed_demons)


func attack():
	animator.play("Attack_" + look_direction)

	is_attacking = true
	current_moving_direction = Vector2.ZERO



func death():
	is_dead = true
	for demon in aggroed_demons:
		if is_instance_valid(demon):
			demon.undo_aggro()
		#demon.undo_aggro()
	
	aggro_field.set_deferred("disabled", false)

	#$CollisionShape2D.queue_free()
	game.remove_soldier(self)
	animator.play("Death")
	

func _on_Area2D2_area_entered(area):
	if area.is_in_group("Demon"):
		self.in_attack_range +=1

		area.get_parent().in_attack_range(1)
	pass # Replace with function body.

func _on_Area2D2_area_exited(area):
	if area.is_in_group("Demon"):
		self.in_attack_range -=1
		area.get_parent().in_attack_range(-1)
	pass # Replace with function body.

func add_aggro_demon(demon):
	current_aggroed_demons += 1
	aggroed_demons.append(demon)
	demons_of_interest.append(demon)

func remove_aggro_demon(demon):
	current_aggroed_demons -= 1
	demons_of_interest.erase(demon)
	aggroed_demons.erase(demon)
	
	if is_instance_valid(aggro_field) == true:
		aggro_field.set_deferred("disabled",false)

func recount_demons():
	if current_aggroed_demons >= max_number_of_aggroed_demons:
		return 0
	else:
		var newly_aggroed_demons = 0
		var demons_to_consider_areas = $AggroField.get_overlapping_areas()
		for demon_area in demons_to_consider_areas:
			if demon_area.is_in_group("Demon"):
				var demon = demon_area.get_parent()
				if demon.make_aggro(self):
					current_aggroed_demons += 1
					newly_aggroed_demons += 1
					if current_aggroed_demons >= max_number_of_aggroed_demons:
						return newly_aggroed_demons
		return newly_aggroed_demons


func _on_AttackAreaSouth_area_entered(area):
	var object = area.get_parent()
	if object.has_method("take_dmg"):
		object.take_dmg(attack)
	pass # Replace with function body.

func _on_AttackAreaWest_area_entered(area):
	var object = area.get_parent()
	if object.has_method("take_dmg"):
		object.take_dmg(attack)

func _on_AttackAreaNorth_area_entered(area):
	var object = area.get_parent()
	if object.has_method("take_dmg"):
		object.take_dmg(attack)

func _on_AttackAreaEast_area_entered(area):
	var object = area.get_parent()
	if object.has_method("take_dmg"):
		object.take_dmg(attack)

func _on_AggroField_area_entered(area):
	if is_dead:
		return
	#if area.call_deferred("free"):
	#	return
	if current_aggroed_demons>= max_number_of_aggroed_demons:
		aggro_field.set_deferred("disabled", true)
	elif area.is_in_group("Demon"):
		var demon = area.get_parent()
		if demon.make_aggro(self):
			add_aggro_demon(demon)
			if current_aggroed_demons>= max_number_of_aggroed_demons:
				aggro_field.set_deferred("disabled", true)
				return


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Death":
		var all_areas_in_range = $AggroField.get_overlapping_areas()
		var demons_to_aggro = current_aggroed_demons
		for area in all_areas_in_range:
			if area.is_in_group("AggroField"):
				demons_to_aggro -= area.get_parent().recount_demons()
				if demons_to_aggro <= 0:
					queue_free()
		
		queue_free()
	if "Attack" in anim_name:
		animator.play("Walking_"+look_direction)
		is_attacking = false
		its_time_between_attacks = true
		yield(get_tree().create_timer(1.0), "timeout")
		its_time_between_attacks = false

