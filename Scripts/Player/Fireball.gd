extends AnimatedSprite

var dmg = 5
var movement_vector = Vector2.ZERO
var correction_vector = Vector2.ZERO
var floatiness_factor = 1
var my_mage


var own_position = Vector2.ZERO
var max_speed = 300
var flying = false

var scale_multiplier = 0.5

var self_destructing = false
var own_scale = Vector2.ZERO
var scale_factor = 0


var my_target
var exploded = false


onready var explosion_hitbox = $ExplosionHitBox/CollisionShape2D
onready var timer = $Timer
onready var explosion_node = get_parent().get_node("Explosion")


func _ready():
	pass
#	set_physics_process(false)

func set_my_mage(soldier):
	my_mage = soldier

func fire_ball_go():
	flying = true
	timer.start(2)

func self_destruct():
	self_destructing = true
	flying = true
	#print("SELF DESTRUCTED")

func scale_up(new_scale):
	
	own_scale = Vector2(new_scale * scale_multiplier, new_scale * scale_multiplier)
	scale_factor = new_scale * scale_multiplier
	
	self.scale = own_scale

func set_dmg(new_dmg):
	dmg = new_dmg

func set_target(target):
	my_target = target

func _physics_process(delta):
	if !exploded:
		self.rotation_degrees = movement_vector.angle() * 180/3.14
	
	if self_destructing:
		if exploded:
			return
		own_scale.x -= delta
		own_scale.y -= delta
		if own_scale.x <=0 or own_scale.y <= 0:
			queue_free()
			return
		else:
			self.scale = own_scale
			scale_factor = own_scale.x
			self.position += movement_vector * delta
			return
	
	if is_instance_valid(my_target) == false:
		#print("CONSIDER SELF DESTRUCT")
		self_destruct()
		return
	else:
		if flying:
			correction_vector =  my_target.global_position - self.global_position
			movement_vector = correction_vector.normalized() * max_speed
#			print(movement_vector)
			self.position += movement_vector * delta
			return
		if !exploded:
			self.rotation_degrees = movement_vector.angle() * 180/3.14


func _on_Area2D_area_entered(area):
	if area.is_in_group("Demon"):
		#area.get_parent().take_dmg(dmg)
		explosion_hitbox.set_deferred("disabled", false)
		exploded = true
		self.play("EXPLOSION")
		
		
		if Settings.enable_soundeffekte and !explosion_node.playing:
			explosion_node.volume_db = Settings.soundeffekte_volume
			explosion_node.play()
		self.offset = Vector2.ZERO
		self.scale = Vector2(scale_factor / scale_multiplier, scale_factor / scale_multiplier)
		
		yield(get_tree().create_timer(0.05),"timeout")
		explosion_hitbox.set_deferred("disabled", true)


func _on_AnimatedSprite_animation_finished():
	if self.animation == "EXPLOSION":
		queue_free()


func _on_ExplosionHitBox_area_entered(area):
	if area.is_in_group("Demon"):
		var demon = area.get_parent()
		demon.take_dmg(dmg * scale_factor)
		demon.make_aggro(my_mage)
	

func _on_Timer_timeout():
	self_destruct()
