extends "BaseSoldier.gd"


export var fireball_fire_ready = false
export var fireball_size = 0.0

var fireball_scene = preload("res://Scenes/Players/Fire_Ball.tscn")
var target
onready var attack_zone = $AttackRange/CollisionShape2D

var my_fireball
var can_currently_attack = false

func remove_aggro_demon(demon):
	return
func _ready():
	health = Global.Mage_health
	max_health = health
	current_visible_health = health
	attack = Global.Mage_attack_damage
	max_speed = Global.Mage_speed
	base_speed = max_speed
	armor = Global.Mage_armor
	
	floatiness_factor = 1
	preffered_distance_to_player_in_cells = 5
	preffered_distance_to_demons_in_cells = 600000
	max_distance_to_demons_in_cells = 8
	
	
	
	game.add_soldier(self)

	health_bar.max_value = health
	health_bar.value = health
	health_bar.visible = false
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func attack():
	create_fire_ball()
	animator.play("Casting_" + look_direction)
	is_attacking = true
	current_moving_direction = Vector2.ZERO
	can_currently_attack = false
	attack_zone.set_deferred("disabled", true)


func _on_AttackRange_area_entered(area):
	target = area.get_parent()
	can_currently_attack = true

func more_attack_behaviour():
	if is_instance_valid(my_fireball) == false:
		return
	if fireball_fire_ready:
		my_fireball.fire_ball_go()
	else:
		my_fireball.scale_up(fireball_size)
		
	

func can_attack():
	if can_currently_attack:
		if !its_time_between_attacks:
			return true
	return false
	
func cancel_attack():
	is_attacking = false
	var my_childrens = get_children()
	#print("CANCEL ATTACK")
	if is_instance_valid(my_fireball) == true:
		my_fireball.self_destruct()
	
	its_time_between_attacks = true
	yield(get_tree().create_timer(1.0), "timeout")
	its_time_between_attacks = false
	attack_zone.set_deferred("disabled", false)
	
func create_fire_ball():
	var my_target = find_best_target()
	
	
	if is_instance_valid(my_target)==true:
		my_fireball = fireball_scene.instance()
		get_parent().add_child(my_fireball)
		my_fireball.set_dmg(attack)
		my_fireball.global_position = self.global_position - Vector2(0, 40)
		my_fireball.scale_up(0)
		my_fireball.set_target(my_target)
		my_fireball.set_my_mage(self)
		where_to_look(my_target)
		animator.play("Casting_" + look_direction)
		
		
	else:
		print(attack_zone.get_parent().get_overlapping_areas())

func where_to_look(my_target):
	var type = "Walking_"
	var vector_length_one =  my_target.global_position - self.global_position
	
	var angle = (vector_length_one.angle()) * 180/3.14

	if angle <= 60 and angle >= -60:
		look_direction = "E"

	elif angle <= 120 and angle >= 60:
		look_direction = "S"


	elif angle >= -120 and angle <= -60:
		look_direction = "N"

	elif angle <= -120 or angle >= 120:
		look_direction = "W"


func find_best_target():
	var best_target
	var shortest_distance = 10000
	var aggroed_demon_found = false
	
	var all_possible_targets = attack_zone.get_parent().get_overlapping_areas()
	
	for possible_target in all_possible_targets:
		if possible_target.is_in_group("Demon"):
			var demon = possible_target.get_parent()
			
			var distance = own_position.distance_to(demon.global_position)
			var is_he_aggroed = demon.get_aggro()
			
			if !aggroed_demon_found and is_he_aggroed:
				shortest_distance = distance
				aggroed_demon_found = true
				best_target = possible_target
			
			elif (aggroed_demon_found and is_he_aggroed) or (!aggroed_demon_found and !is_he_aggroed):
				if distance < shortest_distance:
					shortest_distance = distance
					best_target = possible_target

	
	return best_target
func _on_Awareness_area_entered(area):
	if area.is_in_group("Demon"):
		in_attack_range += 1


func _on_Awareness_area_exited(area):
	if area.is_in_group("Demon"):
		in_attack_range -= 1

func death():
	is_dead = true
	game.remove_soldier(self)
	
	collision_test.queue_free()
	$CollisionShape2D.queue_free()
	animator.play("Death")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Death":
		if is_instance_valid(my_fireball):
			my_fireball.self_destruct()
		
		
		queue_free()
	if "Casting" in anim_name:
		#create_fire_ball()
		animator.play("Walking_"+look_direction)
		is_attacking = false
		its_time_between_attacks = true
		yield(get_tree().create_timer(1.0), "timeout")
		its_time_between_attacks = false
		attack_zone.set_deferred("disabled", false)
