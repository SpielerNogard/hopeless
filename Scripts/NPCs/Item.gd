extends Control

export (NodePath) var item_icon_node
export (NodePath) var item_name_node
export (NodePath) var item_level_node

export (NodePath) var gold_kosten_node
export (NodePath) var iron_kosten_node
export (NodePath) var stone_kosten_node

export (NodePath) var left_sword_node
export (NodePath) var right_sword_node
export (NodePath) var left_shield_node
export (NodePath) var right_shield_node

export (NodePath) var right_text_node
export (NodePath) var left_text_node

export (NodePath) var full_attack_node
export (NodePath) var current_attack_node
export (NodePath) var next_attack_node

export (NodePath) var full_armor_node
export (NodePath) var current_armor_node
export (NodePath) var next_armor_node

export (NodePath) var full_health_node
export (NodePath) var current_health_node
export (NodePath) var next_health_node

export (NodePath) var full_mana_node
export (NodePath) var current_mana_node
export (NodePath) var next_mana_node

export (NodePath) var full_attack_speed_node
export (NodePath) var current_attack_speed_node
export (NodePath) var next_attack_speed_node

export (NodePath) var full_movement_speed_node
export (NodePath) var current_movement_speed_node
export (NodePath) var next_movement_speed_node


onready var item_bougth_sound = $itemBougthSound
onready var full_attack = get_node(full_attack_node)
onready var current_attack = get_node(current_attack_node)
onready var next_attack = get_node(next_attack_node)

onready var full_armor = get_node(full_armor_node)
onready var current_armor = get_node(current_armor_node)
onready var next_armor = get_node(next_armor_node)

onready var full_health = get_node(full_health_node)
onready var current_health = get_node(current_health_node)
onready var next_health = get_node(next_health_node)

onready var full_mana = get_node(full_mana_node)
onready var current_mana = get_node(current_mana_node)
onready var next_mana = get_node(next_mana_node)

onready var full_attack_speed = get_node(full_attack_speed_node)
onready var current_attack_speed = get_node(current_attack_speed_node)
onready var next_attack_speed = get_node(next_attack_speed_node)

onready var full_movement_speed = get_node(full_movement_speed_node)
onready var current_movement_speed = get_node(current_movement_speed_node)
onready var next_movement_speed = get_node(next_movement_speed_node)




onready var item_icon = get_node(item_icon_node)
onready var item_name_text = get_node(item_name_node)
onready var item_level_text = get_node(item_level_node)

onready var gold_kosten_text = get_node(gold_kosten_node)
onready var iron_kosten_text = get_node(iron_kosten_node)
onready var stone_kosten_text = get_node(stone_kosten_node)

onready var left_sword_icon = get_node(left_sword_node)
onready var right_sword_icon = get_node(right_sword_node)

onready var left_shield_icon = get_node(left_shield_node)
onready var right_shield_icon = get_node(right_shield_node)

onready var left_text = get_node(left_text_node)
onready var right_text = get_node(right_text_node)

export (NodePath) var buy_button_text_node
onready var buy_button_text = get_node(buy_button_text_node)

var button_down_color = Color(1,1,1,1)
var button_up_color = Color(0,0,0,1)





var item_name = "Lederhelm"

var gold_kosten = 100
var iron_kosten = 100
var stone_kosten = 100
var icon = preload("res://Assets/GUI/Items/armor/armor_02.png")
var item_id = 0
var max_visible = 4


signal item_bougth
signal failed_to_buy
signal mouse_enter


# Called when the node enters the scene tree for the first time.
func _ready():
	var random_number = randi() % 2

# Nur Schild items müssen damit umgeändert werden

	var me = Global.inventory[item_id]
	var me_item = me[0]
	var me_equipped = me[1]
	var my_icon = load(me_item.icon_path)
	item_icon.set_texture(my_icon)
		

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#Itemaufbau: [Name,Icon,Level,Eisen pro lvl,gold pro lvl, stone pro lvl,Armor, dmg, slotnr, leben, attack_speed, mana,
	var me = Global.inventory[item_id]
	var me_item = me[0]
	var me_equipped = me[1]
	#var my_picture = preload(me_item[1])

	#item_icon_text.set_texture(me_item[1])
	item_level_text.set_text("+"+str(me_item.level))
	var gold_kosten = me_item.base_gold + me_item.gold_per_lvl * me_item.level
	var iron_kosten = me_item.base_iron + me_item.iron_per_lvl * me_item.level
	var stone_kosten = me_item.base_stone + me_item.stone_per_lvl * me_item.level
	
	gold_kosten_text.set_text(str(gold_kosten))
	iron_kosten_text.set_text(str(iron_kosten))
	stone_kosten_text.set_text(str(stone_kosten))

	var stats_armor = me_item.base_armor + me_item.armor_per_lvl * me_item.level
	var stats_damage = me_item.base_attack + me_item.attack_per_lvl * me_item.level
	var stats_attack_speed = me_item.base_attack_speed + me_item.attack_speed_per_lvl * me_item.level
	var stats_health = me_item.base_health + me_item.health_per_lvl * me_item.level
	var stats_movement_speed =  me_item.base_movement_speed + me_item.movement_speed_per_lvl * me_item.level
	var stats_mana = me_item.base_mana + me_item.mana_per_lvl * me_item.level
	
	var currently_visible = 0
	
	if stats_damage > 0 and currently_visible < 4:
		full_attack.visible = true
		current_attack.set_text(str(stats_damage))
		next_attack.set_text(str(stats_damage + me_item.attack_per_lvl))
		currently_visible += 1
	else:
		full_attack.visible = false
	
	if stats_armor > 0 and currently_visible < 4:
		full_armor.visible = true
		currently_visible += 1
		current_armor.set_text(str(stats_armor))
		next_armor.set_text(str(stats_armor +me_item.armor_per_lvl))
	else:
		full_armor.visible = false

	if stats_health >0 and currently_visible < 4:
		full_health.visible = true
		currently_visible += 1
		current_health.set_text(str(stats_health))
		next_health.set_text(str(stats_health + me_item.health_per_lvl))
	else:
		full_health.visible = false
		
	if stats_mana >0 and currently_visible < 4:
		full_mana.visible = true
		currently_visible += 1
		current_mana.set_text(str(stats_mana))
		next_mana.set_text(str(stats_mana + me_item.mana_per_lvl))
	else:
		full_mana.visible = false
		
	if stats_attack_speed >0 and currently_visible < max_visible:
		full_attack_speed.visible = true
		currently_visible += 1
		current_attack_speed.set_text(str(stats_attack_speed))
		next_attack_speed.set_text(str(stats_attack_speed + me_item.attack_speed_per_lvl))
	else:
		full_attack_speed.visible = false
		
	if stats_movement_speed >0 and currently_visible < max_visible:
		full_movement_speed.visible = true
		currently_visible +=1
		current_movement_speed.set_text(str(stats_movement_speed))
		next_movement_speed.set_text(str(stats_movement_speed + me_item.movement_speed_per_lvl))
	else:
		full_movement_speed.visible = false
		
	item_icon.set_texture(load(me_item.icon_path))
	item_name_text.set_text(me_item.name)
	item_level_text.set_text("+"+str(me_item.level))
	


func lvl_up():
	if Global.lvl_up_item(item_id):
		#emit_signal("item_bougth")
		if Settings.enable_soundeffekte:
			item_bougth_sound.volume_db = Settings.soundeffekte_volume
			item_bougth_sound.play()
	else:
		emit_signal("failed_to_buy")


func set_id(zahl):
	item_id = zahl
	var me = Global.inventory[item_id]
	var me_item = me[0]
	var me_equipped = me[1]

		


func _on_Button_pressed():
	lvl_up()


func _on_TextureButton_pressed():
	lvl_up()


func _on_TextureButton_button_down():
	buy_button_text.modulate = button_down_color
	buy_button_text.margin_bottom = 4


func _on_TextureButton_button_up():
	buy_button_text.modulate = button_up_color
	buy_button_text.margin_bottom = 0


func _on_Verbessern_mouse_entered():
	emit_signal("mouse_enter")
