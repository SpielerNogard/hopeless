extends Control

export (NodePath) var item_icon_node
export (NodePath) var anzahl_text_node
export (NodePath) var level_text_node

export (NodePath) var button_node

export (NodePath) var Shop_items_node
export (NodePath) var Equipment_Stats_node


export (NodePath) var attack_node
export (NodePath) var armor_node
export (NodePath) var health_node
export (NodePath) var mana_node
export (NodePath) var attack_speed_node
export (NodePath) var movement_speed_node

export (NodePath) var full_health_potion_node
export (NodePath) var health_potion_strength_node

export (NodePath) var full_mana_potion_node
export (NodePath) var mana_potion_strength_node


onready var full_health_potion = get_node(full_health_potion_node)
onready var health_potion_strength = get_node(health_potion_strength_node)

onready var full_mana_potion = get_node(full_mana_potion_node)
onready var mana_potion_strength = get_node(mana_potion_strength_node)


onready var attack = get_node(attack_node)
onready var armor = get_node(armor_node)
onready var health = get_node(health_node)
onready var mana = get_node(mana_node)
onready var attack_speed = get_node(attack_speed_node)
onready var movement_speed = get_node(movement_speed_node)



onready var item_icon = get_node(item_icon_node)
onready var anzahl_text = get_node(anzahl_text_node)


onready var level_text = get_node(level_text_node)

onready var button = get_node(button_node)


onready var Shop_items = get_node(Shop_items_node)
onready var Equipment_Stats = get_node(Equipment_Stats_node)


signal equipped
signal mouse_enter

var my_name = ""

var shop_item = true
var my_stats

func set_as_shop_item (is_shop_item):
	if is_shop_item:
		Shop_items.visible = true
		Equipment_Stats.visible = false
	else:
		Shop_items.visible = false
		Equipment_Stats.visible = true

func set_icon(icon):
	item_icon.set_texture(icon)

#Name wird nicht mehr angezeigt
func set_name(name):
	my_name = name
	i_am_equipped()
	
	

func set_anzahl(anzahl):
	anzahl_text.set_text(str(anzahl))
	anzahl_text.visible = true

func set_shop_stats(stats):
	my_stats = stats
	shop_item = true
	Shop_items.visible = true
	Equipment_Stats.visible = false
	
	
	if stats.equipped:
		button.visible = false
	var item_anzahl = stats.owned

	var icon_path = stats.icon_path
	var anzahl = stats.owned

	item_icon.set_texture(load(stats.icon_path))
	
	if stats.type == "health":
		full_mana_potion.visible = false
		full_health_potion.visible = true
		health_potion_strength.set_text(str(stats.strength) +"%")
		anzahl_text.set_text(str(Global.new_health_potion.owned))
		
	if stats.type == "mana":
		full_health_potion.visible = false
		full_mana_potion.visible = true
		mana_potion_strength.set_text(str(stats.strength) + "%")
		anzahl_text.set_text(str(Global.new_mana_potion.owned))

# Hier dann entweder Shield oder Sword visible/invisible machen
func set_equipment_stats(stats):
	my_stats = stats
	shop_item = false
	Shop_items.visible = false
	Equipment_Stats.visible = true
	
	my_name = stats.name
	i_am_equipped()
	item_icon.set_texture(load(stats.icon_path))
	var lvl = stats.level
	level_text.set_text("+"+str(lvl))

	
	
	attack.set_text(str(stats.base_attack + stats.attack_per_lvl * stats.level))
	armor.set_text(str(stats.base_armor + stats.armor_per_lvl * stats.level ))
	health.set_text(str(stats.base_health + stats.health_per_lvl * stats.level ))
	mana.set_text(str(stats.base_mana + stats.mana_per_lvl * stats.level ))
	attack_speed.set_text(str(stats.base_attack_speed + stats.attack_speed_per_lvl * stats.level ))
	movement_speed.set_text(str(stats.base_movement_speed + stats.movement_speed_per_lvl * stats.level)) 


# Diese Funktion mit true/false aufrufen
func set_button_visible(is_visible):
	button.visible = is_visible

func _on_Mage_Ausbilden_pressed():
	pass # Replace with function body.

func i_am_equipped():
	var Ergebnis = Global.is_equipped(my_name)
	if Ergebnis == true:
		set_button_visible(false)
	else:
		set_button_visible(true)



func _on_Ausrsten_pressed():
	if !shop_item:
		Global.equip_item(my_name)
	else:
		Global.equip_potion()
	emit_signal("equipped")
	
func _process(delta):
	if !shop_item:
		i_am_equipped()
	elif my_stats != null:
		if my_stats.type == "health":
			if Global.new_health_potion.equipped:
				button.visible = false
			else:
				button.visible = true
		elif my_stats.type == "mana":
			if Global.new_mana_potion.equipped:
				button.visible = false
			else:
				button.visible = true



func _on_Ausrsten_mouse_entered():
	emit_signal("mouse_enter")
