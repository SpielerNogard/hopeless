extends Node

export (NodePath) var item_list_node
export (NodePath) var gold_ress_node
export (NodePath) var iron_habe_node
export (NodePath) var stone_habe_node

onready var hboxcontainer = $CanvasLayer/NinePatchRect/VBoxContainer/HBoxContainer
onready var item_list = get_node(item_list_node)
onready var gold_ress = get_node(gold_ress_node)
onready var iron_habe = get_node(iron_habe_node)
onready var stone_habe = get_node(stone_habe_node)


onready var button_hover_sound = $ButtonHoverSound
onready var button_pressed_sound = $ButtonPressedSound
onready var error_sound = $ErrorSound


var item_scene = preload("res://Scenes/NPCS/Item.tscn")
var button_pressed = ""
onready var smith_music = $SmithMusik

func play_button_pressed_sound():
	if Settings.enable_soundeffekte:
		button_pressed_sound.volume_db = Settings.soundeffekte_volume - 7
		button_pressed_sound.play()

func _ready():
	Global.schmiede_besucht = true
	if Settings.enable_music:
		get_tree().current_scene.get_node("Musik").stop()
		smith_music.volume_db = Settings.music_volume
		smith_music.play()
	var inventory_length = len(Global.inventory)
	add_new_item(inventory_length)
	
func set_ressources():
	iron_habe.set_text(str(Global.iron))
	gold_ress.set_text(str(Global.gold))
	stone_habe.set_text(str(Global.stone))

func add_new_item(anzahl):
	for a in range(anzahl):
		var new_item = item_scene.instance()
		item_list.add_child(new_item)
		new_item.set_id(a)
		new_item.connect("item_bougth",self,"item_bougth_func")
		new_item.connect("failed_to_buy",self,"play_error_sound")
		new_item.connect("mouse_enter",self,"play_hover_sound")

func item_bougth_func():
	print("HEY")


func remove_all_children():
	pass
	for child in item_list.get_children():
		item_list.remove_child(child)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	set_ressources()
	return
	var inventory_length = len(Global.inventory)
	var anzahl_items_in_schmiede = item_list.get_child_count()
	
	if anzahl_items_in_schmiede < inventory_length:
		add_new_item(inventory_length-anzahl_items_in_schmiede)
	elif anzahl_items_in_schmiede > inventory_length:
		remove_all_children()
	for zahl in range(inventory_length):
		var current_item = item_list.get_child(zahl)
		current_item.set_id(zahl)
		
	set_ressources()


func _on_Schlieen_pressed():
	button_pressed = "schließen"
	smith_music.stop()
	
	get_tree().current_scene.get_node("Player").set_in_town(false)

	if Settings.enable_music:
		get_tree().current_scene.get_node("Musik").volume_db = Settings.music_volume
		get_tree().current_scene.get_node("Musik").play()

	self.visible = false
	if Settings.enable_soundeffekte:
		play_button_pressed_sound()
	else:
		queue_free()

func _on_ButtonPressedSound_finished():
	if button_pressed == "schließen":
		queue_free()
func play_error_sound():
	if Settings.enable_soundeffekte:
		error_sound.volume_db = Settings.soundeffekte_volume - 7
		error_sound.play()
func play_hover_sound():
	if Settings.enable_soundeffekte:
		button_hover_sound.volume_db = Settings.soundeffekte_volume
		button_hover_sound.play()

func _on_Schlieen_mouse_entered():
	play_hover_sound()
