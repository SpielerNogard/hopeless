extends Control

export (NodePath) var gold_node
export (NodePath) var shop_items_node
export (NodePath) var item_list_node

onready var gold_text = get_node(gold_node)
onready var Shop_items = get_node(shop_items_node)
onready var button_hover_sound = $ButtonHoverSound
onready var button_pressed_sound = $ButtonPressedSound
onready var error_sound = $ErrorSound
onready var item_bought_sound = $ItemBoughtSound
onready var shop_music = $ShopMusik


var shop_item_scene = preload("res://Scenes/NPCS/Shop_item.tscn")

var item_scene = preload("res://Scenes/NPCS/Inventory_Item.tscn")
onready var item_list = get_node(item_list_node)

var button_pressed = ""

func set_gold():
	gold_text.set_text(str(Global.gold))
	
func remove_all_children():
	pass
	for child in Shop_items.get_children():
		Shop_items.remove_child(child)
		
func add_new_item(anzahl):
	for a in range(anzahl):
		var new_item = shop_item_scene.instance()
		Shop_items.add_child(new_item)
		new_item.connect("item_bought",self,"item_bougth")
		new_item.connect("failed_to_buy",self,"failed_to_buy")
		new_item.connect("mouse_enter",self,"play_hover_sound")

func play_hover_sound():
	if Settings.enable_soundeffekte:
		button_hover_sound.volume_db = Settings.soundeffekte_volume
		button_hover_sound.play()

func failed_to_buy():
	play_error_sound()

func set_items():
	var shop_inventory_length = len(Global.Shop_Iventory)
	var anzahl_items_in_Shop = Shop_items.get_child_count()
	
	if anzahl_items_in_Shop < shop_inventory_length:
		add_new_item(shop_inventory_length - anzahl_items_in_Shop)

	elif anzahl_items_in_Shop > shop_inventory_length:
		remove_all_children()

	for zahl in range(shop_inventory_length):
		var current_item = Shop_items.get_child(zahl)
		current_item.set_id(zahl)
func _ready():
	Global.shop_besucht = true
	if Settings.enable_music:
		get_tree().current_scene.get_node("Musik").stop()
		shop_music.volume_db = Settings.music_volume
		shop_music.play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	set_gold()
	set_items()

func play_error_sound():
	if Settings.enable_soundeffekte:
		error_sound.volume_db = Settings.soundeffekte_volume - 7
		error_sound.play()

func item_bougth():
	play_button_pressed_sound()

func play_button_pressed_sound():
	if Settings.enable_soundeffekte:
		button_pressed_sound.volume_db = Settings.soundeffekte_volume - 7
		button_pressed_sound.play()

func _on_Schlieen_pressed():
	button_pressed = "schließen"
	shop_music.stop()
	
	get_tree().current_scene.get_node("Player").set_in_town(false)
	
	if Settings.enable_music:
		get_tree().current_scene.get_node("Musik").volume_db = Settings.music_volume
		get_tree().current_scene.get_node("Musik").play()
	
	self.visible = false
	if Settings.enable_soundeffekte:
		play_button_pressed_sound()
	else:
		queue_free()

func _on_Schlieen_mouse_entered():
	play_hover_sound()


func _on_ButtonPressedSound_finished():
	if button_pressed == "schließen":
		queue_free()
