extends Node
var Shop = preload("res://Scenes/NPCS/Shop.tscn")
var Schmiede = preload("res://Scenes/NPCS/Schmiede.tscn")
var Kaserne = preload("res://Scenes/Overworld/Kaserneinnen.tscn")

onready var NPCTEXT = get_node("NPCText")
onready var my_Sprite = get_node("AnimatedSprite")
var NPC_name = "Name"
var NPC_texts = ["Hi, ich bin Harry. Ich bin der Schmied im Dorf, ich brauche deine Hilfe","Besorge mir bitte etwas Eisen aus der Mine","Quest: Besorge 10 Eisen"]
var NPC_quest = 0
var has_quest = true
var actual_npc_text_nr = 0
var parent = null
var has_path = false

#onready var current_node = get_parent().get_node(NPC_name+"_Path")
#onready var path_follow_group = get_parent().get_node("ActualPaths")
#onready var path_to_follow = path_follow_group.get_node("Raidon_Quastead")
var current_node
var path_follow_group
var path_to_follow

var points_to_follow : PoolVector2Array
var current_follow_point_index
var current_follow_point

var increment_type
var path_finished = false
var movement_direction = Vector2.ZERO
var speed = 100
var min_distance_next_point = 5

var movement_x = ""
var movement_y = ""
var where_to_move = "south"
var where_current_move = ""
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# Called when the node enters the scene tree for the first time.
func _process(delta):
	search_my_path()
	if has_path == true:
		get_next_point()
		actually_follow_path(delta)
		#animate_me()
		animation()
	else:
		my_Sprite.play("default")

func animate_me():
	if where_current_move != where_to_move:
		my_Sprite.play("walk_"+str(where_to_move.to_lower()))
		where_current_move = where_to_move
func search_my_path():
	var parent = get_parent()
	if parent.name != "root":
		if "path" in parent.name.to_lower():
			if has_path == false:
				has_path = true
				path_to_follow = parent
				points_to_follow = path_to_follow.curve.get_baked_points()
				self.global_position = points_to_follow[0]
				increment_type = 1
				current_follow_point_index = 0
				current_follow_point = points_to_follow[0]
			
func _ready():
	pass
	if "random" in name:
		var my_NPC = Global.give_random_npc()
		NPC_name =my_NPC[0]
		NPC_texts = my_NPC[1]
		NPC_quest = my_NPC[2]
		has_quest = my_NPC[3]
	else:
		var my_NPC = Global.give_NPC_for_name(name)
		NPC_name =my_NPC[0]
		NPC_texts = my_NPC[1]
		NPC_quest = my_NPC[2]
		has_quest = my_NPC[3]

	
func get_name():
	return(NPC_name)
func get_actual_text():
	return(NPC_texts[actual_npc_text_nr])
func next_text():
	var max_anzahl = len(NPC_texts)-1
	actual_npc_text_nr += 1
	if actual_npc_text_nr > max_anzahl:
		actual_npc_text_nr = max_anzahl
		if has_quest == true:
			NPCTEXT.show_quest_buttons()
	#dialog_box.show()
func call_npc():
	pass 
	
func open_schmiede():
	var erstellteMenu = Schmiede.instance()
	add_child(erstellteMenu)
	#get_tree().paused = true

func open_shop():
	var erstellteMenu = Shop.instance()
	add_child(erstellteMenu)
	#get_tree().paused = true

func open_kaserne():
	var erstellteMenu = Kaserne.instance()
	add_child(erstellteMenu)
	#get_tree().paused = true	
	
func talk():
	if name == "Schmiede":
		open_schmiede()
	elif name == "Shop":
		open_shop()
	elif name == "Kaserne":
		open_kaserne()
	else:
		NPCTEXT.start_dialog(NPC_name,NPC_texts)
	
func follow_path(delta):
	get_next_point()
	actually_follow_path(delta)

func actually_follow_path(delta):
	movement_direction =  points_to_follow[current_follow_point_index] - self.global_position
	self.global_position += movement_direction.normalized() * speed * delta

	if movement_direction.x < 0 and movement_direction.x < movement_direction.y:
		movement_x = "West"
		where_to_move = "west"
	elif movement_direction.x > 0 and movement_direction.x > movement_direction.y:
		movement_x = "East"
		where_to_move = "east"
	elif movement_direction.y <0 and movement_direction.x > movement_direction.y:
		movement_y = "North"
		where_to_move = "north"
	elif movement_direction.y >0 and movement_direction.x < movement_direction.y:
		movement_y = "South"
		where_to_move = "south"


		
func get_next_point():
	var distance_to_next_point = self.global_position.distance_to(current_follow_point)
	if distance_to_next_point < min_distance_next_point:
		current_follow_point_index += increment_type
		
		if current_follow_point_index <0 or current_follow_point_index >= points_to_follow.size() - 1:
			path_finished = true
			current_follow_point_index = 0
		current_follow_point = points_to_follow[current_follow_point_index]
		get_next_point()
# Called every frame. 'delta' is the elapsed time since the previous frame.
func animation():
	var animation_direction = "South"
	var angle = (movement_direction.angle()) *180/3.14

	if angle <= 60 and angle >= -60:
		animation_direction = "East"

	elif angle <= 120 and angle >= 60:
		animation_direction = "South"


	elif angle >= -120 and angle <= -60:
		animation_direction = "North"

	elif angle <= -120 or angle >= 120:
		animation_direction = "West"
		
	my_Sprite.play("walk_"+str(animation_direction.to_lower()))
