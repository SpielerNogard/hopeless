extends Control

export (NodePath) var item_list_node

var item_scene = preload("res://Scenes/NPCS/Inventory_Item.tscn")
onready var item_list = get_node(item_list_node)
onready var button_hover_sound = $ButtonHoverSound
onready var button_pressed_sound = $ButtonPressedSound
var button_pressed = ""

func add_equipment(equipment_send):
	#Itemaufbau: [Name,Icon,Level,Eisen pro lvl,gold pro lvl, stone pro lvl,Armor ,dmg
	var equipment = equipment_send[0]
	var item_equipped = equipment_send[1]
	
	var this_equipment = item_scene.instance()
	
	this_equipment.connect("equipped",self,"play_button_pressed_sound")
	this_equipment.connect("mouse_enter",self,"play_hover_sound")
	item_list.add_child(this_equipment)

	this_equipment.set_equipment_stats(equipment)

func play_button_pressed_sound():
	if Settings.enable_soundeffekte:
		button_pressed_sound.volume_db = Settings.soundeffekte_volume - 7
		button_pressed_sound.play()

func play_hover_sound():
	if Settings.enable_soundeffekte:
		button_hover_sound.volume_db = Settings.soundeffekte_volume
		button_hover_sound.play()


func add_item(item):
	#Item [Name,Icon,Anzahl,Preis]
	#Item[item,anzahlimin]
	var item_stats = item
	var item_name = item.name
	var item_anzahl = Global.new_mana_potion.owned
	
	if "Heil" in item_name:
		item_anzahl = Global.new_health_potion.owned


	var icon = item_stats.icon_path
	var anzahl = item_stats.owned

	
	var this_equipment = item_scene.instance()
	this_equipment.connect("equipped",self,"play_button_pressed_sound")
	this_equipment.connect("mouse_enter",self,"play_hover_sound")

	item_list.add_child(this_equipment)
	this_equipment.set_shop_stats(item_stats)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	var player_Inventory_items = Global.Player_item_iventory
	var equipment = Global.inventory
	
	for a in range(len(equipment)):
		add_equipment(equipment[a])
	for a in range(len(player_Inventory_items)):
		add_item(player_Inventory_items[a])


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_inventory") and self.visible:
		_on_Button_pressed()


func _on_Button_pressed():
	get_tree().paused = false
	if Settings.enable_soundeffekte:
		play_button_pressed_sound()
		self.visible = false
		button_pressed = "quit"
	else:
		queue_free()


func _on_ButtonPressedSound_finished():
	if button_pressed == "quit":
		queue_free()
