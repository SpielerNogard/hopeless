extends Control

export (NodePath) var item_icon_node 
export (NodePath) var item_name_label_node 
export (NodePath) var item_anzahl_label_node 
export (NodePath) var item_kosten_label_node 
export (NodePath) var item_im_besitz_label_node 

export (NodePath) var buy_button_text_node

onready var buy_button_text = get_node(buy_button_text_node)

var button_down_color = Color(1,1,1,1)
var button_up_color = Color(0,0,0,1)


signal item_bought
signal failed_to_buy
signal mouse_enter

onready var item_icon_text = get_node(item_icon_node)
onready var item_name_text = get_node(item_name_label_node)
onready var item_anzahl_text = get_node(item_anzahl_label_node)
onready var item_kosten_text = get_node(item_kosten_label_node)
onready var item_im_besitz_text = get_node(item_im_besitz_label_node)
var item_name = "Lederhelm"
var icon = preload("res://Assets/GUI/Items/armor/armor_02.png")
var item_id = 0
var item_anzahl = 0
var item_kosten = 0
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
#	[Name,Icon,Anzahl,Preis]
	var me = Global.Shop_Iventory[item_id]
	print(item_id)
	item_icon_text.set_texture(load(me.icon_path))
	item_name_text.set_text(me.name)
	
	if "Heil" in me.name:
		item_im_besitz_text.set_text(str(Global.new_health_potion.owned))
	else:
		item_im_besitz_text.set_text(str(Global.new_mana_potion.owned))
		
	item_kosten_text.set_text(str(me.price))

func buy_item():
	if Global.buy_item(item_id):
		emit_signal("item_bought")
	else:
		emit_signal("failed_to_buy")
func set_id(zahl):
	item_id = zahl
	#print(zahl)
func _on_Button_pressed():
	buy_item()


func _on_TextureButton_pressed():
	buy_item()


func _on_TextureButton_button_down():
	buy_button_text.add_color_override("font_color", button_down_color)
	buy_button_text.margin_bottom = 4


func _on_TextureButton_button_up():
	buy_button_text.add_color_override("font_color", button_up_color)
	buy_button_text.margin_bottom = 0


func _on_BuyButton_mouse_entered():
	emit_signal("mouse_enter")
