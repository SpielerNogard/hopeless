extends MarginContainer



export (NodePath) var lvl_label_node
export (NodePath) var totalXP_label_node
export (NodePath) var current_xp_and_xp_next_lvl_label_node
export (NodePath) var current_leben_and_max_leben_label_node
export (NodePath) var lebensreg_label_node
export (NodePath) var current_mana_and_max_mana_label_node
export (NodePath) var manareg_label_node
export (NodePath) var skill1_button_node
export (NodePath) var skill2_button_node
export (NodePath) var skill3_button_node
export (NodePath) var skill4_button_node
export (NodePath) var skill_selection_node
export (NodePath) var skill_type_node

export (NodePath) var offense_skill_set_node
export (NodePath) var defense_skill_set_node
export (NodePath) var passive_skill_set_node


export (NodePath) var close_button_label_node

onready var close_button_label = get_node(close_button_label_node)

var button_down_color = Color(1,1,1,1)
var button_up_color = Color(0,0,0,1)


onready var offense_skill_set = get_node(offense_skill_set_node)
onready var defense_skill_set = get_node(defense_skill_set_node)
onready var passive_skill_set = get_node(passive_skill_set_node)



var loaded_skills = "Offensive"


var my_stats = []

onready var skill_type = get_node(skill_type_node)

onready var character_Name_Label = get_node("MarginContainer/HBoxContainer/LeftSide/StatsBackGround/Characterstats/Characterstats/Character_name")

onready var lvl_label = get_node(lvl_label_node)
onready var totalXP_label = get_node(totalXP_label_node)
onready var current_xp_and_xp_next_lvl_label = get_node(current_xp_and_xp_next_lvl_label_node)
onready var current_leben_and_max_leben_label = get_node(current_leben_and_max_leben_label_node)
onready var lebensreg_label = get_node(lebensreg_label_node)
onready var current_mana_and_max_mana_label = get_node(current_mana_and_max_mana_label_node)
onready var manareg_label = get_node(manareg_label_node)

var selected_skill_number = 0

onready var skill_selection = get_node(skill_selection_node)
onready var skill1_button = get_node(skill1_button_node)
onready var skill2_button = get_node(skill2_button_node)
onready var skill3_button = get_node(skill3_button_node)
onready var skill4_button = get_node(skill4_button_node)
	
# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	#skill_selection.visible = false

func select_skill():
	pass
	#skill_selection.visible = true
	
func set_my_skills():
	var my_skills = Global.player_skills	
	var skill1 = my_skills[0]
	var skill2 = my_skills[1]
	var skill3 = my_skills[2]
	var skill4 = my_skills[3]
	skill1_button.set_normal_texture(skill1[1])
	skill2_button.set_normal_texture(skill2[1])
	skill3_button.set_normal_texture(skill3[1])
	skill4_button.set_normal_texture(skill4[1])
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if Input.is_action_just_pressed("ui_skills") and self.visible:
		_on_SchlieenButton_pressed()
	
	set_my_skills()
	get_all_characterstats()
	
	


func get_all_characterstats():

	character_Name_Label.set_text(str(Global.player_name))
	
	lvl_label.set_text("Level: "+str(Global.player_current_lvl))
	totalXP_label.set_text("Level: "+str(Global.player_current_lvl))
	current_xp_and_xp_next_lvl_label.set_text("Level: "+str(Global.player_current_lvl))
	
	current_leben_and_max_leben_label.set_text("Leben: "+str(Global.player_current_health) + " / " + str(Global.player_max_health))
	lebensreg_label.set_text("Level: "+str(Global.player_current_lvl))
	
	current_mana_and_max_mana_label.set_text("Level: "+str(Global.player_current_lvl))
	manareg_label.set_text("Level: "+str(Global.player_current_lvl))

func _on_SchlieenButton_pressed():
	get_tree().paused = false
	queue_free()

func _on_Skill1_pressed():
	selected_skill_number = 0
	select_skill()
	
func _on_Skill2_pressed():
	selected_skill_number = 1
	select_skill()

func _on_Skill3_pressed():
	selected_skill_number = 2
	select_skill()

func _on_Skill4_pressed():
	selected_skill_number = 3
	select_skill()
	
func change_skills(name):
	Global.set_skill(selected_skill_number,name)
	
func _on_Pocket_Sand_pressed():
	change_skills("pocket_sand")


func load_offense_skills():
	loaded_skills = "Offensive"
	offense_skill_set.visible = true
	skill_type.set_text(loaded_skills)
	
func load_defensive_skills():
	loaded_skills = "Defensive"
	defense_skill_set.visible = true
	skill_type.set_text(loaded_skills)

func load_passive_skills():
	loaded_skills = "Passive"
	passive_skill_set.visible = true
	skill_type.set_text(loaded_skills)

func _on_TextureButton_pressed():
	if loaded_skills == "Offensive":
		print("Test")
		offense_skill_set.visible = false
		load_defensive_skills()

	elif loaded_skills == "Defensive":
		defense_skill_set.visible = false
		load_passive_skills()

	elif loaded_skills == "Passive":
		passive_skill_set.visible = false
		load_offense_skills()


func _on_TextureButton2_pressed():
	if loaded_skills == "Offensive":
		offense_skill_set.visible = false
		load_passive_skills()

	elif loaded_skills == "Defensive":
		defense_skill_set.visible = false
		load_offense_skills()

	elif loaded_skills == "Passive":
		passive_skill_set.visible = false
		load_defensive_skills()


func _on_SchlieenButton_button_down():
	close_button_label.add_color_override("font_color", button_down_color)
	close_button_label.margin_bottom = 4


func _on_SchlieenButton_button_up():
	close_button_label.add_color_override("font_color", button_up_color)
	close_button_label.margin_bottom = 0


func _on_Thunder_Storm_pressed():
	change_skills("thunder_storm")

func _on_Holy_Heal_pressed():
	change_skills("holy_heal")

func _on_Stone_Skin_pressed():
	change_skills("stone_skin")
