extends CenterContainer


export (NodePath) var count_black_demons_node
export (NodePath) var count_ghost_demons_node
export (NodePath) var count_lost_soldiers_node
export (NodePath) var count_lost_mages_node

export (NodePath) var black_demon_animator_node
export (NodePath) var ghost_demon_animator_node
export (NodePath) var soldier_animator_node
export (NodePath) var mage_animator_node

export (NodePath) var victory_defeat_label_node
export (NodePath) var sub_title_label_node

onready var victory_defeat_label = get_node(victory_defeat_label_node)
onready var sub_title_label = get_node(sub_title_label_node)

onready var black_demon_animator = get_node(black_demon_animator_node)
onready var ghost_demon_animator = get_node(ghost_demon_animator_node)
onready var soldier_animator = get_node(soldier_animator_node)
onready var mage_animator = get_node(mage_animator_node)


onready var count_black_demons = get_node(count_black_demons_node)
onready var count_ghost_demons = get_node(count_ghost_demons_node)
onready var count_lost_soldiers = get_node(count_lost_soldiers_node)
onready var count_lost_mages = get_node(count_lost_mages_node)


signal button_pressed
signal mouse_enter
var battle_won = false
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func set_labels(black_demons, ghost_demons, soldiers, mages , victory):
	count_black_demons.set_text(str(black_demons) +" x")
	count_ghost_demons.set_text(str(ghost_demons) +" x")
	count_lost_soldiers.set_text(str(soldiers) +" x")
	count_lost_mages.set_text(str(mages) +" x")
	
	mage_animator.frame = 0
	soldier_animator.frame = 0
	battle_won = victory
	
	if victory:
		victory_defeat_label.set_text("Victory")
		sub_title_label.set_text("Die Stadt wurde erfolgreich verteidigt")
	else:
		victory_defeat_label.set_text("Defeat")
		sub_title_label.set_text("Die Armee konnte die Stadt nicht verteidigen")


func _on_CloseVictory_pressed():
	emit_signal("button_pressed")
	if battle_won:
		get_parent().send_victory_to_overworld()
		queue_free()
	else:
		queue_free()


func _on_AnimatedSprite_animation_finished():
	pass # Replace with function body.


func _on_DemonAnimation_animation_finished():
	if black_demon_animator.animation == "Death":
		$Timer.start(0.5)
	elif black_demon_animator.animation == "Attack_S":
		black_demon_animator.play("Death")



func _on_GhostDemon_animation_finished():
	if ghost_demon_animator.animation == "Death":
		pass
	elif ghost_demon_animator.animation == "Attack_S":
		ghost_demon_animator.play("Death")



func _on_Timer_timeout():
	black_demon_animator.play("Attack_S")
	ghost_demon_animator.play("Attack_S")



func _on_Soldier_animation_finished():
	
	
	if soldier_animator.animation == "Attack_S":
		soldier_animator.play("Death")
	elif soldier_animator.animation == "Death":
		pass


func _on_Mage_animation_finished():
	if mage_animator.animation == "Casting_S":
		mage_animator.play("Death")

	elif mage_animator.animation == "Death":
		$SoldierTimer.start(200)


func _on_SoldierTimer_timeout():
	soldier_animator.play("Attack_S")
	mage_animator.play("Casting_S")


func _on_CloseVictory_mouse_entered():
	emit_signal("mouse_enter")
