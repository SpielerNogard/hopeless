extends Control


export (NodePath) var new_game_label_node
export (NodePath) var load_game_label_node
export (NodePath) var quit_game_label_node
export (NodePath) var settings_label_node

onready var new_game_label = get_node(new_game_label_node)
onready var load_game_label = get_node(load_game_label_node)
onready var quit_game_label = get_node(quit_game_label_node)
onready var settings_label = get_node(settings_label_node)

var button_down_color = Color(1,1,1,1)
var button_up_color = Color(0,0,0,1)

onready var button_hover_sound = $ButtonHoverSound
onready var button_pressed_sound = $ButtonPressedSound
onready var musik = get_tree().current_scene.get_node("Musik")

var button_pressed = ""

func load_settings():
	var save_game = File.new()
	if not save_game.file_exists("res://settings.save"):
		return # Error! We don't have a save to load.

	save_game.open("res://settings.save", File.READ)
	while save_game.get_position() < save_game.get_len():
		var save_data = parse_json(save_game.get_line())


		for i in save_data.keys():
			if i == "soundeffekte_volume":
				Settings.set(i, (save_data[i] - 10))
			else:
				Settings.set(i, save_data[i])
	Settings.recalculate_remaining_variables()
	save_game.close()


func _ready():
	load_settings()
	#Global.save_base_stats()
	
	if Settings.enable_music:
		musik.volume_db = Settings.music_volume
		musik.play()
	
	

func load_game(file_path):
	var save_game = File.new()
	if not save_game.file_exists(file_path):
		return # Error! We don't have a save to load.

	save_game.open(file_path, File.READ)
	while save_game.get_position() < save_game.get_len():
		var save_data = parse_json(save_game.get_line())


		for i in save_data.keys():
			Global.set(i, save_data[i])
	Global.recalculate_remaining_variables()
	save_game.close()

func _on_LoadGame_pressed():
	if Settings.enable_soundeffekte:
		button_pressed_sound.volume_db = Settings.soundeffekte_volume - 7
		button_pressed_sound.play(0)
	yield(get_tree().create_timer(0.1),"timeout")
	
	load_game("res://savegame.save")
	Global.switch_to_overworld()



func _on_LoadGame_button_up():
	load_game_label.add_color_override("font_color", button_up_color)
	load_game_label.margin_bottom = 0


func _on_LoadGame_button_down():
	load_game_label.add_color_override("font_color", button_down_color)
	load_game_label.margin_bottom = 4


func play_button_pressed_sound():
	if Settings.enable_soundeffekte:
		button_pressed_sound.volume_db = Settings.soundeffekte_volume - 7
		button_pressed_sound.play(0)

func _on_NewGame_pressed():
	play_button_pressed_sound()
	yield(get_tree().create_timer(0.1),"timeout")
	load_game("res://startstats.save")
	Global.switch_to_overworld()



func _on_NewGame_button_up():
	new_game_label.add_color_override("font_color", button_up_color)
	new_game_label.margin_bottom = 0


func _on_NewGame_button_down():
	new_game_label.add_color_override("font_color", button_down_color)
	new_game_label.margin_bottom = 4


func _on_QuitGame_button_up():
	quit_game_label.add_color_override("font_color", button_up_color)
	quit_game_label.margin_bottom = 0


func _on_QuitGame_button_down():
	quit_game_label.add_color_override("font_color", button_down_color)
	quit_game_label.margin_bottom = 4


func _on_QuitGame_pressed():
	get_tree().quit()


func _on_Settings_button_down():
	settings_label.add_color_override("font_color", button_down_color)
	settings_label.margin_bottom = 4


func _on_Settings_button_up():
	settings_label.add_color_override("font_color", button_up_color)
	settings_label.margin_bottom = 0


func _on_Settings_pressed():
	play_button_pressed_sound()
	yield(get_tree().create_timer(0.1),"timeout")
	
	var settings_menu = load("res://Scenes/GUI/Start_Screen/Settings.tscn").instance()
	add_child(settings_menu)
	


func _on_button_mouse_entered():
	
	if Settings.enable_soundeffekte:
		button_hover_sound.volume_db = Settings.soundeffekte_volume
		button_hover_sound.play(0)








