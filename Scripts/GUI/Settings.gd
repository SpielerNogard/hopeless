extends Control


export (NodePath) var sound_music_volume_node
export (NodePath) var sound_music_enabled_node
export (NodePath) var sound_music_slider_node


export (NodePath) var sound_effect_volume_node
export (NodePath) var sound_effect_enabled_node
export (NodePath) var sound_effect_slider_node

export (NodePath) var demon_base_scaling_node
export (NodePath) var demon_base_scaling_slider_node

export (NodePath) var demon_round_scaling_node
export (NodePath) var demon_round_scaling_slider_node

export (NodePath) var full_screen_enabled_node
export (NodePath) var ignore_cutscenes_enabled_node



onready var sound_music_slider = get_node(sound_music_slider_node)
onready var sound_effect_slider = get_node(sound_effect_slider_node)
onready var demon_base_scaling_slider = get_node(demon_base_scaling_slider_node)
onready var demon_round_scaling_slider = get_node(demon_round_scaling_slider_node)

onready var demon_base_scaling = get_node(demon_base_scaling_node)
onready var demon_round_scaling = get_node(demon_round_scaling_node)
onready var full_screen_enabled= get_node(full_screen_enabled_node)
onready var ignore_cutscenes_enabled = get_node(ignore_cutscenes_enabled_node)


onready var sound_effect_volume = get_node(sound_effect_volume_node)
onready var sound_effect_enabled = get_node(sound_effect_enabled_node)

onready var sound_music_volume = get_node(sound_music_volume_node)
onready var sound_music_enabled = get_node(sound_music_enabled_node)

onready var button_hover_sound = $ButtonHoverSound
onready var button_pressed_sound = $ButtonPressedSound
onready var example_sound_musik = get_tree().current_scene.get_node("Musik")
onready var example_sound_effekt = $ExampleSoundEffekt

var just_started = true
func load_settings():
	var save_game = File.new()
	if not save_game.file_exists("res://settings.save"):
		return # Error! We don't have a save to load.

	save_game.open("res://settings.save", File.READ)
	while save_game.get_position() < save_game.get_len():
		var save_data = parse_json(save_game.get_line())


		for i in save_data.keys():
			Settings.set(i, save_data[i])
	Settings.recalculate_remaining_variables()
	save_game.close()

func _ready():
	load_settings()
	
	
	sound_music_slider.value = Settings.music_volume
	sound_music_volume.set_text(get_db_text(Settings.music_volume))
	sound_music_enabled.pressed = Settings.enable_music
	
	sound_effect_slider.value = Settings.soundeffekte_volume + 10
	sound_effect_volume.set_text(get_db_text(Settings.soundeffekte_volume + 10))
	sound_effect_enabled.pressed = Settings.enable_soundeffekte
	
	
	demon_base_scaling_slider.value = Settings.start_scaling
	demon_base_scaling.set_text(get_percent_text(Settings.start_scaling))
	demon_round_scaling_slider.value = Settings.scaling_per_round
	demon_round_scaling.set_text(get_percent_text(Settings.scaling_per_round))
	full_screen_enabled.pressed = Settings.enable_full_screen
	ignore_cutscenes_enabled.pressed = Settings.ignore_cutscenes

	just_started = false

func get_db_text(value):
	if value <0:
		return (str(value) + " db")
	else:
		return ("+" + str(value) + " db")
		
func get_percent_text(value):
	if value <0:
		return (str(value) + "%")
	else:
		return ("+" + str(value) + "%")

func _on_MusikSlider_value_changed(value):
	if just_started:
		return
	sound_music_volume.set_text(get_db_text(value))

	example_sound_musik.volume_db = sound_music_slider.value 
	if !example_sound_musik.playing and sound_music_enabled.pressed:
		example_sound_musik.play(0)

func on_button_mouse_entered():
	button_hover_sound.volume_db = sound_effect_slider.value - 10
	if sound_effect_enabled.pressed:
		button_hover_sound.play(0)

func _on_CheckBox_mouse_entered():
	pass # Replace with function body.



func _on_MusicEnabled_pressed():
	#Settings.enable_music = sound_music_enabled.pressed
	if sound_music_enabled.pressed:
		example_sound_musik.play()
	
	if !sound_music_enabled.pressed:
		print("HEY")
		example_sound_musik.stop()


func _on_EffektSlider_value_changed(value):
	if just_started:
		return
	if value <0:
		sound_effect_volume.set_text(str(value) + " db")
	else:
		sound_effect_volume.set_text("+" + str(value) + " db")
	
	example_sound_effekt.volume_db = sound_effect_slider.value - 10 - 7
	if !example_sound_effekt.playing and sound_effect_enabled.pressed:
		example_sound_effekt.play(0)


func _on_EffektEnabled_pressed():
	#Settings.enable_soundeffekte = sound_effect_enabled.pressed
	
	if !sound_effect_enabled.pressed:
		button_hover_sound.stop()
		button_pressed_sound.stop()
		example_sound_effekt.stop()




func _on_FullScreen_pressed():
	#Settings.enable_full_screen = full_screen_enabled.pressed
	
	if full_screen_enabled.pressed:
		OS.window_fullscreen = true
	else:
		OS.window_fullscreen = false


func _on_BaseStatsSlider_value_changed(value):
	if just_started:
		return
	if value <0:
		demon_base_scaling.set_text(str(value) + "%")
	else:
		demon_base_scaling.set_text("+" + str(value) + "%")


func _on_Abbrechen_pressed():
	
	if Settings.enable_soundeffekte:
		button_pressed_sound.volume_db = Settings.soundeffekte_volume -7
		button_pressed_sound.play()
	if Settings.enable_music and !sound_music_enabled.pressed:
		example_sound_musik.volume_db = Settings.music_volume
		example_sound_musik.play()
	
	yield(get_tree().create_timer(0.1),"timeout")
	
	if full_screen_enabled.pressed != Settings.enable_full_screen:
		OS.window_fullscreen = Settings.enable_full_screen
	
	self.visible = false


func _on_IgnoreCutscenes_pressed():
	pass # Replace with function body.


func _on_ScalingSlider_value_changed(value):
	if just_started:
		return
	if value <0:
		demon_round_scaling.set_text(str(value) + "%")
	else:
		demon_round_scaling.set_text("+" + str(value) + "%")


func _on_Speichern_pressed():
	set_in_Settings()
	Settings.save_setting()

	if Settings.enable_soundeffekte:
		button_pressed_sound.volume_db = Settings.soundeffekte_volume - 7
		button_pressed_sound.play()
	
	yield(get_tree().create_timer(0.1),"timeout")
	self.visible = false

func set_in_Settings():
	Settings.soundeffekte_volume = sound_effect_slider.value - 10
	Settings.enable_soundeffekte = sound_effect_enabled.pressed
	Settings.music_volume = sound_music_slider.value
	Settings.enable_music = sound_music_enabled.pressed
	Settings.ignore_cutscenes = ignore_cutscenes_enabled.pressed
	Settings.scaling_per_round = demon_round_scaling_slider.value
	Settings.start_scaling = demon_base_scaling_slider.value
	Settings.enable_full_screen = full_screen_enabled.pressed
	


func _on_ButtonPressedSound_finished():
	queue_free()
