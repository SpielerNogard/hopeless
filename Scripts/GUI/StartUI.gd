extends MarginContainer


onready var vboxcontainer = $NinePatchRect/Menu/Rahmen/Background/VBoxContainer

onready var new_game_button_text = vboxcontainer.get_node("newGameCenter/TextureRect/NewGame/Label")

onready var load_game_button_text = vboxcontainer.get_node("LoadGameCenter/TextureRect/LoadGame/Label")

onready var quit_game_button_text = vboxcontainer.get_node("QuitGameCenter/TextureRect/QuitGame/Label")




var button_down_color = Color(1,1,1,1)
var button_up_color = Color(0,0,0,1)


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass




func _on_QuitGame_button_down():
	quit_game_button_text.add_color_override("font_color", button_down_color)
	quit_game_button_text.margin_bottom = 4
	
func _on_QuitGame_button_up():
	quit_game_button_text.add_color_override("font_color", button_up_color)
	quit_game_button_text.margin_bottom = 0


func _on_NewGame_button_down():
	new_game_button_text.add_color_override("font_color", button_down_color)
	new_game_button_text.margin_bottom = 4
	
func _on_NewGame_button_up():
	new_game_button_text.add_color_override("font_color", button_up_color)
	new_game_button_text.margin_bottom = 0
	

func _on_LoadGame_button_down():
	load_game_button_text.add_color_override("font_color", button_down_color)
	load_game_button_text.margin_bottom = 4

func _on_LoadGame_button_up():
	load_game_button_text.add_color_override("font_color", button_up_color)
	load_game_button_text.margin_bottom = 0




func _on_QuitGame_pressed():
	get_tree().quit()
	pass # Replace with function body.
