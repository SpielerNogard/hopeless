extends MarginContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export (NodePath) var continue_node
export (NodePath) var demon_count_node


onready var continue_button_text = get_node(continue_node)
onready var demon_count = get_node(demon_count_node)
onready var button_pressed_sound = $ButtonPressedSound
onready var button_hover_sound = $ButtonHoverSound
onready var victory_sound = $VictorySound

var button_down_color = Color(1,1,1,1)
var button_up_color = Color(0,0,0,1)

# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().current_scene.get_node("Musik").stop()
	
	if Settings.enable_music:
		yield(get_tree().create_timer(0.3),"timeout")
		victory_sound.volume_db = Settings.music_volume
		victory_sound.play()

func set_demons(anzahl):
	demon_count.set_text(str(anzahl)+"/"+str(anzahl))


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func play_button_pressed_sound():
	if Settings.enable_soundeffekte:
		button_pressed_sound.volume_db = Settings.soundeffekte_volume - 7
		button_pressed_sound.play()

func play_hover_sound():
	if Settings.enable_soundeffekte:
		button_hover_sound.volume_db = Settings.soundeffekte_volume
		button_hover_sound.play()

func _on_Spiel_Fortsetzen_pressed():
	Global.after_fight = true
	Global.won_fight  = true
	Global.player_died = false
	get_tree().paused = false
	play_button_pressed_sound()
	
	yield(get_tree().create_timer(0.1),"timeout")

	
	Global.switch_to_overworld_after_fight()


func _on_Spiel_Fortsetzen_button_down():
	continue_button_text.add_color_override("font_color", button_down_color)
	continue_button_text.margin_bottom = 4


func _on_Spiel_Fortsetzen_button_up():
	continue_button_text.add_color_override("font_color", button_up_color)
	continue_button_text.margin_bottom = 0


func _on_Spiel_Fortsetzen_mouse_entered():
	play_hover_sound()
