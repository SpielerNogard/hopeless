extends Control


export (NodePath) var town_name_node
export (NodePath) var gold_per_turn_node
export (NodePath) var iron_per_turn_node
export (NodePath) var stone_per_turn_node
export (NodePath) var food_per_turn_node
export (NodePath) var people_per_turn_node
export (NodePath) var town_picture_node
onready var town_picture = get_node(town_picture_node)


onready var town_name = get_node(town_name_node)
onready var gold_per_turn = get_node(gold_per_turn_node)
onready var iron_per_turn = get_node(iron_per_turn_node)
onready var stone_per_turn = get_node(stone_per_turn_node)
onready var food_per_turn = get_node(food_per_turn_node)
onready var people_per_turn = get_node(people_per_turn_node)

onready var button_hover_sound = $ButtonHoverSound
onready var button_pressed_sound = $ButtonPressedSound

var button_pressed = ""
var town_stats
func _ready():
	pass # Replace with function body.


func _on_Betreten_pressed():
	play_button_pressed_sound()
	
	yield(get_tree().create_timer(0.1),"timeout")
	
	if town_stats.name == Global.main_city.name or town_stats.name == Global.demon_castle.name:
		Global.switch_to_city()
	else:
		Global.switch_to_village()

func set_town_stats(town_dict):
	town_stats = town_dict
	if town_dict.name == Global.main_city.name or town_stats.name == Global.demon_castle.name:
		town_picture.texture = load("res://Assets/World/Overworld/enter town/Enter City.png")
	
	town_name.set_text(str(town_dict.name))
	gold_per_turn.set_text(" + " + str(town_dict.gold))
	iron_per_turn.set_text(" + " + str(town_dict.iron))
	stone_per_turn.set_text(" + " + str(town_dict.stone))
	food_per_turn.set_text(" + " + str(town_dict.food))
	people_per_turn.set_text(" + " + str(town_dict.people))

func _on_Schlieen_pressed():
	if Settings.enable_soundeffekte:
		play_button_pressed_sound()
		self.visible = false
		button_pressed = "quit"
	else:
		queue_free()
	get_tree().current_scene.get_node("Player").set_in_town(false)



func _on_button_mouse_entered():
	play_hover_sound()

func play_button_pressed_sound():
	if Settings.enable_soundeffekte:
		button_pressed_sound.volume_db = Settings.soundeffekte_volume - 7
		button_pressed_sound.play()
	
func play_hover_sound():
	if Settings.enable_soundeffekte:
		button_hover_sound.volume_db = Settings.soundeffekte_volume
		button_hover_sound.play()

func _on_ButtonPressedSound_finished():
	if button_pressed == "quit":
		queue_free()
