extends MarginContainer


var max_slide = 220
var current_slide = 0
var slide_time = 0.25
var slide_direction = -1
onready var test = get_parent()

onready var maximize_button = test.get_node("MinimizeButtonCenter/TextureRect/TextureRect/MaximizeButton")
onready var minimize_button = test.get_node("MinimizeButtonCenter/TextureRect/TextureRect/MinimizeButton")

func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _process(delta):
	current_slide += delta * float(slide_direction) / slide_time
	
	if current_slide <0:
		current_slide = 0
		maximize_button.visible = true
		minimize_button.visible = false
		maximize_button.pressed = false

	if current_slide >1:
		current_slide = 1
		minimize_button.visible = true
		maximize_button.visible = false
		minimize_button.pressed = false

	test.margin_left = - current_slide * max_slide
	test.margin_right = - current_slide * max_slide

func _on_TextureButton_toggled(button_pressed):
	if button_pressed:
		slide_direction = 1
	else:
		slide_direction = -1



func _on_MaximizeButton_toggled(button_pressed):
	if button_pressed:
		slide_direction = 1
	else:
		slide_direction = -1
