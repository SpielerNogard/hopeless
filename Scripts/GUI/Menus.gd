extends CanvasLayer


onready var sound_player = $MenuSound
onready var inventar_open_sound = $InventarOpenSound
onready var menu_open_sound = $MenuOpenSound


var characterMenu2 = preload("res://Scenes/GUI/Character_Stats/CharacterStatsWithSkillSelection.tscn")
var escMenu = preload("res://Scenes/GUI/Escape_Menu/EscMenu.tscn")
var inventar = preload("res://Scenes/NPCS/Inventar.tscn")

var inventar_instance
var escMenu_instance
var characterMenu_instance

func _process(delta):
	if Input.is_action_just_pressed("ui_skills"):
		if is_instance_valid(characterMenu_instance):
			if !characterMenu_instance.visible:
				_on_Character_pressed()
		else:
			_on_Character_pressed()
	elif Input.is_action_just_pressed("ui_inventory"):
		if is_instance_valid(inventar_instance):
			if !inventar_instance.visible:
				_on_Inventar_pressed()
		else:
			_on_Inventar_pressed()
	elif Input.is_action_just_pressed("ui_cancel"):
		if is_instance_valid(escMenu_instance):
			if !escMenu_instance.visible:
				_on_Hauptmen_pressed()
		else:
			_on_Hauptmen_pressed()


func _on_Character_pressed():
	if Settings.enable_soundeffekte:
		sound_player.volume_db = Settings.soundeffekte_volume
		sound_player.play(0)
	if is_instance_valid(characterMenu_instance):
		characterMenu_instance.queue_free()
	var erstellteCharacter = characterMenu2.instance()
	characterMenu_instance = erstellteCharacter
	#erstellteCharacter.set_skills_at_start(skill1.skill_name,skill2.skill_name,skill3.skill_name,skill4.skill_name)
	add_child(erstellteCharacter)
	get_tree().paused = true
	erstellteCharacter.get_all_characterstats()
	#erstellteCharacter.queue_free()


func _on_Inventar_pressed():
	if Settings.enable_soundeffekte:
		inventar_open_sound.volume_db = Settings.soundeffekte_volume
		inventar_open_sound.play(0)

	if is_instance_valid(inventar_instance):
		inventar_instance.queue_free()
	var erstellte_inventar = inventar.instance()
	inventar_instance = erstellte_inventar
	add_child(erstellte_inventar)
	get_tree().paused = true

	pass # Replace with function body.


func _on_Hauptmen_pressed():
	if Settings.enable_soundeffekte:
		menu_open_sound.volume_db = Settings.soundeffekte_volume
		menu_open_sound.play(0)

	if is_instance_valid(escMenu_instance):
		escMenu_instance.queue_free()
		
	var erstellteMenu = escMenu.instance()
	escMenu_instance = erstellteMenu
	add_child(erstellteMenu)
	get_tree().paused = true
