extends MarginContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var skill_name = "default_hit"


#SkillIcons
var volt_strike = preload("res://Assets/GUI/Skill_Icon/if762.png")
var default_hit = preload("res://Assets/GUI/Skill_Icon/Icon1.png")
var pocket_sand = preload("res://Assets/GUI/Skill_Icon/32_6.png")
var normal_attack = preload("res://Assets/GUI/Skill_Icon/if230.png")

var full_cooldown = 9.1		#in Sekunden
var remaining_cooldown = 0
var cast_ready = true

onready var progress_bar = $NinePatchRect/TextureProgress
onready var cool_down = $NinePatchRect/Cooldown

func set_my_name(name):
	skill_name = name
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func cast():
	remaining_cooldown = full_cooldown
	progress_bar.value = 100
	cast_ready = false
	cool_down.visible = true
	progress_bar.visible = true

func update_progress_bar():
	progress_bar.value = remaining_cooldown/full_cooldown * 100
	var the_cooldown_label_number = remaining_cooldown
	the_cooldown_label_number = stepify(remaining_cooldown,0.1)
	the_cooldown_label_number = str(the_cooldown_label_number)

	if the_cooldown_label_number.length()== 1:
		the_cooldown_label_number = the_cooldown_label_number + ".0"
	cool_down.text = str(the_cooldown_label_number)
func cooldown_routine(delta):
	if cast_ready:
		cast()
	else:
		print("PROGRESS")
		remaining_cooldown -= delta
		if remaining_cooldown <= 0:
			cast_ready = true
			cool_down.visible = false
			progress_bar.visible = false
		else:
			update_progress_bar()
func spell_routine(delta):
	var my_picture = get_node("NinePatchRect/TextureRect")
	if skill_name == "default_hit":
		my_picture.set_texture(default_hit)
	elif skill_name == "pocket_sand":
		my_picture.set_texture(pocket_sand)
	elif skill_name == "default_hit_init":
		pass
	elif skill_name == "normal_attack":
		my_picture.set_texture(normal_attack)

func _process(delta):
	cooldown_routine(delta)
	spell_routine(delta)
