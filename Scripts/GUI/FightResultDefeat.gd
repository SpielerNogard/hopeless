extends CenterContainer


export (NodePath) var count_black_demons_node
export (NodePath) var count_ghost_demons_node
export (NodePath) var count_lost_soldiers_node
export (NodePath) var count_lost_mages_node

export (NodePath) var black_demon_animator_node
export (NodePath) var ghost_demon_animator_node
export (NodePath) var soldier_animator_node
export (NodePath) var mage_animator_node

onready var black_demon_animator = get_node(black_demon_animator_node)
onready var ghost_demon_animator = get_node(ghost_demon_animator_node)
onready var soldier_animator = get_node(soldier_animator_node)
onready var mage_animator = get_node(mage_animator_node)


onready var count_black_demons = get_node(count_black_demons_node)
onready var count_ghost_demons = get_node(count_ghost_demons_node)
onready var count_lost_soldiers = get_node(count_lost_soldiers_node)
onready var count_lost_mages = get_node(count_lost_mages_node)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func set_labels(black_demons, ghost_demons, soldiers, mages):
	print(count_black_demons)
	print(black_demons)
	count_black_demons.set_text(str(black_demons) +" x")
	count_ghost_demons.set_text(str(ghost_demons) +" x")
	count_lost_soldiers.set_text(str(soldiers) +" x")
	count_lost_mages.set_text(str(mages) +" x")
	
	mage_animator.frame = 0
	soldier_animator.frame = 0


func _on_CloseVictory_pressed():
	queue_free()


func _on_AnimatedSprite_animation_finished():
	pass # Replace with function body.


func _on_DemonAnimation_animation_finished():
	if black_demon_animator.animation == "Walking_S":
		pass
	elif black_demon_animator.animation == "Attack_S":
		black_demon_animator.play("Walking_S")
		$Timer.start(1)



func _on_GhostDemon_animation_finished():
	if ghost_demon_animator.animation == "Walking_S":
		pass
	elif ghost_demon_animator.animation == "Attack_S":
		ghost_demon_animator.play("Walking_S")



func _on_Timer_timeout():
	black_demon_animator.play("Attack_S")
	ghost_demon_animator.play("Attack_S")

