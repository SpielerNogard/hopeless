extends MarginContainer


export (NodePath) var continue_node

onready var continue_button_text = get_node(continue_node)
onready var death_sound = $DeathMusik
onready var button_pressed_sound = $ButtonPressedSound
onready var button_hover_sound = $ButtonHoverSound

var button_down_color = Color(1,1,1,1)
var button_up_color = Color(0,0,0,1)

# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().current_scene.get_node("Musik").stop()
	
	if Settings.enable_music:
		yield(get_tree().create_timer(0.3),"timeout")
		death_sound.volume_db = Settings.music_volume
		death_sound.play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func play_button_pressed_sound():
	if Settings.enable_soundeffekte:
		button_pressed_sound.volume_db = Settings.soundeffekte_volume - 7
		button_pressed_sound.play()

func play_hover_sound():
	if Settings.enable_soundeffekte:
		button_hover_sound.volume_db = Settings.soundeffekte_volume
		button_hover_sound.play()

func _on_Spiel_Fortsetzen_pressed():
	Global.after_fight = true
	Global.won_fight  = false
	Global.player_died = true
	play_button_pressed_sound()
	
	yield(get_tree().create_timer(0.1),"timeout")

	Global.switch_to_overworld_after_fight()
	


func _on_Spiel_Fortsetzen_button_down():
	continue_button_text.add_color_override("font_color", button_down_color)
	continue_button_text.margin_bottom = 4


func _on_Spiel_Fortsetzen_button_up():
	continue_button_text.add_color_override("font_color", button_up_color)
	continue_button_text.margin_bottom = 0


func _on_Spiel_Fortsetzen_mouse_entered():
	play_hover_sound()
