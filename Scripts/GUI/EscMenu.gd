extends MarginContainer

export (NodePath) var continue_label_node
export (NodePath) var main_menu_label_node
export (NodePath) var quit_game_label_node

onready var continue_label = get_node(continue_label_node)
onready var main_menu_label = get_node(main_menu_label_node)
onready var quit_game_label = get_node(quit_game_label_node)

onready var button_pressed_sound_player = $Sounds/ButtonPressedSound
onready var button_hover_sound_player = $Sounds/ButtonHoverSound

var button_down_color = Color(1,1,1,1)
var button_up_color = Color(0,0,0,1)
var settings = preload("res://Scenes/GUI/Start_Screen/Settings.tscn")

var button_pressed

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel") and self.visible:
		_on_Continue_pressed()

func _on_QuitGame_pressed():
	get_tree().quit()

func play_button_pressed_sound():
	if Settings.enable_soundeffekte:
		button_pressed_sound_player.volume_db = Settings.soundeffekte_volume - 7
		button_pressed_sound_player.play()

func _on_Continue_pressed():
	button_pressed = "Continue"
	get_tree().paused = false
	self.visible = false
	if Settings.enable_soundeffekte:
		play_button_pressed_sound()
	else:
		queue_free()

func _on_Continue_button_up():
	continue_label.add_color_override("font_color", button_up_color)
	continue_label.margin_bottom = 0


func _on_Continue_button_down():
	continue_label.add_color_override("font_color", button_down_color)
	continue_label.margin_bottom = 4


func _on_Main_Menu_pressed():
	play_button_pressed_sound()
	button_pressed = "Main Menu"
	yield(get_tree().create_timer(0.1),"timeout")
	get_tree().paused = false
	Global.switch_to_start_screen()
	


func _on_Main_Menu_button_down():
	main_menu_label.add_color_override("font_color", button_down_color)
	main_menu_label.margin_bottom = 4


func _on_Main_Menu_button_up():
	main_menu_label.add_color_override("font_color", button_up_color)
	main_menu_label.margin_bottom = 0


func _on_QuitGame_button_up():
	quit_game_label.add_color_override("font_color", button_up_color)
	quit_game_label.margin_bottom = 0


func _on_QuitGame_button_down():
	quit_game_label.add_color_override("font_color", button_down_color)
	quit_game_label.margin_bottom = 4


func _on_ButtonPressedSound_finished():
	if button_pressed == "Main Menu":
		print ("Main Menu")
	elif button_pressed == "Continue":
		queue_free()

func _on_Button_pressed():
	if Settings.enable_soundeffekte:
		button_hover_sound_player.volume_db = Settings.soundeffekte_volume
		button_hover_sound_player.play()


func _on_Button_mouse_entered():
	if Settings.enable_soundeffekte:
		button_hover_sound_player.volume_db = Settings.soundeffekte_volume
		button_hover_sound_player.play()


func _on_Settings_pressed():
	play_button_pressed_sound()
	var settings_menu = settings.instance()
	add_child(settings_menu)
	
