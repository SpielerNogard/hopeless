extends TextureButton

onready var label = $Text
var button_down_color = Color(1,1,1,1)
var button_up_color = Color(0,0,0,1)


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_button_up():
	label.add_color_override("font_color", button_up_color)
	label.margin_bottom = 0


func _on_Button_button_down():
	label.add_color_override("font_color", button_down_color)
	label.margin_bottom = 4

