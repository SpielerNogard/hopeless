extends MarginContainer

onready var healthBar = $CanvasLayer2/horizontalLayers/Bars/HealthBar
onready var manaBar = $CanvasLayer2/horizontalLayers/Bars/ManaBar
onready var XPBar = $CanvasLayer2/horizontalLayers/Bars/XPBar

export (NodePath) var basic_attack_node
export (NodePath) var potion_node


onready var health_potion_picture = preload("res://Assets/GUI/Items/potion/potion_01.png")
onready var mana_potion_picture = preload("res://Assets/GUI/Items/potion/potion_06.png")

onready var potion = get_node(potion_node)

onready var basic_attack = get_node(basic_attack_node)

onready var skill_container = $"CanvasLayer2/horizontalLayers/BottomBox/Control/Skills/Distance Groups/NinePatchRect/Skills/Skills"

onready var skill_1 = skill_container.get_node("Skill1")
onready var skill_2 = skill_container.get_node("Skill2")
onready var skill_3 = skill_container.get_node("Skill3")
onready var skill_4 = skill_container.get_node("Skill4")


onready var speed_up = get_node("CanvasLayer2/horizontalLayers/BottomBox/Control/Skills/Distance Groups/NinePatchRect/Skills/MouseButton/Right")


export (NodePath) var quest_label_node
export (NodePath) var black_demons_to_kill_node
export (NodePath) var ghost_demons_to_kill_node

onready var black_demons_to_kill = get_node(black_demons_to_kill_node)
onready var ghost_demons_to_kill = get_node(ghost_demons_to_kill_node)

onready var quest_label = get_node(quest_label_node)




var victory = preload("res://Scenes/GUI/Mission_End_Screens/Victory_Screen.tscn")
var dead = preload("res://Scenes/GUI/Mission_End_Screens/Dead.tscn")


var player_dead = false
var alpha = 0


func _ready():
	yield (get_tree().current_scene,"ready")


func set_potion_picture():
	if Global.new_health_potion.equipped:
		potion.set_picture(health_potion_picture)
	else:
		potion.set_picture(mana_potion_picture)
func update_ui(current_health, max_health, current_mana, max_mana):
	healthBar.updateProgressBar(current_health, max_health)
	XPBar.updateProgressBar(Global.player_current_xp, Global.player_xp_next_lvl)
	manaBar.updateProgressBar(current_mana, max_mana)
	
func set_skill_picture_in_ui(number, picture):
	if number == 1:
		skill_1.set_picture(picture)
	if number == 2:
		skill_2.set_picture(picture)
	if number == 3:
		skill_3.set_picture(picture)
	if number == 4:
		skill_4.set_picture(picture)

func set_mana_cost (cost, number):
	if number == 1:
		skill_1.set_mana_cost(cost)
	if number == 2:
		skill_2.set_mana_cost(cost)
	if number == 3:
		skill_3.set_mana_cost(cost)
	if number == 4:
		skill_4.set_mana_cost(cost)
	if number == 5:
		basic_attack.set_mana_cost(cost)
	if number == 6:
		speed_up.set_mana_cost(cost)

func set_cooldown_skill(remaining_cooldown,value,number):
	if number == 1:
		skill_1.set_cooldown(remaining_cooldown, value)
	if number == 2:
		skill_2.set_cooldown(remaining_cooldown, value)
	if number == 3:
		skill_3.set_cooldown(remaining_cooldown, value)
	if number == 4:
		skill_4.set_cooldown(remaining_cooldown, value)
	if number == 5:
		basic_attack.set_cooldown(remaining_cooldown, value)
	if number == 6:
		speed_up.set_cooldown(remaining_cooldown, value)
		

func set_stats_potion(remaining_cooldown,value):
	potion.set_cooldown(remaining_cooldown,value)

	
	if Global.new_health_potion.equipped:
		potion.set_mana_cost(Global.new_health_potion.owned)
	else:
		potion.set_mana_cost(Global.new_mana_potion.owned)
	

func set_quest(black_demons, ghost_demons):
	if !player_dead:
		black_demons_to_kill.set_text(str(black_demons))
		ghost_demons_to_kill.set_text(str(ghost_demons))

	


func dead_player():
	var childrens = self.get_children()
	
	for child in childrens:
		child.queue_free()
	var erstellteCharacter = dead.instance()
	add_child(erstellteCharacter)
	alpha = 0
	self.modulate.a = alpha
	player_dead = true 

	#get_tree().paused = true
	

func play_skill(nr):
	pass

func reward_player():
	var item = "XP"
	var amount = 100
	var GAME = get_parent().get_parent()
	var player = GAME.get_node("Player")
	player.give_item(item,amount)
	
func on_victory(anzahl_demonen):

	get_tree().current_scene.fight_over = true
	
	var erstellteMenu = victory.instance()
	add_child(erstellteMenu)
	erstellteMenu.set_demons(anzahl_demonen)
	reward_player()

		
func _process(delta):
	if player_dead:
		alpha +=delta /4
		if alpha <= 1:
			self.modulate.a = alpha
		return






func _on_TextureButton_toggled(button_pressed):
	pass # Replace with function body.
