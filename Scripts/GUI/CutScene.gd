extends CanvasLayer


onready var shader = $ColorRect
onready var shader_material = shader.material;




var lvl_to_load = null

var transition_direction = 1
var transition_speed = 0.5
var transition_strength = 0

signal finished_animation

func _ready():
	shader_material.set_shader_param("animation_point", transition_strength)
	transition_direction = -transition_speed
	set_process(false)

func start_animation():
	transition_strength = 0
	transition_direction = 1
	print("Start")
	set_process(true)

func close_animation():
	transition_strength = 1
	transition_direction = -1
	set_process(true)
	

func _process(delta):
	transition_strength += delta * transition_direction * transition_speed
	if transition_strength < 0:
		transition_strength = 0
		shader_material.set_shader_param("animation_point", transition_strength)
		emit_signal("finished_animation")
		
		set_process(false)
		
		return

	if transition_strength > 1:
		transition_strength = 1
		shader_material.set_shader_param("animation_point", transition_strength)
		emit_signal("finished_animation")
		set_process(true)
		
	shader_material.set_shader_param("animation_point", transition_strength)


func load_next_lvl(relevant_lvl):
	lvl_to_load = relevant_lvl
	transition_direction = transition_speed
