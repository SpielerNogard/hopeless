extends Control



onready var town_enter_scene = preload("res://Scenes/GUI/Town/Enter_Town.tscn")
onready var victory_results_scene = preload("res://Scenes/GUI/Town/FightResultsVictory.tscn")


export (NodePath) var soldier_owned_text_node
export (NodePath) var soldier_for_battle_text_node
export (NodePath) var mage_owned_text_node
export (NodePath) var mage_for_battle_text_node
export (NodePath) var win_chance_label_node

export (NodePath) var town_name_node
export (NodePath) var gold_per_turn_node
export (NodePath) var iron_per_turn_node
export (NodePath) var stone_per_turn_node
export (NodePath) var food_per_turn_node
export (NodePath) var people_per_turn_node


export (NodePath) var black_demons_label_node
export (NodePath) var ghost_demons_label_node

export (NodePath) var victory_screen_node

export (NodePath) var town_picture_node


onready var town_picture = get_node(town_picture_node)

onready var button_hover_sound = $ButtonHoverSound
onready var button_pressed_sound = $ButtonPressedSound
onready var error_sound = $ErrorSound



onready var town_name = get_node(town_name_node)
onready var gold_per_turn = get_node(gold_per_turn_node)
onready var iron_per_turn = get_node(iron_per_turn_node)
onready var stone_per_turn = get_node(stone_per_turn_node)
onready var food_per_turn = get_node(food_per_turn_node)
onready var people_per_turn = get_node(people_per_turn_node)



onready var black_demons_label = get_node(black_demons_label_node)
onready var ghost_demons_label = get_node(ghost_demons_label_node)

onready var win_chance_label = get_node(win_chance_label_node)
onready var Soldat_im_Besitz_label = get_node(soldier_owned_text_node)
onready var Soldat_fur_Schlacht_label = get_node(soldier_for_battle_text_node)

onready var Mage_im_Besitz_label = get_node(mage_owned_text_node)
onready var Mage_fur_Schlacht_label = get_node(mage_for_battle_text_node)

var soldat_im_besitz
var soldat_fur_schlacht
var mage_im_besitz
var mage_fur_schlacht
var black_demons_for_battle = 404
var ghost_demons_for_battle = 404
var black_demons_strength = 2
var ghost_demons_strength = 1

var mage_strength_vs_ghost = 10
var mage_strength_vs_black = 2

var soldier_strength_vs_ghost = 2
var soldier_strength_vs_black = 8
	
var remaining_blacks
var remaining_ghosts
var remaining_soldiers
var remaining_mages

var win_chance = 0
var my_dir
var runde = 1
var button_pressed = ""

func play_hover_sound():
	if Settings.enable_soundeffekte:
		button_hover_sound.volume_db = Settings.soundeffekte_volume
		button_hover_sound.play()

func play_button_pressed_sound():
	if Settings.enable_soundeffekte:
		button_pressed_sound.volume_db = Settings.soundeffekte_volume - 7
		button_pressed_sound.play()

func play_error_sound():
	if Settings.enable_soundeffekte:
		error_sound.volume_db = Settings.soundeffekte_volume - 7
		error_sound.play()

func _ready():
	runde = Global.runde
	var power = runde - 1
	
	black_demons_strength = 2 * pow(1.2,power)
	ghost_demons_strength = 2 * pow(1.2, power)
	soldat_im_besitz = Global.einfacher_fusssoldat
	mage_im_besitz = Global.Mages
	soldat_fur_schlacht = 0
	mage_fur_schlacht = 0

	update_labels()

func set_demon_counts(black_demons, ghost_demons):
	black_demons_for_battle = black_demons
	ghost_demons_for_battle = ghost_demons
	
	black_demons_label.set_text(str(black_demons) + " x")
	ghost_demons_label.set_text(str(ghost_demons) + " x")

func set_town_stats(town_dict):
	my_dir = town_dict
	
	if town_dict.name == Global.main_city.name or town_dict.name == Global.demon_castle.name:
		town_picture.texture = load("res://Assets/World/Overworld/enter town/Enter City.png")
	
	town_name.set_text(str(town_dict.name))
	
	
	
	
	gold_per_turn.set_text(" + " + str(town_dict.gold))
	iron_per_turn.set_text(" + " + str(town_dict.iron))
	stone_per_turn.set_text(" + " + str(town_dict.stone))
	food_per_turn.set_text(" + " + str(town_dict.food))
	people_per_turn.set_text(" + " + str(town_dict.people))
	
	black_demons_for_battle = town_dict.black_demons
	ghost_demons_for_battle = town_dict.ghost_demons
	

	black_demons_label.set_text(str(black_demons_for_battle) + " x")
	ghost_demons_label.set_text(str(ghost_demons_for_battle) + " x")

func update_labels():
	Soldat_im_Besitz_label.set_text(str(soldat_im_besitz))
	Soldat_fur_Schlacht_label.set_text(str(soldat_fur_schlacht))
	Mage_im_Besitz_label.set_text(str(mage_im_besitz))
	Mage_fur_Schlacht_label.set_text(str(mage_fur_schlacht))
	
	black_demons_label.set_text(str(black_demons_for_battle) + " x")
	ghost_demons_label.set_text(str(ghost_demons_for_battle) + " x")
	
	
	update_win_chance()

func update_win_chance():
	remaining_blacks = black_demons_for_battle
	remaining_ghosts = ghost_demons_for_battle
	remaining_soldiers = soldat_fur_schlacht
	remaining_mages = mage_fur_schlacht
	
	soldier_vs_black_demons()
	mage_vs_ghost_demons()
	mage_vs_black_demons()
	soldier_vs_ghost_demons()
	
	var total_demons = black_demons_for_battle + ghost_demons_for_battle

	
	var total_demons_strength = black_demons_for_battle * black_demons_strength + ghost_demons_for_battle * ghost_demons_strength
	var remaining_demon_strength = remaining_blacks * black_demons_strength + remaining_ghosts * ghost_demons_strength
	
	win_chance = float(1 - ((float(remaining_demon_strength)) / total_demons_strength))
	
	var test = win_chance
	var begin_win_chance = pow(win_chance, 2.5)
	var end_win_chance = pow(win_chance, 0.5)
	
	win_chance = (1 -test) * begin_win_chance + test * end_win_chance
	win_chance *= 100
	
	win_chance_label.set_text(str(win_chance) + " %")

func soldier_vs_black_demons():
	if remaining_blacks > 0:
		if soldat_fur_schlacht >0:
			remaining_blacks -= soldier_strength_vs_black * remaining_soldiers * Global.einfacher_fusssoldat_level / black_demons_strength
			remaining_soldiers =0
			if remaining_blacks <0:
				remaining_soldiers = (-float(remaining_blacks * black_demons_strength) / soldier_strength_vs_black)
				remaining_blacks = 0
				
func soldier_vs_ghost_demons():
	if remaining_ghosts > 0:
		if soldat_fur_schlacht >0:
			remaining_ghosts -= soldier_strength_vs_ghost * remaining_soldiers * Global.einfacher_fusssoldat_level / ghost_demons_strength
			remaining_soldiers = 0
			if remaining_ghosts <0:
				remaining_soldiers = (-float(remaining_ghosts * ghost_demons_strength) / soldier_strength_vs_ghost)
				remaining_ghosts = 0
				
func mage_vs_black_demons():
	if remaining_blacks > 0:
		if mage_fur_schlacht >0:
			remaining_blacks -= mage_strength_vs_black * remaining_mages * Global.Mage_level / black_demons_strength
			remaining_mages = 0
			if remaining_blacks <0:
				remaining_mages = (-float(remaining_blacks * black_demons_strength) / mage_strength_vs_black)
				remaining_blacks = 0
				
func mage_vs_ghost_demons():
	if remaining_ghosts > 0:
		if mage_fur_schlacht >0:
			remaining_ghosts -= mage_strength_vs_ghost * remaining_mages * Global.Mage_level / ghost_demons_strength
			remaining_mages = 0
			if remaining_ghosts <0:
				remaining_mages = (-float(remaining_ghosts * ghost_demons_strength) / mage_strength_vs_ghost)
				remaining_ghosts = 0

func _on_Verlassen_pressed():
	get_tree().current_scene.get_node("Player").set_in_town(false)
	my_dir.black_demons = black_demons_for_battle
	my_dir.ghost_demons = ghost_demons_for_battle
	
	for place in Global.alle_orte_dict:
		if place.name == my_dir.name:
			place = my_dir
	
	button_pressed = "quit"
	self.visible = false
	if Settings.enable_soundeffekte:
		play_button_pressed_sound()
	else:
		queue_free()



func _on_AddSoldier_pressed():
	if soldat_im_besitz >0:
		soldat_im_besitz -= 1
		soldat_fur_schlacht += 1
		update_labels()
		play_button_pressed_sound()
	else:
		play_error_sound()


func _on_SubSoldier_pressed():
	if soldat_fur_schlacht >0:
		soldat_im_besitz += 1
		soldat_fur_schlacht -= 1
		update_labels()
		play_button_pressed_sound()
	else:
		play_error_sound()

func _on_AddMage_pressed():
	if mage_im_besitz >0:
		mage_im_besitz -= 1
		mage_fur_schlacht += 1
		update_labels()
		play_button_pressed_sound()
	else:
		play_error_sound()

func _on_SubMage_pressed():
	if mage_fur_schlacht >0:
		mage_im_besitz += 1
		mage_fur_schlacht -= 1
		update_labels()
		play_button_pressed_sound()
	else:
		play_error_sound()


func _on_BeginFight_pressed():
	Global.einfacher_fussoldat_fur_Schlacht = soldat_fur_schlacht
	Global.Mages_fur_Schlacht = mage_fur_schlacht
	Global.black_demons_to_summon = black_demons_for_battle
	Global.ghost_demons_to_summon = ghost_demons_for_battle
	Global.player_died = true
	Global.after_fight = true
	
	Global.Mages -= mage_fur_schlacht
	Global.einfacher_fusssoldat -= soldat_fur_schlacht
	
	
	Global.after_fight = true
	Global.player_died = true
	play_button_pressed_sound()
	yield(get_tree().create_timer(0.1),"timeout")
	Global.begin_fight()


func victory(tmp_win_chance, rand_number):
	var ratio_soldiers_lost = (1 - (tmp_win_chance - rand_number))
	soldat_im_besitz += soldat_fur_schlacht
	mage_im_besitz += mage_fur_schlacht
	
	var soldier_lost = max(0,round(ratio_soldiers_lost * (soldat_fur_schlacht - remaining_soldiers)))
	var mage_lost = max(0,round(ratio_soldiers_lost * (mage_fur_schlacht - remaining_mages)))
	
	soldat_im_besitz -= soldier_lost
	mage_im_besitz -= mage_lost
	
	Global.einfacher_fusssoldat = soldat_im_besitz
	Global.Mages = mage_im_besitz
	
	soldat_fur_schlacht = 0
	mage_fur_schlacht = 0
	
	update_labels()

	var fight_results = victory_results_scene.instance()
	
	add_child(fight_results)
	
	fight_results.connect("button_pressed",self,"play_button_pressed_sound")
	fight_results.connect("mouse_enter",self,"play_hover_sound")
	fight_results.set_labels(black_demons_for_battle, ghost_demons_for_battle,
							 soldier_lost, mage_lost, true)
	

func defeat(tmp_win_chance):
	var total_soldiers = soldat_fur_schlacht + mage_fur_schlacht
		
	if total_soldiers <= 0:
		return
	else:
		var ratio_demons_defeated = pow(tmp_win_chance, 2)
			
		var ratio_soldier = float(soldat_fur_schlacht) / total_soldiers
		var ratio_mage = 1 - ratio_soldier

		Global.einfacher_fusssoldat = soldat_im_besitz
		Global.Mages = mage_im_besitz

		var black_demons_defeated = round(black_demons_for_battle * ratio_demons_defeated * ratio_soldier)
		var ghost_demons_defeated = round(ghost_demons_for_battle * ratio_demons_defeated * ratio_mage)

		black_demons_for_battle -= black_demons_defeated
		ghost_demons_for_battle -= ghost_demons_defeated


		var fight_results = victory_results_scene.instance()
		
		add_child(fight_results)
		fight_results.connect("button_pressed",self,"play_button_pressed_sound")
		fight_results.connect("mouse_enter",self,"play_hover_sound")
		fight_results.set_labels(black_demons_defeated, ghost_demons_defeated, soldat_fur_schlacht, mage_fur_schlacht, false)
		
		mage_fur_schlacht = 0
		soldat_fur_schlacht = 0
		update_labels()
		update_win_chance()
		
func send_victory_to_overworld():
	get_tree().current_scene.town_freed()
	
	
	
	
	button_pressed = "quit"
	get_tree().current_scene.get_node("Player").set_in_town(false)
	self.visible = false
	if !Settings.enable_soundeffekte:
		queue_free()

func _on_SendSoldier_pressed():
	if soldat_fur_schlacht + mage_fur_schlacht >0:
		play_button_pressed_sound()
	else:
		play_error_sound()
		return
	var rand_number = 0
	var number_of_rolls = 2

# Unter 50% ist es unwahrscheinlicher zu gewinnen, Über 50% wahre wahrscheinlichkeit
	if win_chance <50:
		for i in range(0, number_of_rolls):
			rand_number += randf()
		rand_number /= number_of_rolls
	else:
		rand_number = randf()
	

	var tmp_win_chance = win_chance / 100
	
	
	if tmp_win_chance >= rand_number:
		victory(tmp_win_chance, rand_number)
	else:
		defeat(tmp_win_chance)


func _on_CloseVictory_pressed():
	pass # Replace with function body.


func _on_ButtonPressedSound_finished():
	if button_pressed == "quit":
		queue_free()


func _on_Button_mouse_entered():
	play_hover_sound()
