extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var level_text = get_node("CanvasLayer/MarginContainer/VBoxContainer/SoldatWerte/Stats/Level")
onready var Leben_text = get_node("CanvasLayer/MarginContainer/VBoxContainer/SoldatWerte/Stats/Leben")
onready var Attack_damage_text = get_node("CanvasLayer/MarginContainer/VBoxContainer/SoldatWerte/Stats/AttackDamage")
onready var imbesitz_text = get_node("CanvasLayer/MarginContainer/VBoxContainer/SoldatWerte/Stats/imbesitz")

onready var gold_cost_text = get_node("CanvasLayer/MarginContainer/VBoxContainer/Kosten/HBoxContainer/gold")
onready var iron_cost_text = get_node("CanvasLayer/MarginContainer/VBoxContainer/Kosten/HBoxContainer2/iron")
onready var stone_cost_text = get_node("CanvasLayer/MarginContainer/VBoxContainer/Kosten/HBoxContainer3/stone")
onready var food_cost_text = get_node("CanvasLayer/MarginContainer/VBoxContainer/Kosten/HBoxContainer4/food")
onready var persons_cost_text = get_node("CanvasLayer/MarginContainer/VBoxContainer/Kosten/HBoxContainer5/persons")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func set_stats():
	level_text.set_text("Level: "+str(Global.Mage_level))
	Leben_text.set_text("Leben: "+str(Global.Mage_health))
	Attack_damage_text.set_text("Damage per hit: "+str(Global.Mage_attack_damage))
	imbesitz_text.set_text("Im Besitz: "+str(Global.Mages))
	
	gold_cost_text.set_text("Gold: "+str(Global.Mage_gold_cost))
	iron_cost_text.set_text("Eisen: "+str(Global.Mage_iron_cost))
	stone_cost_text.set_text("Stein: "+str(Global.Mage_stone_cost))
	food_cost_text.set_text("Nahrung: "+str(Global.Mage_food_cost))
	persons_cost_text.set_text("Bevölkerung: "+str(Global.Mage_persons_cost))
# Called every frame. 'delta' is the elapsed time since the previous frame.

func new_soldier():
	Global.iron -= Global.Mage_iron_cost
	Global.gold -= Global.Mage_gold_cost
	Global.nahrung -= Global.Mage_food_cost
	Global.stone -= Global.Mage_stone_cost
	Global.persons -= Global.Mage_persons_cost
	Global.Mages +=1
	
func create_soldier():
	pass
	if Global.iron >= Global.Mage_iron_cost:
		if Global.gold >= Global.Mage_gold_cost:
			if Global.nahrung >= Global.Mage_food_cost:
				if Global.stone >= Global.Mage_stone_cost:
					if Global.persons >= Global.Mage_persons_cost:
						new_soldier()
func _process(delta):
	set_stats()


func _on_TextureButton_pressed():
	create_soldier()


func _on_TextureButton2_pressed():
	Global.switch_soldats_in_baracks()
