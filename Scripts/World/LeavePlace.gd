extends Node2D

onready var musik = $Musik

func _ready():
	if Settings.enable_music:
		musik.volume_db = Settings.music_volume
		musik.play()
	


func _on_Area2D_body_entered(body):
	if body.is_in_group("Player"):
		Global.switch_to_overworld()
