extends Node2D


onready var tile_map = get_tree().current_scene.get_node("map/movementGrid")
onready var cost_map
onready var distance_map

var new_set_interesting_cells = []
var max_distance = 65500
var next_cells = []
var scene = preload("res://Scenes/Levels/Arrow.tscn")
var all_arrows = []
var start_cells = []

var start_points = []
var update_call = false


var cells_from_update_call = []
var cells_from_continue_call = []


var old_value = 0
var origin_cell_vector = Vector2.ZERO

var vector_map
var calculated_cell_map
var amount_of_vectors_map

var length_x
var length_y



func create_2d_array(width, height, value):
	var a = []

	for x in range(width):
		a.append([])
		a[x].resize(height)

		for y in range(height):
			a[x][y] = value

	return a
# Called when the node enters the scene tree for the first time.
func _ready():
	var used_rect = tile_map.get_used_rect()
	

	var start_x = used_rect.position.x
	var start_y = used_rect.position.y
	var end_x = used_rect.end.x
	var end_y = used_rect.end.y
	length_x = end_x - start_x
	length_y = end_y - start_y

	all_arrows = create_2d_array(length_x,length_y,Vector2.ZERO)
	vector_map = create_2d_array(length_x,length_y,Vector2.ZERO)
	distance_map = create_2d_array(length_x,length_y,max_distance)
	calculated_cell_map = create_2d_array(length_x,length_y,false)
	amount_of_vectors_map = create_2d_array(length_x,length_y,0)
	create_cost_map(length_x, length_y)
	fill_array(length_x, length_y)
	return


func create_cost_map(width, height):
	cost_map = []
	for x in range(width):
		cost_map.append([])
		cost_map[x].resize(height)

		for y in range(height):
			if tile_map.get_cell(x,y) > 0:
				cost_map[x][y] = 1
			else:
				cost_map[x][y] = max_distance

	return cost_map

func fill_array(x,y):
	var first_500 = 0
	for x in range(vector_map.size()):
		for y in range(vector_map[0].size()):
			if tile_map.get_cell(x,y) > 0:
				vector_map[x][y] = Vector2 (0,0)
				if first_500 >0:
					first_500 -= 1

					var arrow = scene.instance()
					add_child(arrow)
					print("somethings")

					arrow.position.x = x * 48 + 24
					arrow.position.y = y * 48 + 24
					arrow.set_id(Vector2 (x,y))
					all_arrows[x][y] = arrow
			else:
				vector_map[x][y] = Vector2.ZERO

func update_vector_map(remaining_steps):
	start_cells = []
	new_set_interesting_cells = []
	for start_point in start_points:
		var tile_x = floor(start_point.x / 48)
		var tile_y = floor(start_point.y / 48)

		if tile_x <0 or tile_x >= length_x or tile_y<0 or tile_y >= length_y:
			pass
		else:
			if !calculated_cell_map[tile_x][tile_y]:
				calculated_cell_map[tile_x][tile_y] = true
				distance_map[tile_x][tile_y] = 0
				vector_map[tile_x][tile_y] = start_point
				start_cells.append(Vector2(tile_x,tile_y))
				cells_from_update_call.append(Vector2(tile_x,tile_y))
	set_vector_cells_around_player()
	find_new_set_interesting_cells(remaining_steps - 1)
	#update_arrows()

func find_new_set_interesting_cells(remaining_steps):
	while remaining_steps >0 and new_set_interesting_cells.size()!=0:
		var tmp = new_set_interesting_cells
		remaining_steps -=1
		new_set_interesting_cells = []
		for new_cell in tmp:
			cells_around(new_cell.x, new_cell.y)
			for cell in next_cells:
				new_set_interesting_cells.append(cell)
			#	cells_from_update_call.append(cell)
				if update_call:
					cells_from_update_call.append(cell)
				else:
					cells_from_continue_call.append(cell)

		for old_cell in tmp:
			calculated_cell_map[old_cell.x][old_cell.y] = true
		remaining_steps -=1


func slide_at_wall(old_tile_x, old_tile_y, new_tile_x, new_tile_y, vector):
	#sind die selben Tiles, daher egal
	if old_tile_x == new_tile_x and old_tile_y == new_tile_y:
		return vector
	#es wird angenommen, dass der Dämon nicht außerhalb des Feldes sein kann (old_tile_x and old_tile_y sind im Bereich)
	#Bewegung in X-Richtung
	if old_tile_x != new_tile_x and old_tile_y == new_tile_y:
		if new_tile_x >= length_x:
			vector.x = 0
			return vector
		if new_tile_x < 0:
			vector.x = 0
			return vector
		if cost_map[new_tile_x][old_tile_y]>10:
			vector.x = 0
			return vector
	#Bewegung in Y-Richtung
	if old_tile_x == new_tile_x and old_tile_y != new_tile_y:
		if new_tile_y >= length_y:
			vector.y = 0
			return vector
		if new_tile_y < 0:
			vector.y = 0
			return vector
		if cost_map[old_tile_x][new_tile_y]>10:
			vector.y = 0
			return vector
	#Bewegung in Diagonaler Richtung
	if old_tile_x != new_tile_x and old_tile_y != new_tile_y:
		# Check, ob der Move in X-Y-Richtung erlaubt ist
		var x_allowed = true
		var y_allowed = true
		var diagonale = true
		if new_tile_x >= length_x or new_tile_x <0:
			x_allowed = false
			diagonale = false
		if new_tile_y >= length_y or new_tile_y < 0:
			y_allowed = false
			diagonale = false
		# Check, in welcher Richtung Wände sind
		if diagonale:
			if cost_map[new_tile_x][new_tile_y] > 10:
				diagonale = false
		if x_allowed:
			if cost_map[new_tile_x][old_tile_y] > 10:
				x_allowed = false
		if y_allowed:
			if cost_map[old_tile_x][new_tile_y] > 10:
				y_allowed = false
		#die Diagonale hat keine Hindernisse
		if diagonale:
			if !x_allowed:
				vector.x = 0
			if !y_allowed:
				vector.y = 0
			return vector
		else:
			if x_allowed:
				vector.y = 0
				return vector
			if y_allowed:
				vector.x = 0
				return vector
			return Vector2.ZERO


		
		print("Pls, fix")
		
		
		#Dämon ist in einer Ecke
		return Vector2.ZERO
		
		return vector

	return vector

func wall_in_walk_direction(x:int,y:int, vector) -> bool:
	if vector.y >0:
		if y +1 >= length_y:
			return true
		if cost_map[x][y+1] > 10:
			return true
	if vector.x >0:
		if x +1 >= length_x:
			return true
		if cost_map[x+1][y] > 10:
			return true
	if vector.y <0:
		if y - 1 < 0:
			return true
		if cost_map[x][y-1] > 10:
			return true
	if vector.x <0:
		if x - 1 < 0:
			return true	
		if cost_map[x-1][y] > 10:
			return true
	if vector.x == 0 and vector.y == 0:
		return true
	return false

func cells_around(x,y):
	#variablen resetten
	next_cells = []
	origin_cell_vector = vector_map[x][y]
	old_value = distance_map[x][y]
	
	#up
	var new_y = y - 1
	if new_y >= 0:
		if !calculated_cell_map[x][new_y]:
			if cost_map[x][new_y] < 1000:
				add_vector_to_cell(x, new_y, old_value,Vector2.DOWN)
				if amount_of_vectors_map[x][new_y] == 1:
					next_cells.append(Vector2(x, new_y))
	#down
	new_y = y + 1
	if new_y < length_y:
		if !calculated_cell_map[x][new_y]:
			if cost_map[x][new_y] < 1000:
				add_vector_to_cell(x, new_y, old_value,Vector2.UP)
				if amount_of_vectors_map[x][new_y] == 1:
					next_cells.append(Vector2(x, new_y))
	#left
	var new_x = x - 1
	if new_x>=0:
		if !calculated_cell_map[new_x][y]:
			if cost_map[new_x][y] < 1000:		
				add_vector_to_cell(new_x, y, old_value,Vector2.RIGHT)
				if amount_of_vectors_map[new_x][y] == 1:
					next_cells.append(Vector2(new_x, y))
	#right
	new_x = x +1
	if new_x < length_x:
		if !calculated_cell_map[new_x][y]:
			if cost_map[new_x][y] < 1000:
				add_vector_to_cell(new_x, y, old_value,Vector2.LEFT)
				if amount_of_vectors_map[new_x][y] == 1:
					next_cells.append(Vector2(new_x, y))

func add_vector_to_cell(x,y, old_amount, direction_vector):
	#calculated_cell_map[x][y] = true
	var new_vector = origin_cell_vector
	if amount_of_vectors_map[x][y] != 0:
		new_vector = new_vector/amount_of_vectors_map[x][y] + vector_map[x][y]
	distance_map[x][y] = old_amount + 1
	amount_of_vectors_map[x][y] +=1
	#new_vector = origin_cell_vector + direction_vector
	if wall_in_walk_direction(x,y, new_vector):
		vector_map[x][y] = direction_vector
	else:
		vector_map[x][y] = new_vector.normalized()

func set_vector_cells_around_player():
	for cell in start_cells:
	
		var dislocate_x = -1
		var dislocate_y = 0
	
		while dislocate_x < 2:
			if set_vector_cell_to_player(cell.x + dislocate_x, cell.y, cell):
				new_set_interesting_cells.append(Vector2(cell.x + dislocate_x,cell.y))
				cells_from_update_call.append(Vector2(cell.x + dislocate_x,cell.y))
			dislocate_x+=2
	
		dislocate_y = -1
		dislocate_x = 0
		while dislocate_y < 2:
			if set_vector_cell_to_player(cell.x, cell.y + dislocate_y, cell):
				new_set_interesting_cells.append(Vector2(cell.x,cell.y + dislocate_y))
				cells_from_update_call.append(Vector2(cell.x,cell.y + dislocate_y))
			dislocate_y+=2

func set_vector_cell_to_player(x,y, cell):
	if x<0:
		return false
	if y<0:
		return false
	if x>=length_x:
		return false
	if y>=length_y:
		return false
	elif cost_map[x][y] == 1:
		if cell.x <0 or cell.x >= vector_map.size() or cell.y<0 or cell.y >= vector_map[0].size():
			return false
		distance_map[x][y] = 1
		var new_vector = Vector2(cell.x - x, cell.y-y)
		vector_map[x][y] = new_vector.normalized()
		calculated_cell_map[x][y] = true
		return true
	return false

func update_arrows():
	for x in range(vector_map.size()):
		for y in range(vector_map[0].size()):
			if cost_map[x][y] == 1:
				all_arrows[x][y].set_rot(vector_map[x][y].angle())

func reset_all_maps(steps, keep_vectors):

	var lowest_x = length_x
	var highest_x = 0
	var lowest_y = length_y
	var highest_y = 0
	
	for start_point in start_points:
		if !(lowest_x == 0 and highest_x == length_x - 1):
			var tile_x = floor(start_point.x / 48)
			if tile_x < lowest_x:
				lowest_x = tile_x
			if tile_x >highest_x:
				highest_x = tile_x
				
		if !(lowest_y == 0 and highest_y == length_y - 1):
			var tile_y = floor(start_point.y / 48)
			if tile_y < lowest_y:
				lowest_y = tile_y
			if tile_y >highest_y:
				highest_y = tile_y
		
	
	
	
	var start_x = lowest_x - steps
	if start_x <0:
		start_x = 0
	var end_x = highest_x + steps
	if end_x >= length_x:
		end_x = length_x -1
	var start_y = lowest_y - steps
	if start_y <0:
		start_y = 0
	var end_y = highest_y + steps
	if end_y >= length_y:
		end_y = length_y -1
	var current_x = start_x
	
	while current_x <= end_x:
		var current_y = start_y
		while current_y <= end_y:
			calculated_cell_map[current_x][current_y] = false
			#distance_map[current_x][current_y] = max_distance
			amount_of_vectors_map[current_x][current_y] = 0
			if !keep_vectors:
				vector_map[current_x][current_y] = Vector2.ZERO
			
			current_y += 1
		current_x +=1



func is_there_a_wall(tile_x,tile_y) ->bool:
	if tile_x <0 :
		return true
	if tile_x >=length_x:
		return true
	if tile_y <0:
		return true
	if tile_y >= length_y:
		return true
	if cost_map[tile_x][tile_y] == 1:
		return false
	return true

func set_start_points(positions):
	start_points = []
	for position in positions:
		start_points.append(position.global_position)

func update_map(steps, positions, keep_vectors =true, reset_maps = true):
	update_call = true
	set_start_points(positions)
	reset_cells(cells_from_update_call, keep_vectors)
	#cells_visitable(cells_from_continue_call)
	#reset_all_maps(steps + 1, keep_vectors)
	
	cells_from_update_call = []
	update_vector_map(steps)

#func cells_visitable(relevant_cells):
#	for cell in relevant_cells:
#		calculated_cell_map[cell.x][cell.y] = false
#		amount_of_vectors_map[cell.x][cell.y] = 0
	#	vector_map[cell.x][cell.y] = Vector2.ZERO
	#	distance_map[cell.x][cell.y] = max_distance

func reset_cells(relevant_cells, keep_vectors):
	for cell in relevant_cells:
		calculated_cell_map[cell.x][cell.y] = false
		amount_of_vectors_map[cell.x][cell.y] = 0
		
		if !keep_vectors:
			vector_map[cell.x][cell.y] = Vector2.ZERO
			distance_map[cell.x][cell.y] = max_distance
		#all_arrows[cell.x][cell.y].set_rot(vector_map[cell.x][cell.y].angle())

func continue_update(steps, keep_vectors = true):

	update_call = false
	reset_cells(cells_from_continue_call, keep_vectors)
	cells_from_continue_call = []
	
	find_new_set_interesting_cells(steps)
	#update_arrows()

func get_vector_direction(tile_x,tile_y, object_position) ->Vector2:
	return vector_map[tile_x][tile_y]

func get_distance_of_cell(tile_x,tile_y):
	return distance_map[tile_x][tile_y]
