extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var gold_text = get_node("CanvasLayer/Ressources/Gold")
onready var nahrung_text = get_node("CanvasLayer/Ressources/Nahrung")
onready var iron_text = get_node("CanvasLayer/Ressources/Iron")
onready var stone_text = get_node("CanvasLayer/Ressources/Stone")
onready var einwohner_text = get_node("CanvasLayer/Ressources/Einwohner")


func update_ressources():
	gold_text.set_text(str(Global.gold))
	nahrung_text.set_text(str(Global.nahrung))
	iron_text.set_text(str(Global.iron))
	stone_text.set_text(str(Global.stone))
	einwohner_text.set_text(str(Global.persons))
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func recieve_ressources():
	var all_towns = get_tree().get_nodes_in_group("Towns")
	if Global.after_fight == true:
		for town in all_towns:
			Global.gold+=Global.gold_per_fight
			Global.iron+=Global.iron_per_fight
			Global.stone+=Global.stone_per_fight
			Global.persons+=Global.persons_per_fight
			Global.nahrung+=Global.nahrung_per_fight
		Global.after_fight = false
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	update_ressources()
	recieve_ressources()
