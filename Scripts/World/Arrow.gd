extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var id_x = -1
var id_y = -1

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func get_id():
	return Vector2(id_x,id_y)
	
func set_rot(angle):
	self.rotation = angle


func set_id(new_id : Vector2):
	id_x = new_id.x
	id_y = new_id.y
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
