extends Node2D

var black_demon = preload("res://Scenes/Enemies/Demon.tscn")
var ghost_demon = preload("res://Scenes/Enemies/GhostDemon.tscn")

var current_frame = 0
var frame_cycle = 4

var black_demons_to_summon = 0
var ghost_demons_to_summon = 0
var ratio_black_to_ghost = 0

var max_black_demons_on_field = 20
var max_ghost_demons_on_field = 40

var black_demons_summoned = 0
var ghost_demons_summoned = 0

var black_demons_on_field = 0
var ghost_demons_on_field = 0

var killed_black_demons = 0
var killed_ghost_demons = 0

export (NodePath) var grid_to_player_node
export (NodePath) var grid_to_demons_node
export (NodePath) var grid_to_soldiers_node

onready var grid_to_player = get_node(grid_to_player_node)
onready var grid_to_demons = get_node(grid_to_demons_node)
onready var grid_to_soldiers = get_node(grid_to_soldiers_node)


onready var gui = $InformationLayer/GUI


var all_demons = []
var demons_with_aggro = []
var demons_with_aggro_relevant_cells = []

var all_soldiers = []
var all_soldiers_relevant_cells = []

var all_players = []
var all_players_relevant_cells = []
var demons_to_queue = []
var fight_over = false


onready var player = $Player
onready var music = $Musik


# Called when the node enters the scene tree for the first time.
func _ready():
	if Settings.enable_music:
		music.volume_db = Settings.music_volume
		music.play()
	
	Global.after_fight = true
	#get_tree().paused=true
	all_players.append(player)
	black_demons_to_summon = Global.black_demons_to_summon
	ghost_demons_to_summon = Global.ghost_demons_to_summon
	max_black_demons_on_field = Global.max_black_demons_on_the_field
	max_ghost_demons_on_field = Global.max_ghost_demons_on_the_field
	
	grid_to_player.update_map(3000, all_players)
	
	gui.set_quest(black_demons_to_summon, ghost_demons_to_summon)
	
	if ghost_demons_to_summon ==0:
		ratio_black_to_ghost = 1
	else:
		ratio_black_to_ghost = float(black_demons_to_summon) / ghost_demons_to_summon
	$Player.in_battle = true


func give_demon_to_summon():
	var remaining_black_demons = black_demons_to_summon - black_demons_summoned
	var remaining_ghost_demons = ghost_demons_to_summon - ghost_demons_summoned
	
	if remaining_black_demons <= 0 and remaining_ghost_demons <= 0:
		print("Fehler, zu viele Dämonen")
		black_demons_summoned +=1
		return "black_demon"

	if remaining_black_demons <= 0:
		ghost_demons_summoned +=1
		return "ghost_demon"
		

	if remaining_ghost_demons <= 0:
		black_demons_summoned +=1
		return "black_demon"
	
	if black_demons_on_field >= max_black_demons_on_field:
		return "ghost_demon"
	if ghost_demons_on_field >= max_ghost_demons_on_field:
		return "black_demon"
	
	var remaining_ratio = float(remaining_black_demons) / remaining_ghost_demons

	
	if remaining_ratio > ratio_black_to_ghost:
		black_demons_summoned +=1
		return "black_demon"
	else:
		ghost_demons_summoned +=1
		return "ghost_demon"


func add_demon(demon, type):
	if type =="Ghost":
		ghost_demons_on_field += 1
	if type == "Black":
		black_demons_on_field += 1
	
	all_demons.append(demon)


func remove_demon(demon, type):
	if type =="Ghost":
		ghost_demons_on_field -= 1
		killed_ghost_demons += 1

	if type == "Black":
		black_demons_on_field -= 1
		killed_black_demons += 1
	
	if killed_black_demons >= black_demons_to_summon and killed_ghost_demons >= ghost_demons_to_summon:
		gui.on_victory(black_demons_to_summon + ghost_demons_to_summon)
		$Player.has_won = true
		$Player/Skill_Controller.queue_free()
		Global.won_fight = true
		return

	var test1 = str(killed_black_demons + killed_ghost_demons)
	var test2 = str(Global.summoning_portal_demons_to_kill)
	var text = "Quest\n\n Töte Dämonen\n\n "+ str(test1) + "/" + str(test2)
	gui.set_quest(black_demons_to_summon - killed_black_demons, ghost_demons_to_summon - killed_ghost_demons)
	
	all_demons.erase(demon)

func add_demon_aggro(demon):
	demons_with_aggro.append(demon)
	
func remove_demon_aggro(demon):
	demons_with_aggro.erase(demon)
	
	
func reset_demons_collision_allowed():
	for demon in all_demons:
		pass
		demon.set_collision_allowed(false)
		
func allow_collision_lower_half_demons():
	reset_demons_collision_allowed()
	var demon_limit = ceil( all_demons.size() / 2)
	var x = 0
	while x<demon_limit:
		all_demons[x].set_collision_allowed(true)
		x+=1

func allow_collision_upper_half_demons():
	reset_demons_collision_allowed()
	var demon_start = ceil( all_demons.size() / 2)
	var x = demon_start
	while x<all_demons.size():
		all_demons[x].set_collision_allowed(true)
		x+=1

func add_soldier(soldier):
	all_soldiers.append(soldier)

func remove_soldier(soldier):
	all_soldiers.erase(soldier)
	Global.current_soldier_on_field-=1

func update_map_demons():
	grid_to_demons.update_map(5,all_demons, false)
	
func update_map_soldiers():
	grid_to_soldiers.update_map(5, all_soldiers, false)
	
func update_map_player():
	grid_to_player.update_map(20, all_players)


func can_demon_be_summoned():
	if ghost_demons_on_field >= max_ghost_demons_on_field and black_demons_on_field >= max_black_demons_on_field:
		return false

	if (ghost_demons_to_summon - ghost_demons_summoned) <= 0 and (black_demons_to_summon - black_demons_summoned) <= 0:
		return false
	
	return true

func delete_old_demons():
	for demon in demons_with_aggro:
		if !is_instance_valid(demon):
			print ("Fix Later, demon not rightfully deleted")
			demons_with_aggro.erase(demon)


func kill_one_demon():
	if demons_to_queue.size() != 0:
		demons_to_queue[0].free()
		demons_to_queue.remove(0)

func add_me_to_kill(demon):
	demons_to_queue.append(demon)

func _process(delta):
	kill_one_demon()
	demons_with_aggro = []
	
	if fight_over:
		return
	current_frame +=1
	
	#print(demons_with_aggro.size())
	
	if current_frame == frame_cycle:
		current_frame = 0
		
	if current_frame == 0:
		#movement_grid.update_map(2000)
		update_map_player()

	if current_frame == 1:
		update_map_soldiers()
		allow_collision_lower_half_demons()

	if current_frame == 2:
		update_map_demons()
		allow_collision_upper_half_demons()

	if current_frame == 3:
		grid_to_player.continue_update(5)
		grid_to_soldiers.continue_update(3, false)
		grid_to_demons.continue_update(2, false)
