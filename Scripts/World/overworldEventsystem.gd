extends Node2D

var black_demon = preload("res://Scenes/Overworld/DemonGroup.tscn")


export (NodePath) var player_node
export (NodePath) var animation_camera_node
export (NodePath) var normal_camera_node
export (NodePath) var cutscene_layer_node
export (NodePath) var schmiede_node
export (NodePath) var shop_node
export (NodePath) var caserne_node

onready var schmiede = get_node(schmiede_node) 
onready var shop = get_node(shop_node) 
onready var caserne = get_node(caserne_node) 
onready var ressouceSound = $getRessources

onready var cutscene_layer = get_node(cutscene_layer_node) 
onready var player = get_node(player_node)

onready var animation_camera = get_node(animation_camera_node)
onready var normal_camera = get_node(normal_camera_node)

onready var all_places_captured_by_demons = []
onready var all_possible_targets = []
onready var places_under_attack = []

var all_places =[]
var runde = 0
var town_currently_visiting = null
var town_currently_visiting_as_node = null
var calc_lost_rounds = 0

var new_places_under_attack = []
var new_places_captured = []

var new_newly_places_under_attack = []

var new_newly_places_captured = []
var player_can_move = true

var town_freed_cutscene = false
var town_freed




func set_town_currently_visiting(town_node):
	town_currently_visiting_as_node = town_node
	town_currently_visiting = town_node.name
	Global.current_town_visiting = town_node.name
	Global.save_game()

func town_freed():
	town_currently_visiting_as_node.set_freed()
	town_freed = town_currently_visiting_as_node
	town_freed_cutscene = true
	for place in Global.alle_orte_dict:
		if place.name == town_currently_visiting:
			place.under_attack = false
			place.captured = false
			place.protected_rounds = (Global.runde / 4)
			
			if place.name == Global.demon_castle.name:
				player_won()
	

func player_won():
	Global.switch_to_main_menu()

func animation_finished():
	player_can_move = true


func _ready():
	
	if Settings.enable_music:
		$Musik.volume_db = Settings.music_volume
		$Musik.play()
	
	town_currently_visiting = Global.current_town_visiting
	town_currently_visiting_as_node = get_node("Maps/" + str(town_currently_visiting))
	var move_Vector = Vector2(0,-96)
	
	if town_currently_visiting == "Schmiede":
		player.global_position = schmiede.global_position - move_Vector/3
	elif town_currently_visiting == "Kaserne":
		player.global_position = caserne.global_position - move_Vector/3
	elif town_currently_visiting == "Shop":
		player.global_position = shop.global_position - move_Vector/3
	
	else:
		player.global_position = get_node("Maps/"+ Global.current_town_visiting + "/Enter_point").global_position - move_Vector
	
	player.global_position = Vector2(floor(player.global_position.x / 48) * 48 + 24,
									floor(player.global_position.y / 48) * 48 + 24)
	
	randomize()
	new_ready()
	return






func new_ready():
	check_captured_and_attacked_places()
	let_towns_update()
	
	if Global.won_fight:
		town_freed()
		Global.won_fight = false
	
	if Global.after_fight:
		Global.after_fight = false
		new_attack_round()
		if Global.player_died:
			#new_attack_round()		# A bit too brutal
			Global.player_died = false

	elif Global.game_got_loaded:
		play_cutscene()
		Global.game_got_loaded = false


func play_cutscene():
	if Settings.ignore_cutscenes:
		return
	player.set_in_cutscene(true)
	animation_camera.global_position = player.global_position
	cutscene_layer.start_animation()
	yield(cutscene_layer,"finished_animation")
	animation_camera.current = true
	
	
	
	
	for newly_captured_place in new_newly_places_captured:
		newly_captured_place.reset_shader_and_wait()
	
	if town_freed_cutscene:
		animation_camera.global_position = town_currently_visiting_as_node.get_node("Enter_point").global_position
		yield(town_currently_visiting_as_node,"finished_animation")
		town_freed_cutscene = false
	
	
	
	for newly_captured_place in new_newly_places_captured:
		var my_target = newly_captured_place.get_node("Enter_point")
		animation_camera.go_to_point(my_target.global_position)
		yield(animation_camera, "goal_reached")
		newly_captured_place.start_animation()
		yield(newly_captured_place,"finished_animation")

	for newly_attacked_place in new_newly_places_under_attack:
		print("Hey")
		var nodepath ="Maps/" + newly_attacked_place.name + "/Enter_point"
		animation_camera.go_to_point(get_node(nodepath).global_position)
		yield(animation_camera, "goal_reached")
		yield(get_tree().create_timer(0.5),"timeout")
	
	if !Global.shop_besucht and !shop.get_parent().get_parent().is_captured():
		animation_camera.go_to_point(shop.global_position)
		yield(animation_camera, "goal_reached")
		yield(get_tree().create_timer(0.5),"timeout")
	
	if !Global.schmiede_besucht and !schmiede.get_parent().get_parent().is_captured():
		animation_camera.go_to_point(schmiede.global_position)
		yield(animation_camera, "goal_reached")
		yield(get_tree().create_timer(0.5),"timeout")

	if !Global.kaserne_besucht and !caserne.get_parent().get_parent().is_captured():
		animation_camera.go_to_point(caserne.global_position)
		yield(animation_camera, "goal_reached")
		yield(get_tree().create_timer(0.5),"timeout")
	

	animation_camera.go_to_point(player.global_position)
	yield(animation_camera, "goal_reached")
	cutscene_layer.close_animation()
	yield(cutscene_layer,"finished_animation")
	normal_camera.current = true
	player.set_in_cutscene(false)
	

	
func get_new_attack_targets():
	for place in new_places_captured:
		var the_node = get_node("Maps/" +place.name)
		if is_instance_valid(the_node):
			var more_attack_points = the_node.get_possible_targets()
			for attack_point in more_attack_points:
				if !(attack_point in all_possible_targets) and attack_point.is_attackable():
					all_possible_targets.append(attack_point)
func new_take_places():
	print (Global.after_fight)
	print (Global.player_died)
	new_newly_places_captured  =[]
	for place in Global.alle_orte_dict:
		place.protected_rounds -= 1
		if place.under_attack:
			if place.name == Global.main_city.name and !Global.after_fight and Global.player_died:
				player_lost()
			elif place.name != Global.main_city.name:
				place.captured = true
				place.under_attack = false
				place.black_demons *=2
				place.ghost_demons *=2
				var place_as_node = get_node("Maps/"+place.name)
				place_as_node.remove_demon_childs()
				new_newly_places_captured.append(place_as_node)


func player_lost():
	get_tree().quit()

func new_attack_round():
	Global.runde += 1
	
	update_ressources()

	new_take_places()
	check_captured_and_attacked_places()
	let_towns_update()
	all_possible_targets = []
	new_newly_places_under_attack = []
	get_new_attack_targets()
	var number_to_attack = Global.runde + 1
	var number_of_demons = 10 + 10 * Global.runde
	
	var ratio_black_demons = float(Global.Mages) / (1 + (Global.einfacher_fusssoldat + Global.Mages))
	
	if ratio_black_demons < 0.1:
		ratio_black_demons = 0.1
	var ratio_ghost_demons = 1 - ratio_black_demons

	if all_possible_targets.size() != 0:
	
		for i in range(0,number_to_attack):
			var random_int = randi() % all_possible_targets.size()
			var place_to_attack = all_possible_targets[random_int]
			if update_ort_under_attack(place_to_attack.name,round(number_of_demons * ratio_black_demons),
														 round(number_of_demons * ratio_ghost_demons)):
			
				places_under_attack.append(place_to_attack)
				place_to_attack.set_under_attack()
				if !(place_to_attack in new_newly_places_under_attack):
					new_newly_places_under_attack.append(place_to_attack)

		

	check_captured_and_attacked_places()
	let_towns_update()
	Global.save_game()
	play_cutscene()
	
func let_towns_update():
	for place in Global.alle_orte_dict:
		var place_node = get_node("Maps/" +place.name)
		if is_instance_valid(place_node):
			place_node.update_stats(place)
func check_captured_and_attacked_places():
	for place in Global.alle_orte_dict:
		if place.captured:
			new_places_captured.append(place)
		elif place.under_attack:
			new_places_under_attack.append(place)
func _process(delta):
	#if Input.is_action_just_pressed("ui_attack"):
		#new_attack_round()
	pass

func update_ressources():
	var gold_earning = 0
	var iron_earning = 0
	var stone_earning = 0
	var food_earning = 0
	var people_earning = 0
	
	for place in Global.alle_orte_dict:
		if !place.captured and !place.under_attack:
			gold_earning += place.gold
			iron_earning += place.iron
			stone_earning += place.stone
			food_earning += place.food
			people_earning += place.people

	if Settings.enable_soundeffekte:
		ressouceSound.volume_db = Settings.soundeffekte_volume
		ressouceSound.play()

	Global.gold += gold_earning
	Global.iron += iron_earning
	Global.stone += stone_earning
	Global.nahrung += food_earning
	Global.persons += people_earning


func update_ort_under_attack(ort_name , black_demons, ghost_demons):
	for place in Global.alle_orte_dict:
		if place.name == ort_name:
			if place.protected_rounds <= 0:
				place.under_attack = true
				place.black_demons += black_demons
				place.ghost_demons += ghost_demons
				return true
			else:
				place.protected_rounds -= 1
				return false
	return false
