extends Control


export (NodePath) var all_ressources_gold_node
export (NodePath) var all_ressources_iron_node
export (NodePath) var all_ressources_stone_node
export (NodePath) var all_ressources_bread_node
export (NodePath) var all_ressources_human_node


export (NodePath) var soldier_recruit_gold_cost_node
export (NodePath) var soldier_recruit_iron_cost_node
export (NodePath) var soldier_recruit_stone_cost_node
export (NodePath) var soldier_upgrade_gold_cost_node
export (NodePath) var soldier_upgrade_iron_cost_node
export (NodePath) var soldier_upgrade_stone_cost_node
export (NodePath) var soldier_current_health_node
export (NodePath) var soldier_current_attack_node
export (NodePath) var soldier_current_defense_node
export (NodePath) var soldier_next_health_node
export (NodePath) var soldier_next_attack_node
export (NodePath) var soldier_next_defense_node
export (NodePath) var soldier_explain_text_node
export (NodePath) var soldier_all_upgrade_costs_node
export (NodePath) var soldier_animator_node
export (NodePath) var soldier_amount_node
export (NodePath) var mage_recruit_gold_cost_node
export (NodePath) var mage_recruit_iron_cost_node
export (NodePath) var mage_recruit_stone_cost_node
export (NodePath) var mage_upgrade_gold_cost_node
export (NodePath) var mage_upgrade_iron_cost_node
export (NodePath) var mage_upgrade_stone_cost_node
export (NodePath) var mage_current_health_node
export (NodePath) var mage_current_attack_node
export (NodePath) var mage_current_defense_node
export (NodePath) var mage_next_health_node
export (NodePath) var mage_next_attack_node
export (NodePath) var mage_next_defense_node
export (NodePath) var mage_explain_text_node
export (NodePath) var mage_all_upgrade_costs_node
export (NodePath) var mage_animator_node
export (NodePath) var mage_amount_node

#------------------------------------------------------------------
#Alle Ressourcen Felder
onready var all_ressources_gold = get_node(all_ressources_gold_node)
onready var all_ressources_iron = get_node(all_ressources_iron_node)
onready var all_ressources_stone = get_node(all_ressources_stone_node)
onready var all_ressources_bread = get_node(all_ressources_bread_node)
onready var all_ressources_human = get_node(all_ressources_human_node)



#-------------------------------------------------------------------
#Soldaten Felder

# Anzahl wieviele Soldaten der Spieler
onready var soldier_amount = get_node(soldier_amount_node)

# Soldier ausbildungskosten
onready var soldier_recruit_gold_cost = get_node(soldier_recruit_gold_cost_node)
onready var soldier_recruit_iron_cost = get_node(soldier_recruit_iron_cost_node)
onready var soldier_recruit_stone_cost = get_node(soldier_recruit_stone_cost_node)

# Soldier Verbesserungskosten
onready var soldier_upgrade_gold_cost = get_node(soldier_upgrade_gold_cost_node)
onready var soldier_upgrade_iron_cost = get_node(soldier_upgrade_iron_cost_node)
onready var soldier_upgrade_stone_cost = get_node(soldier_upgrade_stone_cost_node)

# Soldier derzeitige Stats
onready var soldier_current_health = get_node(soldier_current_health_node)
onready var soldier_current_attack = get_node(soldier_current_attack_node)
onready var soldier_current_defense = get_node(soldier_current_defense_node)

# Soldier Stats nach dem Upgrade
onready var soldier_next_health = get_node(soldier_next_health_node)
onready var soldier_next_attack = get_node(soldier_next_attack_node)
onready var soldier_next_defense = get_node(soldier_next_defense_node)

# diese werden von mir behandelt
onready var soldier_explain_text = get_node(soldier_explain_text_node)
onready var soldier_all_upgrade_costs = get_node(soldier_all_upgrade_costs_node)
onready var soldier_animator = get_node(soldier_animator_node)

#--------------------------------------------------------------------------------------
# Mage Felder
# # Anzahl wieviele Mages der Spieler
onready var mage_amount = get_node(mage_amount_node)

# Mage ausbildungskosten
onready var mage_recruit_gold_cost = get_node(mage_recruit_gold_cost_node)
onready var mage_recruit_iron_cost = get_node(mage_recruit_iron_cost_node)
onready var mage_recruit_stone_cost = get_node(mage_recruit_stone_cost_node)

# Mage Verbesserungskosten
onready var mage_upgrade_gold_cost = get_node(mage_upgrade_gold_cost_node)
onready var mage_upgrade_iron_cost = get_node(mage_upgrade_iron_cost_node)
onready var mage_upgrade_stone_cost = get_node(mage_upgrade_stone_cost_node)

# Mage derzeitige Stats
onready var mage_current_health = get_node(mage_current_health_node)
onready var mage_current_attack = get_node(mage_current_attack_node)
onready var mage_current_defense = get_node(mage_current_defense_node)

# Mage Stats nach dem Upgrade
onready var mage_next_health = get_node(mage_next_health_node)
onready var mage_next_attack = get_node(mage_next_attack_node)
onready var mage_next_defense = get_node(mage_next_defense_node)

# diese werden von mir behandelt
onready var mage_explain_text = get_node(mage_explain_text_node)
onready var mage_all_upgrade_costs = get_node(mage_all_upgrade_costs_node)
onready var mage_animator = get_node(mage_animator_node)

onready var button_hover_sound = $ButtonHoverSound
onready var button_pressed_sound = $ButtonPressedSound
onready var error_sound = $ErrorSound

var button_pressed = ""

onready var caserne_music = $caserneMusik
func update_ressources():
	all_ressources_gold.text = str(Global.gold)
	all_ressources_iron.text = str(Global.iron)
	all_ressources_stone.text = str(Global.stone)
	all_ressources_bread.text = str(Global.nahrung)
	all_ressources_human.text = str(Global.persons)


func _ready():
	if Settings.enable_music:
		get_tree().current_scene.get_node("Musik").stop()
		caserne_music.volume_db = Settings.music_volume
		caserne_music.play()
	Global.kaserne_besucht = true
	
	soldier_all_upgrade_costs.visible = false
	soldier_explain_text.visible = true
	
	mage_all_upgrade_costs.visible = false
	mage_explain_text.visible = true
	
	set_mage()
	set_soldier()
	update_ressources()

func set_mage():

	mage_current_attack.text = str(Global.Mage_attack_damage)
	mage_current_defense.text = str(Global.Mage_armor)
	mage_current_health.text = str(Global.Mage_health)
	
	mage_next_attack.text = str(Global.Mage_attack_damage + Global.Mage_attack_per_lvl)
	mage_next_defense.text = str(Global.Mage_armor + Global.Mage_armor_per_lvl)
	mage_next_health.text = str(Global.Mage_health + Global.Mage_health_per_lvl)
	
	var gold_cost = Global.Mage_gold_upgradekosten_base + Global.Mage_gold_upgradekosten_per_lvl * (Global.Mage_level - 1)
	var iron_cost = Global.Mage_iron_upgradekosten_base + Global.Mage_iron_upgradekosten_per_lvl * (Global.Mage_level - 1)
	var nahrung_cost = Global.Mage_food_upgradekosten_base + Global.Mage_food_upgradekosten_per_lvl * (Global.Mage_level - 1)
	
	mage_recruit_gold_cost.text = str(Global.Mage_gold_ausbildungskosten)
	mage_recruit_iron_cost.text = str(Global.Mage_iron_ausbildungskosten)
	mage_recruit_stone_cost.text = str(Global.Mage_food_ausbildungskosten)
	
	mage_upgrade_gold_cost.text = str(gold_cost)
	mage_upgrade_iron_cost.text = str(iron_cost)
	mage_upgrade_stone_cost.text = str(nahrung_cost)
	
	mage_amount.text = str(Global.Mages) + " x"
	


func set_soldier():
	soldier_current_attack.text = str(Global.einfacher_fusssoldat_attack_damage)
	soldier_current_defense.text = str(Global.einfacher_fusssoldat_armor)
	soldier_current_health.text = str(Global.einfacher_fusssoldat_health)
	
	soldier_next_attack.text = str(Global.einfacher_fusssoldat_attack_damage + Global.einfacher_fussoldat_attack_per_lvl)
	soldier_next_defense.text = str(Global.einfacher_fusssoldat_armor + Global.einfacher_fussoldat_armor_per_lvl)
	soldier_next_health.text = str(Global.einfacher_fusssoldat_health + Global.einfacher_fussoldat_health_per_lvl)
	
	
	var gold_cost = Global.einfacher_fussoldat_gold_upgradekosten_base + Global.einfacher_fussoldat_gold_upgradekosten_per_lvl * (Global.einfacher_fusssoldat_level - 1)
	var iron_cost = Global.einfacher_fussoldat_iron_upgradekosten_base + Global.einfacher_fussoldat_iron_upgradekosten_per_lvl * (Global.einfacher_fusssoldat_level - 1)
	var nahrung_cost = Global.einfacher_fussoldat_food_upgradekosten_base + Global.einfacher_fussoldat_food_upgradekosten_per_lvl * (Global.einfacher_fusssoldat_level - 1)

	
	soldier_recruit_gold_cost.text = str(Global.einfacher_fussoldat_gold_ausbildungskosten)
	soldier_recruit_iron_cost.text = str(Global.einfacher_fussoldat_iron_ausbildungskosten)
	soldier_recruit_stone_cost.text = str(Global.einfacher_fussoldat_food_ausbildungskosten)
	
	soldier_upgrade_gold_cost.text = str(gold_cost)
	soldier_upgrade_iron_cost.text = str(iron_cost)
	soldier_upgrade_stone_cost.text = str(nahrung_cost)
	
	soldier_amount.text = str(Global.einfacher_fusssoldat) + " x"
	
	



func play_button_pressed_sound():
	if Settings.enable_soundeffekte:
		button_pressed_sound.volume_db = Settings.soundeffekte_volume - 7
		button_pressed_sound.play()

func play_error_sound():
	if Settings.enable_soundeffekte:
		error_sound.volume_db = Settings.soundeffekte_volume - 7
		error_sound.play()

func play_hover_sound():
	if Settings.enable_soundeffekte:
		button_hover_sound.volume_db = Settings.soundeffekte_volume
		button_hover_sound.play()

func _on_Schlieen_pressed():
	get_tree().current_scene.get_node("Player").set_in_town(false)
	
	caserne_music.stop()
	if Settings.enable_music:
		get_tree().current_scene.get_node("Musik").volume_db = Settings.music_volume
		get_tree().current_scene.get_node("Musik").play()
	
	button_pressed = "quit"
	
	
	
	self.visible = false
	if Settings.enable_soundeffekte:
		play_button_pressed_sound()
	else:
		queue_free()

#Soldaten ausbilden
func _on_Soldier_Ausbilden_pressed():
	if Global.new_soldier():
		play_button_pressed_sound()
	else:
		play_error_sound()
	set_soldier()
	update_ressources()

#Mage ausbilden
func _on_Mage_Ausbilden_pressed():
	if Global.new_Mage():
		play_button_pressed_sound()
	else:
		play_error_sound()
	set_mage()
	update_ressources()

#Soldaten upgraden
func _on_Soldier_upgraden_pressed():
	if Global.lvl_up_soldier():
		play_button_pressed_sound()
	else:
		play_error_sound()
	set_soldier()
	update_ressources()

#Mage upgraden
func _on_Mage_upgraden_pressed():
	if Global.lvl_up_Mage():
		play_button_pressed_sound()
	else:
		play_error_sound()
	set_mage()
	update_ressources()


func _on_Soldier_upgraden_mouse_entered():
	soldier_all_upgrade_costs.visible = true
	soldier_explain_text.visible = false
	play_hover_sound()

func _on_Soldier_upgraden_mouse_exited():
	soldier_all_upgrade_costs.visible = false
	soldier_explain_text.visible = true


func _on_Mage_upgraden_mouse_entered():
	mage_all_upgrade_costs.visible = true
	mage_explain_text.visible = false
	play_hover_sound()

func _on_Mage_upgraden_mouse_exited():
	mage_all_upgrade_costs.visible = false
	mage_explain_text.visible = true



func _on_ButtonPressedSound_finished():
	if button_pressed == "quit":
		queue_free()


func _on_button_mouse_entered():
	play_hover_sound()
