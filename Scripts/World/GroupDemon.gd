extends Node2D

onready var animation_player = $AnimationPlayer

var attack_cd
var attack_cd_left
var is_attacking = false
var is_dead = false

func _ready():
	animation_player.play("Walking_S")
	
	is_attacking = true
	attack_cd = randf()
	attack_cd_left = randf()
	animation_player.play("Attack_S")

func die():
	animation_player.play("Death")
	is_dead = true
	

func _process(delta):
	if is_attacking or is_dead:
		return
	attack_cd_left -= delta
	if attack_cd_left <0:
		animation_player.play("Attack_S")
		is_attacking = true
		

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Death":
		queue_free()
	elif anim_name == "Attack_S":
		animation_player.play("Walking_S")
		attack_cd_left = attack_cd 
		is_attacking = false
