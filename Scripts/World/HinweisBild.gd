extends TextureRect


var speed = 20
var max_offset = 15
var current_offset = 0
var wait_time = 0
var second_wait_time = 0

var start_position


export (NodePath) var conected_town_node

onready var conected_town = get_node(conected_town_node)

# Called when the node enters the scene tree for the first time.
func _ready():
	start_position = self.rect_global_position
	self.material = conected_town.material

func _process(delta):
	check_if_i_should_be_visible()
	current_offset += delta * speed
	
	if current_offset >= max_offset:
		current_offset = max_offset
		wait_time -= delta

	if current_offset <= 0:
		current_offset = 0
		wait_time -= delta *0.3
		
	if wait_time <= 0:
		wait_time = 0.1
		speed = -speed
	
	self.rect_global_position = start_position - Vector2(0,current_offset)

func check_if_i_should_be_visible():
	for place in Global.alle_orte_dict:
		if place.name == conected_town.name:
			if place.captured:
				self.visible = false
