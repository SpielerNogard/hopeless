extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var cost_per_soldat_gold = 100
var cost_per_soldat_persons = 1
var cost_per_soldat_food = 10
var soldats = 0
var gold = 0.0
var persons = 0.0
var food= 0.0
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func new_soldat():
	if gold >= cost_per_soldat_gold and persons>= cost_per_soldat_persons and food >= cost_per_soldat_food:
		soldats += 1
		gold = gold - cost_per_soldat_gold
		food = food - cost_per_soldat_food
		persons = persons - cost_per_soldat_food
		
	else:
		print("Sie haben nicht genug Ressourcen")
		

func open_kaserne_innen():
	var Kaserne_innen_ui = get_node("Kaserneinnen")
	Kaserne_innen_ui.visible = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_TextureButton_pressed():
	open_kaserne_innen()


func _on_Ausbilden_pressed():
	new_soldat()
