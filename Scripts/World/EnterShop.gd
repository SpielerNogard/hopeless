extends Node2D

export(String, FILE, "*.tscn,*.scn") var file_path setget set_file_path
export (NodePath) var relevant_town_node
onready var relevant_town = get_node(relevant_town_node)

var gui_choose_soldier = preload("res://Scenes/GUI/Barracks/Soldatenauswahl.tscn")

onready var informations_container = get_tree().current_scene.get_node("CanvasLayers/InformationLayer")
var shop_menu
var town_dic

func set_file_path(p_value):
	if typeof(p_value) == TYPE_STRING and p_value.get_extension() in ["tscn", "scn"]:
		var d = Directory.new()
		if not d.file_exists(p_value):
			return
		file_path = p_value

func enter_town():
	get_my_stats()
	if !town_dic.captured and !town_dic.under_attack:
		var tmp = load(file_path).instance()
		informations_container.add_child(tmp)


	else:
		var tmp = gui_choose_soldier.instance()
		informations_container.add_child(tmp)
		tmp.set_town_stats(town_dic)

func _ready():
	for place in Global.alle_orte_dict:
		if place.name == relevant_town.name:
			town_dic = place

func get_my_stats():
	for place in Global.alle_orte_dict:
		if place.name == relevant_town.name:
			town_dic = place
			return
		town_dic = Global.ort_error



func _on_Enter_point_body_entered(body):
	body.set_in_town(true)
	body.set_town_node(self)
	get_tree().current_scene.set_town_currently_visiting(self)
