extends VBoxContainer


export (NodePath) var total_gold_node
export (NodePath) var total_iron_node
export (NodePath) var total_stone_node
export (NodePath) var total_food_node
export (NodePath) var total_people_node

export (NodePath) var earning_gold_node
export (NodePath) var earning_iron_node
export (NodePath) var earning_stone_node
export (NodePath) var earning_food_node
export (NodePath) var earning_people_node

onready var total_gold = get_node(total_gold_node)
onready var total_iron = get_node(total_iron_node)
onready var total_stone = get_node(total_stone_node)
onready var total_food = get_node(total_food_node)
onready var total_people = get_node(total_people_node)

onready var earning_gold = get_node(earning_gold_node)
onready var earning_iron = get_node(earning_iron_node)
onready var earning_stone = get_node(earning_stone_node)
onready var earning_food = get_node(earning_food_node)
onready var earning_people = get_node(earning_people_node)

var gold_earning = 0
var iron_earning = 0
var stone_earning = 0
var food_earning = 0
var people_earning = 0

func _ready():
	set_total_ressources()

func _process(delta):
	set_total_ressources()
	set_earnings()
func set_total_ressources():
	total_gold.set_text(str(Global.gold))
	total_iron.set_text(str(Global.iron))
	total_stone.set_text(str(Global.stone))
	total_food.set_text(str(Global.nahrung))
	total_people.set_text(str(Global.persons))

	
func set_earnings():
	var gold_earning = 0
	var iron_earning = 0
	var stone_earning = 0
	var food_earning = 0
	var people_earning = 0
	for place in Global.alle_orte_dict:
		if !place.under_attack and !place.captured:
			gold_earning += place.gold
			iron_earning += place.iron
			stone_earning += place.stone
			food_earning += place.food
			people_earning += place.people
	
	earning_gold.set_text("+" + str(gold_earning))
	earning_iron.set_text("+" + str(iron_earning))
	earning_stone.set_text("+" + str(stone_earning))
	earning_food.set_text("+" + str(food_earning))
	earning_people.set_text("+" + str(people_earning))
	
	
			
			
			
			
			
			
