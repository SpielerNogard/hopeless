extends Node2D

func save_game():
	var save_game = File.new()
	save_game.open("res://savegame.save", File.WRITE)
	if self.filename.empty():
		print("persistent node '%s' is not an instanced scene, skipped" % self.name)
	
	elif !self.has_method("save"):
		print("persistent node '%s' is missing a save() function, skipped" % self.name)
	else:
		var node_data = self.call("save")
		print(node_data)
		save_game.store_line(to_json(node_data))
		save_game.close()

func save():
	var save_dict = {
		"filename" : get_filename(),
		"parent" : get_parent().get_path(),
		"pos_x" : position.x, # Vector2 is not supported by JSON
		"pos_y" : position.y,
		"attack" : 1,
		"defense" : 1,
		"current_health" : 1,
		"max_health" : 2,
		"damage" : 3,
		"regen" : 4,
		"experience" : 5,
		"tnl" : 6,
		"level" : 7,
		"attack_growth" : 8,
		"defense_growth" : 9,
		"health_growth" : 1,
		"is_alive" : 2,
		"last_attack" : 3
	}
	return save_dict
	
func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		print("Test")
		save_game()
