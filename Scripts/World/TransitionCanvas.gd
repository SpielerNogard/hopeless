extends CanvasLayer


onready var shader = $ColorRect
onready var shader_material = shader.material;




var lvl_to_load = null

var transition_direction = -1
var transition_speed = 2
var transition_strength = 1


func _ready():
	shader_material.set_shader_param("transition", transition_strength)
	transition_direction = -transition_speed
	yield (get_tree().create_timer(0.1),"timeout")
	if Settings.enable_soundeffekte:
		$Backwards.volume_db = Settings.soundeffekte_volume - 7
		$Backwards.play()
	
func _process(delta):
	transition_strength += delta * transition_direction
	if transition_strength < 0:
		transition_strength = 0
		shader_material.set_shader_param("transition", transition_strength)
		return

	if transition_strength > 1:
		transition_strength = 1
		shader_material.set_shader_param("transition", transition_strength)
		if !$Forward.playing:
			yield(get_tree().create_timer(0.5),"timeout")
			get_tree().change_scene(lvl_to_load)
		else:
			return
	shader_material.set_shader_param("transition", transition_strength)


func load_next_lvl(relevant_lvl):
	lvl_to_load = relevant_lvl
	transition_direction = transition_speed
	
	if Settings.enable_soundeffekte:
		$Forward.volume_db = Settings.soundeffekte_volume - 7
		$Forward.play()


func _on_Forward_finished():
	pass # Replace with function body.
