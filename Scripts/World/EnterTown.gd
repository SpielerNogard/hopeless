extends Node2D

export(String, FILE, "*.tscn,*.scn") var file_path setget set_file_path
var gui_choose_soldier = preload("res://Scenes/GUI/Barracks/Soldatenauswahl.tscn")
var enter_town = preload("res://Scenes/GUI/Town/Enter_Town.tscn")
var black_demon = preload("res://Scenes/Overworld/DemonGroup.tscn")

export (NodePath) var next_attackable_point_1_node
export (NodePath) var next_attackable_point_2_node
export (NodePath) var next_attackable_point_3_node
export (NodePath) var next_attackable_point_4_node



onready var next_attackable_point_1 = get_node(next_attackable_point_1_node)
onready var next_attackable_point_2 = get_node(next_attackable_point_2_node)
onready var next_attackable_point_3 = get_node(next_attackable_point_3_node)
onready var next_attackable_point_4 = get_node(next_attackable_point_4_node)

onready var informations_container = get_tree().current_scene.get_node("CanvasLayers/InformationLayer")

onready var shader_material = self.material;
onready var all_next_attackable_points = [next_attackable_point_1, next_attackable_point_2,
									next_attackable_point_3, next_attackable_point_4]

var captured_by_demons = false
var under_attack = false
var freed = false

var animation_point = 0
var shader_mode = "free"
var change = 1.5
var number_black_demons = 2
var number_ghost_demons = 2
var wait = false


var my_stats


signal finished_animation


func set_captured_by_demons(black_demons,ghost_demons ):
	captured_by_demons = true
	under_attack = false
	number_ghost_demons = ghost_demons
	number_black_demons = black_demons
	
	shader_material.set_shader_param("animation_point", 0)
	kill_demons()

func is_captured():
	return my_stats.captured

func update_stats(town_dict):
	my_stats = town_dict

	if my_stats.captured:
		captured_by_demons = true
		under_attack = false
		remove_demon_childs()
	elif my_stats.under_attack:
		under_attack = true
		captured_by_demons = false
		add_demons()
	else:
		shader_material.set_shader_param("animation_point", 0)

func reset_shader_and_wait():
	shader_material.set_shader_param("animation_point", 0)
	animation_point = 0
	wait = true

func start_animation():
	wait = false

func set_file_path(p_value):
	if typeof(p_value) == TYPE_STRING and p_value.get_extension() in ["tscn", "scn"]:
		var d = Directory.new()
		if not d.file_exists(p_value):
			return
		file_path = p_value


func _ready():
	material = self.material.duplicate()
	shader_material = material
	#self.connect("killed_all_demons", get_parent(), "finished_killing")
	#self.connect("place_captured",get_parent(), "capture_animation_finished")
	
	get_my_stats()
	

func get_my_stats():
	for place in Global.alle_orte_dict:
		if place.name == self.name:
			my_stats = place
			return
		my_stats = Global.ort_error

func _process(delta):
	if wait:
		return
	if captured_by_demons:
		var transition_point = animation_point
		animation_point += delta * 0.5
		
		if animation_point >1:
			transition_point = 1
		
		if animation_point >1.2:
			#set_process(false)
			animation_point = 1.2
			emit_signal("finished_animation")
		shader_material.set_shader_param("animation_point", transition_point)

	if under_attack:
		var transition_point = animation_point
		animation_point += delta * change
		
		if animation_point >1:
			transition_point = 1
			
		if animation_point < 0:
			transition_point = 0
		
		if animation_point > 2:
			animation_point = 1
			change = -change

		if animation_point < -0.5:
			animation_point = 0
			change = -change
			
		shader_material.set_shader_param("animation_point", transition_point)

	if freed:
		var transition_point = animation_point
		animation_point -= delta * 0.5
		
		if animation_point <0:
			transition_point = 0
		
		if animation_point <-0.5:
			#set_process(false)
			animation_point = -0.5
			emit_signal("finished_animation")
		shader_material.set_shader_param("animation_point", transition_point)

func set_under_attack():
	under_attack = true

func remove_demon_childs():
	var children = $Enter_point.get_children()
	for child in children:
		if child.is_in_group("GroupDemon"):
			child.queue_free()


func set_freed():
	under_attack = false
	captured_by_demons = false
	animation_point = 1
	shader_material.set_shader_param("animation_point", 1)
	print(my_stats.captured)
	
	if !my_stats.captured:
		if !kill_demons():
			return false
		print ("Reached here")
		shader_material.set_shader_param("animation_point", 0)
		animation_point = 0
	else:
		shader_material.set_shader_param("animation_point", 1)
		animation_point = 1
		freed = true
	
	return true

# später für Cooldown zwischen angriffe bei Orten
func is_attackable():
	if my_stats.under_attack or my_stats.captured or my_stats.name == "Error Dorf" or my_stats.protected_rounds > 0:
		return false
	return true

func add_demons():
	var tmp = black_demon.instance()
	$Enter_point.add_child(tmp)

	

func get_possible_targets():
	var relevant_attack_points = []
	for possible_target in all_next_attackable_points:
		if is_instance_valid(possible_target):
			relevant_attack_points.append(possible_target)
	return relevant_attack_points

func kill_demons():
	var children = $Enter_point.get_children()
	var child_found = false
	for child in children:
		if child.is_in_group("GroupDemon"):
			child.kill_demons()
			child_found = true

	if !child_found:
		return false
	else:
		return true
	
func done_killing():
	emit_signal("finished_animation")

func _on_Area2D_body_entered(body):
	body.set_in_town(true)
	body.set_town_node(self)
	get_tree().current_scene.set_town_currently_visiting(self)


func enter_town():
	if !captured_by_demons and !under_attack:
		var tmp = enter_town.instance()
		informations_container.add_child(tmp)
		
		for place in Global.alle_orte_dict:
			if place.name == self.name:
				tmp.set_town_stats(place)
				return

		tmp.set_town_stats(Global.ort_error)

	else:
		var tmp = gui_choose_soldier.instance()
		informations_container.add_child(tmp)
		get_my_stats()
		tmp.set_town_stats(my_stats)




func _on_Enter_point_body_entered(body):
	body.set_in_town(true)
	body.set_town_node(self)
	get_tree().current_scene.set_town_currently_visiting(self)
