extends Camera2D

var speed = 800

var goal_coords

signal goal_reached


func _ready():
	set_physics_process(false)
func go_to_point(point_coords):
	goal_coords = point_coords

	set_physics_process(true)
	

func _physics_process(delta):
	var distance_to_coords = self.global_position.distance_to(goal_coords)
	
	if distance_to_coords < speed * delta *1.5:
		self.global_position = goal_coords
		set_physics_process(false)
		emit_signal("goal_reached")
		return

	else:
		var direction = goal_coords -  self.global_position
		self.position += direction.normalized() * delta * speed
