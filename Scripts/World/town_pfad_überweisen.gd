extends Node2D

export (NodePath) var n_path = null
export (NodePath) var s_path = null
export (NodePath) var w_path = null
export (NodePath) var e_path = null
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func get_path_node(type):
	if type == "N":
		return n_path
	if type == "S":
		return s_path
	if type == "E":
		return e_path
	if type == "W":
		return w_path


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
