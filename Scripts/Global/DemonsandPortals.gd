extends Node



var summoning_portal_demon_summon_time = 1 #In Frames zurzeit
var summoning_portal_current_time = 0
var summoning_portal_summoned_demons = 0
var summoning_portal_next_houndred = 100
var summoning_portal_current_houndred = 0
var summoning_portal_killed_Demons = 0
var summoning_portal_all_killed_Demons = 0
var summoning_portal_demons_to_kill = 30
var summoning_portal_max_demons_on_field = 30
var summoning_portal_current_demons_on_field = 0

var demon_art_to_summon = "mix" #mix, black, ghost
var black_demon = preload("res://Scenes/Enemies/Demon.tscn")
var ghost_demon = preload("res://Scenes/Enemies/GhostDemon.tscn")

func give_demon_to_summon():
	var demon_to_summon = black_demon
	if demon_art_to_summon == "mix":
		var zahl = randi()%2+1
		if zahl == 2:
			demon_to_summon = ghost_demon
		elif zahl == 1:
			demon_to_summon = black_demon
	elif demon_art_to_summon == "black":
		demon_to_summon = black_demon
	elif demon_art_to_summon == "ghost":
		demon_to_summon = ghost_demon
		
	return(demon_to_summon)
			
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
