extends Node


var player_name = "Gustav"
var player_current_look_direction = "S"
#Health
var player_max_health = 100
var player_current_health = 100000
var player_health_per_lvl = 50
var player_health_reg = 0

#Mana
var player_max_mana = 100
var player_current_mana = 100
var player_mana_per_lvl = 50
var player_mana_reg = 0

#XP
var player_current_lvl = 1
var player_current_xp = 0
var player_xp_next_lvl = 100
var player_xp_multiplier = 1.2
var player_overall_xp = 0

#Attack
var player_attack_damage = 5
var player_attack_speed = 5

#Walking
var player_speed = 200







# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

