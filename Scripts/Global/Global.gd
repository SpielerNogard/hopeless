extends Node

#---------------------------------------------------------------------------------------------------------------------------------
#Soldaten
#komplett Anzahl Soldaten
var soldats = 0
#Soldatenarten
#einfacher Fußsoldat
var einfacher_fusssoldat = 5
var einfacher_fussoldat_fur_Schlacht = 0
var einfacher_fusssoldat_distance_to_player = 1000
var einfacher_fusssoldat_health = 200
var einfacher_fusssoldat_armor = 10
var einfacher_fusssoldat_attack_damage = 2
var einfacher_fusssoldat_max_number_of_aggroed_demons = 10
var einfacher_fusssoldat_speed = 200
var einfacher_fusssoldat_level = 1
var einfacher_fussoldat_food_cost = 0
var einfacher_fussoldat_persons_cost = 0
var einfacher_fussoldat_gold_ausbildungskosten = 200
var einfacher_fussoldat_iron_ausbildungskosten = 50
var einfacher_fussoldat_food_ausbildungskosten = 100


var einfacher_fussoldat_gold_upgradekosten_base = 300
var einfacher_fussoldat_iron_upgradekosten_base = 300
var einfacher_fussoldat_food_upgradekosten_base = 300

var einfacher_fussoldat_gold_upgradekosten_per_lvl = 300
var einfacher_fussoldat_iron_upgradekosten_per_lvl = 300
var einfacher_fussoldat_food_upgradekosten_per_lvl = 300


var einfacher_fussoldat_gold_cost = 100
var einfacher_fussoldat_iron_cost = 100
var einfacher_fussoldat_stone_cost = 100

var einfacher_fussoldat_base_attack = 2
var einfacher_fussoldat_attack_per_lvl = 1

var einfacher_fussoldat_base_armor = 100
var einfacher_fussoldat_armor_per_lvl = 80

var einfacher_fussoldat_base_health = 5000
var einfacher_fussoldat_health_per_lvl = 2000

#Mage
var Mages = 5
var Mages_fur_Schlacht = 20
var Mage_health = 100
var Mage_speed = 300
var Mage_attack_damage = 20
var Mage_level = 1
var Mage_food_cost = 100
var Mage_persons_cost = 100
var Mage_gold_cost = 100
var Mage_iron_cost = 100
var Mage_stone_cost = 100
var Mage_armor = 1


var Mage_gold_ausbildungskosten = 800
var Mage_iron_ausbildungskosten = 10
var Mage_food_ausbildungskosten = 200


var Mage_gold_upgradekosten_base = 300
var Mage_iron_upgradekosten_base = 300
var Mage_food_upgradekosten_base = 300

var Mage_gold_upgradekosten_per_lvl = 300
var Mage_iron_upgradekosten_per_lvl = 300
var Mage_food_upgradekosten_per_lvl = 300

var Mage_base_attack = 20
var Mage_attack_per_lvl = 10

var Mage_base_armor = 10
var Mage_armor_per_lvl = 3

var Mage_base_health = 500
var Mage_health_per_lvl = 200



#Soldaten auf Schlachtfeld
var current_soldier_on_field = 0
var max_soldier_on_field = 30
#---------------------------------------------------------------------------------------------------------------------------------
#Black Demon
var black_demon_max_health = 100
var black_demon_xp_amount = 2
var black_demon_attack_damage = 100
var black_demon_max_speed = 200
var black_demon_attack_distance_to_player = 50
var black_demon_attack_speed = 0.5
#---------------------------------------------------------------------------------------------------------------------------------
#Ghost_Demon
var ghost_demon_max_speed = 150
var ghost_demon_speed_multiplier = 2
var ghost_demon_distance_to_player = 10000
var ghost_demon_attack_damage = 30
var ghost_demon_attack_cd = 2	#in s
var ghost_demon_base_speed = 100
var ghost_demon_xp_amount = 1
var ghost_demon_max_health = 25
#---------------------------------------------------------------------------------------------------------------------------------
#Player Stats
var player_name = "Gustav"
var player_current_look_direction = "S"
var current_town_visiting = "Main City"
var won_fight = false
var game_got_loaded = false

var default = ["default",preload("res://Assets/GUI/Skill_Icon/White_Box.png")]
var thunder_storm = ["thunder_storm",preload("res://Assets/GUI/Skill_Icon/if762.png")]
var pocket_sand = ["pocket_sand",preload("res://Assets/GUI/Skill_Icon/32_6.png")]
var holy_heal = ["holy_heal",preload("res://Assets/GUI/Skill_Icon/Basic_10_T.png")]
var stone_skin = ["stone_skin",preload("res://Assets/GUI/Skill_Icon/Combat_16_T.png")]
var all_skills = [thunder_storm,pocket_sand,holy_heal,stone_skin,default]
var player_skills = [thunder_storm,pocket_sand,holy_heal,stone_skin]
#Health
var player_max_health = 1000
var player_base_health = 3000
var player_current_health = 100000
var player_health_per_lvl = 300
var player_health_reg = 0

#Mana
var player_base_mana = 300
var player_max_mana = 5
var player_current_mana = 100000
var player_mana_per_lvl = 50
var player_mana_reg = 0

#XP
var player_current_lvl = 1
var player_current_xp = 0
var player_xp_next_lvl = 100
var player_xp_multiplier = 1.2
var player_overall_xp = 0

#Attack
var player_attack_damage = 500
var player_base_attack_damage = 10
var player_attack_per_lvl = 5

var player_armor = 50
var player_base_armor = 50
var player_armor_multiplier = 1

var player_attack_speed = 5

#Walking
var player_base_movement_speed = 200
var player_start_speed = 200
var player_speed = 200
var player_speed_up_speed = 1
#---------------------------------------------------------------------------------------------------------------------------------
#Ressourcen Daten
var after_fight = true
var player_died = false
#Nahrung
var nahrung = 0.0
var nahrung_per_fight = 100

#Gold
var gold = 0
var gold_per_fight = 100

#Iron
var iron= 0
var iron_per_fight = 100

#Stone
var stone= 0
var stone_per_fight = 100

#Bevölkerung
var persons= 0
var persons_per_fight = 100




var ort_error = {
	"name":"Error Dorf",
	"gold":-404,
	"iron":-404,
	"stone":-404,
	"food":-404,
	"people":-404,
	"black_demons":404,
	"ghost_demons":404,

	"protected_rounds":0,
	"captured":false,
	"under_attack":false
}
# Ort_1
var demon_castle = {
	"name":"Demon Castle",
	"gold":100000,
	"iron":100000,
	"stone":100000,
	"food":100000,
	"people":100,
	"black_demons":2000,
	"ghost_demons":1000,

	"protected_rounds":0,
	"captured":false,
	"under_attack":true
}

var main_city = {
	"name":"Main City",
	"gold":500,
	"iron":500,
	"stone":500,
	"food":500,
	"people":5,
	"black_demons":0,
	"ghost_demons":0,

	"protected_rounds":0,
	"captured":false,
	"under_attack":false
}

var ort_3 = {
	"name":"Ertonson",
	"gold":100,
	"iron":100,
	"stone":30,
	"food":900,
	"people":4,
	"black_demons":0,
	"ghost_demons":0,
	
	"protected_rounds":0,
	"captured":false,
	"under_attack":false
}

var ort_4 = {
	"name":"Eshirwell",
	"gold":80,
	"iron":70,
	"stone":80,
	"food":90,
	"people":2,
	"black_demons":0,
	"ghost_demons":0,
	
	"protected_rounds":0,
	"captured":false,
	"under_attack":false
}

var ort_5 = {
	"name":"Ozrita",
	"gold":800,
	"iron":1700,
	"stone":2000,
	"food":0,
	"people":1,
	"black_demons":0,
	"ghost_demons":0,
	
	"protected_rounds":0,
	"captured":false,
	"under_attack":false
}

var ort_6 = {
	"name":"Raidon",
	"gold":5000,
	"iron":0,
	"stone":0,
	"food":0,
	"people":0,
	"black_demons":0,
	"ghost_demons":0,
	
	"protected_rounds":0,
	"captured":false,
	"under_attack":false
}

var ort_7 = {
	"name":"Quastead",
	"gold":2000,
	"iron":800,
	"stone":1200,
	"food":1500,
	"people":6,
	"black_demons":0,
	"ghost_demons":0,
	
	"protected_rounds":0,
	"captured":false,
	"under_attack":false
}

var ort_8 = {
	"name":"Trawood",
	"gold":1000,
	"iron":1000,
	"stone":1000,
	"food":1400,
	"people":1,
	"black_demons":0,
	"ghost_demons":0,
	
	"protected_rounds":0,
	"captured":false,
	"under_attack":false
}



var alle_orte_dict = [demon_castle, main_city, ort_3, ort_4, ort_5, ort_6, ort_7, ort_8]
var runde = 1

var kaserne_besucht = false
var schmiede_besucht = false
var shop_besucht = false

#---------------------------------------------------------------------------------------------------------------------------------
#Kaserne
var all_soldiers_in_barracks = ["res://Scenes/GUI/Barracks/normal_soldier.tscn","res://Scenes/GUI/Barracks/little_mage.tscn"]
var current_barracks_scene = 0
var current_scene = null

#---------------------------------------------------------------------------------------------------------------------------------
#Inventar und Items
#Itemaufbau: [Name,Icon,Level,Eisen pro lvl,gold pro lvl, stone pro lvl,Armor, dmg, slotnr, leben, attack_speed, mana,
var Lederhelm = ["Lederhelm","res://Assets/GUI/Items/helmet/helmet_02.png",1,100,100,100,1,0,0,0,0,0]
var LederRuestung = ["Ledertunika","res://Assets/GUI/Items/armor/armor_03.png",1,100,100,100,1,0,1,0,0,0]

var new_lederhelm = {
	"name": "Lederhelm",
	"icon_path": "res://Assets/GUI/Items/helmet/helmet_02.png",
	"level": 0,
	
	"base_iron": 200,
	"iron_per_lvl":100,
	"base_gold": 20,
	"gold_per_lvl": 60,
	"base_stone": 20,
	"stone_per_lvl": 60,
	
	"base_armor":20,
	"armor_per_lvl":10,
	
	"base_attack":0,
	"attack_per_lvl": 0,
	
	"base_health":200,
	"health_per_lvl":100,
	
	"base_attack_speed":0,
	"attack_speed_per_lvl": 0,
	
	"base_mana": 80,
	"mana_per_lvl" : 100,
	
	"base_movement_speed":0,
	"movement_speed_per_lvl":0,
	"slotnr":1
}

var new_lederschuhe = {
	"name": "lederschuhe",
	"icon_path": "res://Assets/GUI/Items/boot/boot_02.png",
	"level": 0,
	
	"base_iron": 10,
	"iron_per_lvl":30,
	"base_gold": 100,
	"gold_per_lvl": 300,
	"base_stone": 1000,
	"stone_per_lvl": 300,
	
	"base_armor":5,
	"armor_per_lvl":3,
	
	"base_attack":0,
	"attack_per_lvl": 0,
	
	"base_health":5,
	"health_per_lvl":20,
	
	"base_attack_speed":0,
	"attack_speed_per_lvl": 0,
	
	"base_mana": 0,
	"mana_per_lvl" : 0,
	
	"base_movement_speed":100,
	"movement_speed_per_lvl":20,
	
	"slotnr": 2
}

var new_holzschild = {
	"name": "holzschild",
	"icon_path": "res://Assets/GUI/Items/shield/shield_02.png",
	"level": 0,
	
	"base_iron": 500,
	"iron_per_lvl":600,
	"base_gold": 500,
	"gold_per_lvl": 600,
	"base_stone": 500,
	"stone_per_lvl": 600,
	
	"base_armor":50,
	"armor_per_lvl":30,
	
	"base_attack":0,
	"attack_per_lvl": 0,
	
	"base_health":500,
	"health_per_lvl":300,
	
	"base_attack_speed":0,
	"attack_speed_per_lvl": 0,
	
	"base_mana": 0,
	"mana_per_lvl" : 0,
	
	"base_movement_speed":0,
	"movement_speed_per_lvl":0,
	
	"slotnr": 3
}

var new_lederhandschuhe = {
	"name": "lederhandschuhe",
	"icon_path": "res://Assets/GUI/Items/glove/glove_02.png",
	"level": 0,
	
	"base_iron": 300,
	"iron_per_lvl":500,
	"base_gold": 100,
	"gold_per_lvl": 100,
	"base_stone": 200,
	"stone_per_lvl": 400,
	
	"base_armor":10,
	"armor_per_lvl":5,
	
	"base_attack":0,
	"attack_per_lvl": 0,
	
	"base_health":100,
	"health_per_lvl":150,
	
	"base_attack_speed":0.1,
	"attack_speed_per_lvl": 0.05,
	
	"base_mana": 0,
	"mana_per_lvl" : 0,
	
	"base_movement_speed":0,
	"movement_speed_per_lvl":0,
	
	"slotnr": 4
}
var new_eisenschwert = {
	"name": "eisenschwert",
	"icon_path": "res://Assets/GUI/Items/weapon/weapon_07.png",
	"level": 0,
	
	"base_iron": 500,
	"iron_per_lvl":1000,
	
	"base_gold": 700,
	"gold_per_lvl": 700,
	"base_stone": 200,
	"stone_per_lvl": 80,
	
	"base_armor":0,
	"armor_per_lvl":0,
	
	"base_attack":10,
	"attack_per_lvl": 5,
	
	"base_health":0,
	"health_per_lvl":0,
	
	"base_attack_speed":1,
	"attack_speed_per_lvl": 0.2,
	
	"base_mana": 0,
	"mana_per_lvl" : 0,
	
	"base_movement_speed":0,
	"movement_speed_per_lvl":0,
	
	"slotnr": 5
}

#Schuhe
var Lederschuhe= ["Lederschuhe","res://Assets/GUI/Items/boot/boot_02.png",1,100,100,100,1,0,2,0,0,0]
var Lederschuhebesser= ["Lederschuhebesser","res://Assets/GUI/Items/boot/boot_03.png",1,100,100,100,1,0,2,0,0,0]
var Stahlkappenschuhe= ["Stahlkappenschuhe","res://Assets/GUI/Items/boot/boot_04.png",1,100,100,100,1,0,2,0,0,0]
var Plattenstiefel= ["Plattenstiefel","res://Assets/GUI/Items/boot/boot_05.png",1,100,100,100,1,0,2,0,0,0]
var Bauernstiefel= ["Bauernstiefel","res://Assets/GUI/Items/boot/boot_06.png",1,100,100,100,1,0,2,0,0,0]
#Schilder
var Holzschild = ["Holzschild","res://Assets/GUI/Items/shield/shield_02.png",1,100,100,100,1,0,4,0,0,0]
var verstarkHolzschild = ["verstärkter Holzschild","res://Assets/GUI/Items/shield/shield_03.png",1,100,100,100,1,0,4,0,0,0]
var Eisenplattenschild = ["Eisenplattenschild","res://Assets/GUI/Items/shield/shield_04.png",1,100,100,100,1,0,4,0,0,0]
var Eisenschild = ["Eisenschild","res://Assets/GUI/Items/shield/shield_05.png",1,100,100,100,1,0,4,0,0,0]
var Panzerung = ["Panzerung","res://Assets/GUI/Items/shield/shield_06.png",1,100,100,100,1,0,4,0,0,0]
var Dreieck = ["Dreieck","res://Assets/GUI/Items/shield/shield_07.png",1,100,100,100,1,0,4,0,0,0]
#Handschuhe
var LederHandschuhe = ["Lederhandschuhe","res://Assets/GUI/Items/glove/glove_02.png",1,100,100,100,1,0,3,0,0,0]
var LederHandschuhegefuttert = ["gefütterte Lederhandschuhe","res://Assets/GUI/Items/glove/glove_03.png",1,100,100,100,1,0,3,0,0,0]
var LederHandschuheeisen = ["Lederhandschuhe verstärkt","res://Assets/GUI/Items/glove/glove_04.png",1,100,100,100,1,0,3,0,0,0]
var LederHandschuheplatten = ["Lederhandschuhe mit Platten","res://Assets/GUI/Items/glove/glove_05.png",1,100,100,100,1,0,3,0,0,0]
var Eisenhandschuhe = ["Eisenhandschuhe","res://Assets/GUI/Items/glove/glove_08.png",1,100,100,100,1,0,3,0,0,0]
#Waffen
var Eisenschwert = ["Eisenschwert","res://Assets/GUI/Items/weapon/weapon_07.png",1,100,100,100,0,1,5,0,1,0]
var Feuerschwert = ["Feuerschwert","res://Assets/GUI/Items/weapon/weapon_02.png",1,100,100,100,0,1,5,0,0.5,0]
var Stahlschwert = ["Stahlschwert","res://Assets/GUI/Items/weapon/weapon_03.png",1,100,100,100,0,1,5,0,2,0]
var Heilendekraft = ["Heilende Kraft","res://Assets/GUI/Items/weapon/weapon_04.png",1,100,100,100,0,1,5,0,1.7,0]
var Grosschwert = ["Großschwert","res://Assets/GUI/Items/weapon/weapon_05.png",1,100,100,100,0,1,5,0,4,0]
var Blitzknuppel = ["Blitzknüppel","res://Assets/GUI/Items/weapon/weapon_06.png",1,100,100,100,0,1,5,0,0.3,0]

#var Weapons = [[Eisenschwert,true],[Stahlschwert,false],[Feuerschwert,false],[Heilendekraft,false],[Grosschwert,false],[Blitzknuppel,false]]
var Weapons = [[new_eisenschwert,true]]
#var Gloves = [[LederHandschuhe,true],[LederHandschuhegefuttert,false],[LederHandschuheeisen,false],[LederHandschuheplatten,false],[Eisenhandschuhe,false]]
var Gloves = [[new_lederhandschuhe,true]]
#var Shields = [[Holzschild,true],[verstarkHolzschild,false],[Eisenplattenschild,false],[Eisenschild,false],[Panzerung,false],[Dreieck,false]]
var Shields = [[new_holzschild,true]]
#var Boots= [[Lederschuhe,true],[Lederschuhebesser,false],[Stahlkappenschuhe,false],[Plattenstiefel,false],[Bauernstiefel,false]]
var Boots = [[new_lederschuhe,true]]
#var Armors = [[LederRuestung,true],[Lederhelm,true]]
var Armors = [[new_lederhelm,true]]
var inventory = Weapons+Gloves+Shields+Boots+Armors
#---------------------------------------------------------------------------------------------------------------------------------
#ShopSystem
#Item [Name,Icon,Anzahl,Preis]

var new_health_potion = {
	"name":"Heiltrank",
	"icon_path":"res://Assets/GUI/Items/potion/potion_01.png",
	"price":300,
	"owned":0,
	"type":"health",
	
	"strength":30,
	"equipped":true
	
}

var new_mana_potion = {
	"name":"Manatrank",
	"icon_path":"res://Assets/GUI/Items/potion/potion_06.png",
	"price":600,
	"owned":0,
	"type":"mana",
	
	"strength":30,
	"equipped":false
	
}

var Health_Potion = ["Heiltrank","res://Assets/GUI/Items/potion/potion_01.png",10,1]
var Mana_Potion = ["Manatrank","res://Assets/GUI/Items/potion/potion_06.png",10,1]
var Shop_Iventory = [new_health_potion,new_mana_potion]
#var Player_item_iventory = [[Health_Potion,0],[Mana_Potion,0]]
var Player_item_iventory = [new_health_potion, new_mana_potion]
#---------------------------------------------------------------------------------------------------------------------------------
#DemonsandPortals
var summoning_portal_demon_summon_time = 0.1 #In Sekunden
var black_demons_to_summon = 1
var ghost_demons_to_summon = 2
var max_black_demons_on_the_field = 100
var max_ghost_demons_on_the_field = 200


var summoning_portal_current_time = 0
var summoning_portal_summoned_demons = 0
var summoning_portal_next_houndred = 100
var summoning_portal_current_houndred = 0
var summoning_portal_killed_Demons = 0
var summoning_portal_all_killed_Demons = 0
var summoning_portal_demons_to_kill = 500
var summoning_portal_max_demons_on_field = 500
var summoning_portal_current_demons_on_field = 0

var demon_art_to_summon = "mix" #mix, black, ghost
var black_demon = preload("res://Scenes/Enemies/Demon.tscn")
var ghost_demon = preload("res://Scenes/Enemies/GhostDemon.tscn")
#---------------------------------------------------------------------------------------------------------------------------------
var BeirenZemo = ["Beiren Zemo",["Die Dämonen sind in letzter Zeit in immer mehr Dörfer eingedrungen."],0,false]
var Harry = ["Harry",["Hi, ich bin Harry. Ich bin der Schmied im Dorf, ich brauche deine Hilfe","Besorge mir bitte etwas Eisen aus der Mine"],0,false]
var OlogFernflame = ["Olog Fernflame",["Das Dorf wird von Dämonen angegriffen!"],0,false]
var NhahnemDanni = ["Nhahnem Danni",["Ich habe gehört, dass der Held endlich aufgetaucht ist..."," Ich hoffe, sie können diese hoffnungslose Situation beenden."],0,false]
var BrukvirkStoutbreaker = ["Brukvirk Stoutbreaker",["Ich war ein Krieger wie du, bis ich einen Pfeil ins Knie schlug."],0,false]
var GezanVug = ["Gezan Vug",["Ich habe ein Gerücht gehört, dass der Dämonengott und der Menschengott tatsächlich Geschwister sind."," Aber nachdem sie irgendwie herausgefallen waren, wurden sie zu Schwertfeinden."],0,false]
var KimShoman = ["Kim Shoman",["Ich hörte einmal einen Dämon seltsame Worte schreien, ich glaube, sie sagten 'Oh my wah moe shin day roo'."],0,false]
var MarbathSagegust = ["Marbath Sagegust",["Welche Evolution haben Dämonen durchgemacht, um so auszusehen?"],0,false]
var BramSkytaker = ["Bram Skytaker",["Was zum Teufel machen wir hier!?"],0,false]
var DihvoMidLankhuulikt = ["Dihvo-Mid Lankhuulikt",["Es wird für uns in diesem Krieg immer hoffnungsloser."," Ich hoffe wir können überleben..."],0,false]
var GerazHithrit = ["Geraz Hithrit",["Sie sagen, Sie können uns helfen, aber stimmt das?"],0,false]
var DirvitvesMontaldathi = ["Dirvitves Montaldathi",["Die Beschaffung von Waren wird immer schwieriger, da die Dämonen immer mehr Handelsrouten blockieren."],0,false]
var RethosHirume = ["Rethos Hirume",["Ich habe meine Familie an diese verfluchten Dämonen verloren, bitte rächt meine Familie..."],0,false]
var ChihCheiy = ["Chih Cheiy",["Gibt es überhaupt eine Möglichkeit, diese Dämonen loszuwerden?"],0,false]
var IayCheim = ["Iay Cheim",["Warum haben sie uns angegriffen? Machen sie es so gerne zu töten?"],0,false]
var VacieloZazidru = ["Vacielo Zazidru",["Mein Lehrer, Professor Schlechtweg, hat uns gelehrt, dass wir bis zum bitteren Ende nicht aufgeben sollen... Ich hoffe, er hat recht."],0,false]
var SumamelZileil = ["Sumamel Zileil",["Ich weiß, ich sollte das nicht sagen, aber ich finde die Dämonen attraktiv."],0,false]
var MeishelZeidor = ["Meishel Zeidor",["Wir haben neulich eine andere Stadt an die Dämonen verloren, sie werden einfach nicht aufhören..."],0,false]
var TasrissoBrightfist = ["Tasrisso Brightfist",["Diese dreckigen Dämonen werden für das bezahlen, was sie getan haben!"],0,false]
var KizniAlpentoe = ["Kizni Alpentoe",["Mein Vater ist ein Soldat, der gegen die Dämonen kämpft, ich hoffe, es geht ihm gut..."],0,false]
var LizosGlesk = ["Lizos Glesk",["Sie sagen, der größte Angriff könnte in naher Zukunft stattfinden."],0,false]
var ToGrukniv = ["To Grukniv",["Ich habe einen seltsamen Stein gefunden."],0,false]
var ZigithiThunderaxe = ["Zigithi Thunderaxe",["Ich habe gehört, dass es einen Ort gibt, an dem Dämonen in unser Königreich eindringen,"," aber ich habe keine Ahnung, wo das ist."],0,false]
var MestroPyregloom = ["Mestro Pyregloom",["Ich habe gehört, dass es den Dämonen nach der ersten Invasion gelungen ist,"," Portale zu schaffen, damit mehr Dämonen in unser Land gelangen können."],0,false]
var HerizhiMevruzdehd = ["Herizhi Mevruzdehd",["'Wenn Sie einen Gesundheitsbalken über dem Kopf von jemandem sehen, bedeutet das, dass er ein Feind ist."," Aber nicht unbedingt ein Feind von Ihnen, sondern ein Feind seiner selbst.' ","Das hat mir jemand gesagt, der behauptet, ein Raydeo-Bleemp zu sein, was auch immer das sein mag."],0,false]
var ThophiHevrehd = ["Thophi Hevrehd",["Also bist du hierhergekommen, um zu handeln, Held?"],0,false]
var SemvechiTranurothi = ["Semvechi Tranurothi",["Die Leute hier in dieser Stadt reden so einen Unsinn, es fühlt sich an wie etwas, das man bekommt, wenn man zufällige Leute in einer Bar bitten würde, Zeilen zu schreiben."," Mir gefällt es hier nicht, bitte hilf mir zu entkommen."],0,false]
var ZendriDudunta = ["Zendri Dudunta",["Verdammt, ich wünschte, ich hätte die Kraft, diese Dämonen zu bekämpfen!"],0,false]
var XuaChia = ["Xua Chia",["Hast du meinen Sohn gesehen? Er sieht ein wenig gruselig aus, er hat eine Hauterkrankung."],0,false]
var MuaShaim = ["Mua Shaim",["Ich werde einen Dämon schlagen."],0,false]
var VondmOrnura = ["Vondm Ornura",["Ich werde ein Huhn schlagen."],0,false]
var OsneimilShamei = ["Osneimil Shamei",["Hilfe! Er wird mein Huhn schlagen!"],0,false]
var ZehnolMekol = ["Zehnol Mekol",["Oh, Held, willst du handeln?"],0,false]
var JonuwollChestmaw = ["Jonuwoll Chestmaw",["Oh, Held, ich sehe, du bist stärker geworden. Vielleicht könnten wir mal ein bisschen Sparring machen."],0,false]
var JhechieHazemourn = ["Jhechie Hazemourn",["Wir haben in diesem Krieg gegen die Dämonen so viele Verluste erlitten, dass ich nicht umhinkann, mich zu fragen, ob es überhaupt einen Weg gibt, zu gewinnen."],0,false]
var SoneChatsk = ["Sone Chatsk",["Und einfach so - ich habe die vierte Wand zertrümmert."],0,false]
var SaChirgoz = ["Sa Chirgoz",["In letzter Zeit gab es einige ungewöhnlich starke Dämonen…"],0,false]
var NagevriShadowstrength = ["Nagevri Shadowstrength",["Ich hoffe, die Ritter können dem Helden helfen, diese Dämonen zu bekämpfen."],0,false]
var ZovraWillowshaper = ["Zovra Willowshaper",["Dämonen sind beängstigend!"],0,false]
var HesezrohJanskuundad = ["Hesezroh Janskuundad",["Wir können bei all den Kämpfen in letzter Zeit nicht in Frieden trinken."],0,false]
var LezhoZihkat = ["Lezho Zihkat",["Held, bitte werde stärker für uns."],0,false]
var YarrafaRyeduventa = ["Yarrafa Ryeduventa",["Die Dämonen haben eine nahe gelegene Stadt angegriffen, bitte helft ihnen!"],0,false]
var ZaheUngulbe = ["Zahe Ungulbe",["Held, ich habe gehört, je mehr Dämonen du besiegst, desto stärker wirst du. Mögest du sie alle besiegen!"],0,false]
var JueMua = ["Jue Mua",["Was sollen wir machen?"],0,false]
var MuiYue = ["Mui Yue",["Wie werden wir gewinnen?"],0,false]
var VuatqJunzuza = ["Vuatq Junzuza",["Wir brauchen mehr Vorräte..."],0,false]
var BhekhamPadal = ["Bhekham Padal",["Held, denkst du, du kannst gewinnen?"],0,false]
var BeshunJennin = ["Beshun Jennin",["Sparen Sie immer Mana, wenn Sie kämpfen, es wäre schade, wenn Sie mitten in einem harten Kampf ausgehen würden."],0,false]
var BiiWisechewer = ["Bii Wisechewer",["Ich bete, dass wir nicht sterben..."],0,false]
var VograddLaughinglance = ["Vogradd Laughinglance",["Können wir diese Monster irgendwie loswerden?"],0,false]
var BrervofBesk = ["Brervof Besk",["Ich höre, dass Dämonen stärker werden, wenn du stärker wirst."],0,false]
var HefShoritsk = ["Hef Shoritsk",["Ich hasse das..."],0,false]
var AdrerthCragslayer = ["Adrerth Cragslayer",["Würdest du mir helfen, Held?"],0,false]
var TrunFreewatcher = ["Trun Freewatcher",["Held, es gab Sprüche über einen mächtigen Dämon, der in der Nähe gesichtet wurde."],0,false]
var VihikRuthFalihpeld = ["Vihik-Ruth Falihpeld",["Warum werden sie noch stärker!?"],0,false]
var JirDohRenzald = ["Jir-Doh Renzald",["Ich will nur zu Hause bleiben."],0,false]
var FadjerzotGadenzinta = ["Fadjerzot Gadenzinta",["Ich möchte nur bei meiner Familie bleiben und nicht hingehen und kämpfen."],0,false]
var TarokVetrabya = ["Tarok Vetrabya",["Ich weiß, du liest das hier, eine Person, die den Helden kontrolliert."],0,false]
var JanTuy = ["Jan Tuy",["Ich brauche deine Dienste, Held.","Bitte, beschütze diesen Ort"],0,false]
var LiamDa = ["Liam Da",["Könntest du ein paar Dämonen für mich töten?"],0,false]
var FurdarneUmivon = ["Furdarne Umivon",["Ich wünschte, wir könnten Kreaturen kontrollieren, um diese Dämonen für uns zu bekämpfen."],0,false]
var BehlamJikhi = ["Behlam Jikhi",["Lass sie uns nicht unser Zuhause nehmen!"],0,false]
var SushenShussul = ["Sushen Shussul",["Da sind Dämonen in der Nähe..."],0,false]
var GobranDarkshaper = ["Gobran Darkshaper",["Können Sie untersuchen, was los ist?"],0,false]
var VeoFarrowdust = ["Veo Farrowdust",["Where am I? Why are there demons? Why are they speaking a language I cannot understand here?"],0,false]
var HedgifGlidz = ["Hedgif Glidz",["Diese Dämonen werden bezahlen!"],0,false]
var GanKamadz = ["Gan Kamadz",["Diese Dämonen sind sehr sündig."],0,false]
var ZurkathClouddancer = ["Zurkath Clouddancer",["Gott, bitte gib uns die Hoffnung, die wir in dieser hoffnungslosen Zeit brauchen!"],0,false]
var RerthMirthmantle = ["Rerth Mirthmantle",["Wie kann das sein..."],0,false]
var LoreuchehJepruezdem = ["Loreucheh Jepruezdem",["Können Sie mir einen Moment ersparen?"],0,false]
var ZulaSuzohd = ["Zula Suzohd",["Wir werden diesen Krieg beenden!"],0,false]
var HeleyTruereaper = ["Heley Truereaper",["Tritt sie an den Bordstein!"],0,false]
var SosSenev = ["Sos Senev",["Ist das das Ende?"],0,false]
var TozesSog = ["Tozes Sog",["Es ist der bisher größte Angriff!"],0,false]
var VohKiohRuuldrekt = ["Voh-Kioh Ruuldrekt",["Hätten wir das nur verhindern können..."],0,false]
var TamveldakHenavoki = ["Tamveldak Henavoki",["Dämonen haben angegriffen!"],0,false]
var GoldodUgoba = ["Goldod Ugoba",["Dieser Dämon sieht sehr stark aus, das ist kein gewöhnlicher Feind."],0,false]
var WiaUen = ["Wia Uen",["Nimm sie runter!"],0,false]
var WongNummer = ["Wong Nummer",["你拨错了号码。"],0,false]
var ChierviceazMavanzan = ["Chierviceaz Mavanzan",["Ist das das Ende?"],0,false]
var JeidohrohBhahro = ["Jeidohroh Bhahro",["Wir müssen versuchen, irgendwie zu gewinnen."],0,false]
var RehnihJessan = ["Rehnih Jessan",["Wir kommen vielleicht zum Endspiel dieses Krieges gegen diese üblen Dämonen."],0,false]
var CisrillohThreehide = ["Cisrilloh Threehide",["Roll for initiative."],0,false]
var KirlaLongcreek = ["Kirla Longcreek",["Ich habe eine nat 20!"],0,false]
var VeneStag = ["Vene Stag",["Hoffentlich endet dieser Krieg bald."],0,false]
var VeGrarnitsk = ["Ve Grarnitsk",["Was machst du hier, Held?"],0,false]
var LefnoriEmberspirit = ["Lefnori Emberspirit",["Es gibt nicht viel, was ich tun kann, um zu helfen."],0,false]
var VeliHavenshade = ["Veli Havenshade",["Warum greifen uns die Dämonen an, bis zu ihrem ersten Angriff gab es niemanden, der jemals von ihnen gehört hatte..."],0,false]
var KuzefruthJultuvab = ["Kuzefruth Jultuvab",["Held, ich brauche deine Hilfe.","Töte alle Dämonen, sodass mein Sohn Ruhe finden kann"],0,false]
var LezroFihkifk = ["Lezro Fihkifk",["Ich hasse diese Situation..."],0,false]
var RehilrathStematrudi = ["Rehilrath Stematrudi",["Ich wünschte, es gäbe mehr Helden wie dich, Held."],0,false]
var FuveRudurnu = ["Fuve Rudurnu",["Ich könnte einen Nacktlauflauf machen."],0,false]
var TsiaoZiam = ["Tsiao Ziam",["Glaubst du, wir könnten Dämonenfleisch essen?"],0,false]
var LuaYiy = ["Lua Yiy",["Was sollen wir jetzt machen?"],0,false]
var LodmRecosos = ["Lodm Recosos",["Ich habe gehört, dass einer der Führer des Königreichs, Chris, ein Workaholic ist."],0,false]
var MedamShassur = ["Medam Shassur",["Danny ist schlecht darin, Karten zu erstellen."],0,false]
var RuoCrowtail = ["Ruo Crowtail",["Kannst du diese Dämonen für uns bekämpfen?"],0,false]
var MurStonerage = ["Mur Stonerage",["So viele Dämonen greifen an!"],0,false]
var JeutunuZacruuvuum = ["Jeutunu Zacruuvuum",["Die Dämonen haben in letzter Zeit zugenommen."],0,false]
var JenracGonovri = ["Jenrac Gonovri",["Ich höre Gerüchte, dass es eine Person namens Fabian gab, die versucht hat, etwas namens künstlicher Intelligenz in Golems einzusetzen, damit sie schlauer werden."],0,false]
var JaoJaom = ["Jao Jaom",["Dämonen sind lahm."],0,false]

var NPC_Template2 = ["",[],0,false]
var NPCs1 = [BeirenZemo,Harry,OlogFernflame,NhahnemDanni,BrukvirkStoutbreaker,GezanVug,KimShoman,MarbathSagegust,BramSkytaker,DihvoMidLankhuulikt,GerazHithrit,DirvitvesMontaldathi]
var NPCs2 = [RethosHirume,ChihCheiy,IayCheim,VacieloZazidru,SumamelZileil,MeishelZeidor,TasrissoBrightfist,KizniAlpentoe,LizosGlesk,ToGrukniv,ZigithiThunderaxe,MestroPyregloom,HerizhiMevruzdehd]
var NPCs3 = [ThophiHevrehd,SemvechiTranurothi,ZendriDudunta,XuaChia,MuaShaim,VondmOrnura,OsneimilShamei,ZehnolMekol,JonuwollChestmaw,JhechieHazemourn,SoneChatsk]
var NPCs4 = [SaChirgoz,NagevriShadowstrength,ZovraWillowshaper,HesezrohJanskuundad,LezhoZihkat,YarrafaRyeduventa,ZaheUngulbe,JueMua,MuiYue,VuatqJunzuza,BhekhamPadal,BeshunJennin]
var NPCs5 = [BiiWisechewer,VograddLaughinglance,BrervofBesk,HefShoritsk,AdrerthCragslayer,TrunFreewatcher,VihikRuthFalihpeld,JirDohRenzald,FadjerzotGadenzinta,TarokVetrabya,JanTuy,LiamDa]
var NPCs6 = [FurdarneUmivon,BehlamJikhi,SushenShussul,GobranDarkshaper,VeoFarrowdust,HedgifGlidz,GanKamadz,ZurkathClouddancer,RerthMirthmantle,LoreuchehJepruezdem]
var NPCs7 = [ZulaSuzohd,HeleyTruereaper,SosSenev,TozesSog,VohKiohRuuldrekt,TamveldakHenavoki,GoldodUgoba,WiaUen,WongNummer,ChierviceazMavanzan,JeidohrohBhahro]
var NPCs8 = [RehnihJessan,CisrillohThreehide,KirlaLongcreek,VeneStag,VeGrarnitsk,LefnoriEmberspirit,VeliHavenshade,KuzefruthJultuvab,LezroFihkifk,RehilrathStematrudi,FuveRudurnu,TsiaoZiam]
var NPCs9 = [LuaYiy,LodmRecosos,MedamShassur,RuoCrowtail,MurStonerage,JeutunuZacruuvuum,JenracGonovri,JaoJaom]
var NPCS = NPCs1+NPCs2+NPCs3+NPCs4+NPCs5+NPCs6+NPCs7+NPCs8+NPCs9
var verwendete_npcs = []
#---------------------------------------------------------------------------------------------------------------------------------
#Funktionen
func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)
	set_armor_and_damage()

func give_random_npc():
	var len_npc = len(NPCS)
	var my_nmbr = randi()%len_npc+0
	var gewahlter_npc = NPCS[my_nmbr]

	if not gewahlter_npc in verwendete_npcs:
		return(NPCS[my_nmbr])
	else:
		gewahlter_npc = give_random_npc()
		return(gewahlter_npc)
	
func give_NPC_for_name(name):
	var Ergebnis = Harry
	for NPC in NPCS:
		var npc_name = NPC[0]
		if npc_name == name:
			Ergebnis = NPC
	return(Ergebnis)
			
func set_armor_and_damage():
	player_armor = player_base_armor * player_armor_multiplier
	player_attack_damage = player_base_attack_damage + player_attack_per_lvl * (player_current_lvl - 1)
	player_attack_speed = 0
	player_speed = player_base_movement_speed
	
	player_max_health = player_base_health+(player_health_per_lvl*(player_current_lvl-1))
	player_max_mana = player_base_mana+(player_mana_per_lvl*(player_current_lvl-1))
	for inventory_item in inventory:
		var inventory_item_stats = inventory_item[0]
		var is_this_equipped = inventory_item[1]
		
		var plus_armor = inventory_item_stats.base_armor + inventory_item_stats.armor_per_lvl * inventory_item_stats.level
		var plus_dmg = inventory_item_stats.base_attack + inventory_item_stats.attack_per_lvl * inventory_item_stats.level
		var plus_life = inventory_item_stats.base_health + inventory_item_stats.health_per_lvl * inventory_item_stats.level
		var plus_attack_speed= inventory_item_stats.base_attack_speed + inventory_item_stats.attack_speed_per_lvl * inventory_item_stats.level
		var plus_mana = inventory_item_stats.base_mana + inventory_item_stats.mana_per_lvl * inventory_item_stats.level
		var plus_movement_speed = inventory_item_stats.base_movement_speed + inventory_item_stats.movement_speed_per_lvl * inventory_item_stats.level
		if is_this_equipped == true:
			player_armor += plus_armor
			player_attack_damage += plus_dmg
			player_max_health += plus_life
			player_max_mana += plus_mana
			player_attack_speed += plus_attack_speed
			player_speed += plus_movement_speed
	
		
			
	
func de_equip_item(item_name):
	for inventory_item in inventory:
		var inventory_item_stats = inventory_item[0]
		var slot_nmbr = inventory_item_stats.slotnr
		var is_this_equipped = inventory_item[1]
		if inventory_item_stats.name == item_name:
			
			inventory_item[1] = false
	
func equip_item(name):
	
	for inventory_item in inventory:
		var inventory_item_stats = inventory_item[0]
		var slot_nmbr = inventory_item_stats.slotnr
		var is_this_equipped = inventory_item[1]
		if inventory_item_stats.name == name:
			inventory_item[1] = true
			for inventory_item_to in inventory:
				var inventory_item_stats_to = inventory_item_to[0]
				var slot_nmbr_to = inventory_item_stats_to.name
				if slot_nmbr_to == slot_nmbr:
					if inventory_item_stats_to.name != name: 
						de_equip_item(inventory_item_stats_to.name)
	set_armor_and_damage()
	
func is_equipped(name):
	var Ergebnis = false
	for inventory_item in inventory:
		var inventory_item_stats = inventory_item[0]
		var is_this_equipped = inventory_item[1]
		if inventory_item_stats.name == name:
			Ergebnis = is_this_equipped
	return(Ergebnis)

func ressource_transaktion(gold_cost,iron_cost,stone_cost,nahrung_cost,persons_cost):
	if gold >= gold_cost:
		if nahrung >= nahrung_cost:
			if stone >= stone_cost:
				if iron >= iron_cost:
					if persons >= persons_cost:
						gold -= gold_cost
						nahrung -= nahrung_cost
						stone -= stone_cost
						iron -= iron_cost
						persons -= persons_cost
						return true
	return false

func lvl_up_Mage():
	var gold_cost = Mage_gold_upgradekosten_base + Mage_gold_upgradekosten_per_lvl * (Mage_level - 1)
	var iron_cost = Mage_iron_upgradekosten_base + Mage_iron_upgradekosten_per_lvl * (Mage_level - 1)
	var stone_cost = 0
	var nahrung_cost = Mage_food_upgradekosten_base + Mage_food_upgradekosten_per_lvl * (Mage_level - 1)
	var persons_cost = 0
	
	if ressource_transaktion(gold_cost, iron_cost, stone_cost, nahrung_cost, persons_cost):
		Mage_level+=1
		calc_mage_stats()
		return true
	return false

func calc_mage_stats():
	Mage_attack_damage = Mage_base_attack + Mage_attack_per_lvl * (Mage_level - 1)
	Mage_health = Mage_base_health + Mage_health_per_lvl * (Mage_level - 1)
	Mage_armor = Mage_base_armor + Mage_armor_per_lvl * (Mage_level - 1)

func lvl_up_soldier():
	var gold_cost = einfacher_fussoldat_gold_upgradekosten_base + einfacher_fussoldat_gold_upgradekosten_per_lvl * (einfacher_fusssoldat_level - 1)
	var iron_cost = einfacher_fussoldat_iron_upgradekosten_base + einfacher_fussoldat_iron_upgradekosten_per_lvl * (einfacher_fusssoldat_level - 1)
	var stone_cost = 0
	var nahrung_cost = einfacher_fussoldat_food_upgradekosten_base + einfacher_fussoldat_food_upgradekosten_per_lvl * (einfacher_fusssoldat_level - 1)
	var persons_cost = 0
	
	if ressource_transaktion(gold_cost, iron_cost, stone_cost, nahrung_cost, persons_cost):
		einfacher_fusssoldat_level+=1
		calc_soldier_stats()
		return true
	return false

func calc_soldier_stats():
	einfacher_fusssoldat_attack_damage = einfacher_fussoldat_base_attack + einfacher_fussoldat_attack_per_lvl * (einfacher_fusssoldat_level - 1)
	einfacher_fusssoldat_health = einfacher_fussoldat_base_health + einfacher_fussoldat_health_per_lvl * (einfacher_fusssoldat_level - 1)
	einfacher_fusssoldat_armor = einfacher_fussoldat_base_armor + einfacher_fussoldat_armor_per_lvl * (einfacher_fusssoldat_level - 1)

func new_soldier():
	var gold_cost = einfacher_fussoldat_gold_ausbildungskosten
	var iron_cost = einfacher_fussoldat_iron_ausbildungskosten
	var stone_cost = 0
	var nahrung_cost = einfacher_fussoldat_food_ausbildungskosten
	var persons_cost = 1
	
	if ressource_transaktion(gold_cost, iron_cost, stone_cost, nahrung_cost, persons_cost):
		einfacher_fusssoldat +=1
		return true
	return false
func new_Mage():
	var gold_cost = Mage_gold_ausbildungskosten
	var iron_cost = Mage_iron_ausbildungskosten
	var stone_cost = 0
	var nahrung_cost = Mage_food_ausbildungskosten
	var persons_cost = 1
	
	if ressource_transaktion(gold_cost, iron_cost, stone_cost, nahrung_cost, persons_cost):
		Mages +=1
		return true
	return false
func give_demon_to_summon():
	
	var demon_to_summon = black_demon
	if demon_art_to_summon == "mix":
		var zahl = randi()%2+1
		if zahl == 2:
			demon_to_summon = ghost_demon
		elif zahl == 1:
			demon_to_summon = black_demon
	elif demon_art_to_summon == "black":
		demon_to_summon = black_demon
	elif demon_art_to_summon == "ghost":
		demon_to_summon = ghost_demon
		
	return(demon_to_summon)
	
func buy_item(item_id):
	var item = Shop_Iventory[item_id]
	var player_inventory_item_id = -1
	var item_name = item.name
	var item_anzahl = item.owned
	var item_price = item.price
	
	if gold >= item_price:
		gold -= item_price
		if "Heil" in item_name:
			new_health_potion.owned += 1
		else:
			new_mana_potion.owned += 1
		
		return true
	else:
		return false
		 
func lvl_up_item(item_id):
	var this_item_all = inventory[item_id]
	var this_item = this_item_all[0]
	
	var gold_cost = this_item.base_gold + this_item.level * this_item.gold_per_lvl
	var iron_cost = this_item.base_iron + this_item.level * this_item.iron_per_lvl
	var stone_cost = this_item.base_stone + this_item.level * this_item.stone_per_lvl
	
	if gold >= gold_cost and iron >= iron_cost and stone >= stone_cost:
		gold -= gold_cost
		iron -= iron_cost
		stone -= stone_cost
		this_item.level+=1
		return true
	else:
		return false
	
	
	
	
func goto_scene(path):
	get_tree().current_scene.get_node("CanvasLayer").load_next_lvl(path)
	#get_tree().change_scene(path)
	
func switch_to_main_menu():
	goto_scene("res://Scenes/GUI/Start_Screen/StartScreen.tscn")

func switch_to_village():
	goto_scene("res://Scenes/Levels/Town.tscn")

func switch_to_city():
	goto_scene("res://Scenes/Levels/City.tscn")

func switch_to_overworld():
	goto_scene("res://Scenes/Players/overworld.tscn")

func switch_to_lvl():
	goto_scene("res://Scenes/Levels/GameMap.tscn")

func switch_to_start_screen():
	goto_scene("res://Scenes/GUI/Start_Screen/StartScreen.tscn")

func switch_to_overworld_after_fight():
	goto_scene("res://Scenes/Players/overworld.tscn")
	
func begin_fight():
	goto_scene("res://Scenes/Levels/GameMap.tscn")

func set_skill(number,name):
	for skill in all_skills:
		var skill_name = skill[0]
		if skill_name == name:
			player_skills[number] = skill
	
func switch_soldats_in_baracks():
	var max_lenght_soldier = len(all_soldiers_in_barracks) - 1
	current_barracks_scene+=1
	if current_barracks_scene > max_lenght_soldier:
		current_barracks_scene = 0
		
	goto_scene(all_soldiers_in_barracks[current_barracks_scene])


func recalculate_remaining_variables():
	calc_mage_stats()
	calc_soldier_stats()
	# Player stats

func equip_potion():
	if new_health_potion.equipped:
		new_health_potion.equipped = false
		new_mana_potion.equipped = true
	elif new_mana_potion.equipped:
		new_health_potion.equipped = true
		new_mana_potion.equipped = false
		


func save_game():
	var save_game = File.new()
	save_game.open("res://savegame.save", File.WRITE)
	var node_data = self.call("save")

	save_game.store_line(to_json(node_data))
	save_game.close()

func save_base_stats():
	var save_game = File.new()
	save_game.open("res://startstats.save", File.WRITE)
	var node_data = self.call("save")

	save_game.store_line(to_json(node_data))
	save_game.close()

func save():
	var save_dict = {
		"current_town_visiting" :current_town_visiting,
		"runde" : runde,
		"after_fight": after_fight,
		"player_died": player_died,
		"won_fight": won_fight,
		"schmiede_besucht": schmiede_besucht,
		"shop_besucht": shop_besucht,
		"kaserne_besucht": kaserne_besucht,

		"einfacher_fusssoldat" : einfacher_fusssoldat,
		"einfacher_fusssoldat_level" : einfacher_fusssoldat_level,
		
		"Mages" : Mages,
		"Mage_level" : Mage_level,
		"player_current_lvl" : player_current_lvl,
		"inventory": inventory,
		
		"alle_orte_dict" : alle_orte_dict,
		"gold" : gold,
		"iron" : iron,
		"stone" : stone,
		"nahrung" : nahrung,
		"persons" : persons,
		"new_health_potion" : new_health_potion,
		"new_mana_potion": new_mana_potion

	}
	return save_dict
