extends Node

#Soldaten Anzahl komplett
var soldats = 0

#Soldatenarten
#einfacher Fußsoldat
var einfacher_fusssoldat = 1200
var einfacher_fussoldat_fur_Schlacht = 10
var einfacher_fusssoldat_distance_to_player = 1000
var einfacher_fusssoldat_health = 200000
var einfacher_fusssoldat_attack_damage = 2
var einfacher_fusssoldat_max_number_of_aggroed_demons = 4
var einfacher_fusssoldat_speed = 100

var einfacher_fusssoldat_level = 1
var einfacher_fussoldat_food_cost = 100
var einfacher_fussoldat_persons_cost = 100
var einfacher_fussoldat_gold_cost = 100
var einfacher_fussoldat_iron_cost = 100
var einfacher_fussoldat_stone_cost = 100


#Mage
var Mages = 12
var Mages_fur_Schlacht = 20
var Mage_health = 100
var Mage_speed = 100
var Mage_attack_damage = 100

var Mage_level = 1
var Mage_food_cost = 100
var Mage_persons_cost = 100
var Mage_gold_cost = 100
var Mage_iron_cost = 100
var Mage_stone_cost = 100

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
