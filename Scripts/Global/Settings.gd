extends Node


var soundeffekte_volume = 0

var enable_soundeffekte = true
var music_volume = 0
var enable_music = true
var ignore_cutscenes = false
var scaling_per_round = 0.2
var start_scaling = 0
var enable_full_screen = false


func save_setting():
	var save_game = File.new()
	save_game.open("res://settings.save", File.WRITE)
	var node_data = self.call("save")

	save_game.store_line(to_json(node_data))
	save_game.close()

func recalculate_remaining_variables():
	if OS.window_fullscreen != enable_full_screen:
		OS.window_fullscreen = enable_full_screen

func save():
	var save_dict = {
		"soundeffekte_volume":soundeffekte_volume,
		"enable_soundeffekte":enable_soundeffekte,
		"music_volume":music_volume,
		"enable_music":enable_music,
		"ignore_cutscenes":ignore_cutscenes,
		"scaling_per_round":scaling_per_round,
		"start_scaling":start_scaling,
		"enable_full_screen":enable_full_screen
	}
	return save_dict
