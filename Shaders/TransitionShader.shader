shader_type canvas_item;
uniform float transition:hint_range(0f,1f) = 0f;

// In fragment:

void fragment(){
	
	if (transition <= 0.001f){
		COLOR = textureLod(SCREEN_TEXTURE, SCREEN_UV,0.0);
	}
	else {
	float size_x = transition/20f + SCREEN_PIXEL_SIZE.x;
	float size_y = transition/20f + SCREEN_PIXEL_SIZE.y;
	
	vec3 color_rgb = textureLod(SCREEN_TEXTURE, SCREEN_UV - mod(SCREEN_UV, vec2(size_x, size_y)), 0.0).rgb; 
	color_rgb.r = color_rgb.r * (1f-transition);
	color_rgb.g = color_rgb.g * (1f-transition);
	color_rgb.b = color_rgb.b * (1f-transition);
	
	COLOR.rgb = color_rgb;
	}
}