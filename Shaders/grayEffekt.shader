shader_type canvas_item;

uniform float darkening = 0.0f;
uniform bool activated = false;
uniform float animation_point: hint_range(0f,1f) = 0f;

void fragment(){
	vec4 color = texture(TEXTURE,UV);
	if (activated){

	color.r = color.r * (1f - animation_point) + (color.g * animation_point) - darkening * animation_point;
	color.b = color.b * (1f - animation_point) + (color.g * animation_point) - darkening * animation_point;
	color.g -= darkening * animation_point;
	
	COLOR = color;
	}
	else {
		COLOR = color;
	}
}